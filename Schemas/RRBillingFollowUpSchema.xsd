﻿<xs:schema targetNamespace="http://schemas.duckcreektech.com/Server/BillingFollowUp/1.0"
         elementFormDefault="qualified"
         xmlns="http://schemas.duckcreektech.com/Server/BillingFollowUp/1.0"
         xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:annotation>
    <xs:documentation>The schema for documenting the Billing Follow Up Requests and Responses.</xs:documentation>
  </xs:annotation>
  <xs:element name="BillingFollowUp.generateFollowUpRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>The BillingFollowUp.generateFollowUp request generates a billing follow-up associated with a specific account and invoice.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="ManuscriptId" type="xs:string" minOccurs="1" maxOccurs="1">
          <xs:annotation>
            <xs:documentation>The identification string associated with the follow-up manuscript used for specific configurations for the details related to the specific invoice follow-up protocols, for example how many days after the invoice date or due date to initiate follow-up activities.</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="ProcessType" minOccurs="1" maxOccurs="1" type="xs:string" use="required">
          <xs:annotation>
            <xs:documentation>The type or type code related to the process handling of the follow-up generation.</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="AccountId" minOccurs="0" maxOccurs="unbounded">
          <xs:annotation>
            <xs:documentation>The ID of the account associated with the follow-up.</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="FollowUpParameters" minOccurs="1" maxOccurs="1" use="required">
          <xs:annotation>
            <xs:documentation>A wrapper element that contains elements, such as InvoiceId and InvoiceDate, to describe the different items in Follow-Up.</xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:element name="InvoiceDueDate" minOccurs="1" maxOccurs="1" type="xs:dateTime" use="required">
                <xs:annotation>
                  <xs:documentation>The date on which the invoice is due.</xs:documentation>
                </xs:annotation>
              </xs:element>
              <xs:element name="InvoiceDate" minOccurs="1" maxOccurs="1" type="xs:dateTime" use="required">
                <xs:annotation>
                  <xs:documentation>The date on which the invoice was created.</xs:documentation>
                </xs:annotation>
              </xs:element>
              <xs:element name="InvoiceReference" minOccurs="1" maxOccurs="1" type="xs:string" use="required">
                <xs:annotation>
                  <xs:documentation>The reference Id of the invoice.</xs:documentation>
                </xs:annotation>
              </xs:element>
              <xs:element name="InvoiceId" minOccurs="1" maxOccurs="1" type="xs:string" use="required">
                <xs:annotation>
                  <xs:documentation>The ID of the invoice associated with the follow-up.</xs:documentation>
                </xs:annotation>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name="BillingFollowUp.generateFollowUpRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>The BillingFollowUp.generateFollowUp response indicates whether the billing follow-up generation was a success and has a numeric indicator for which </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>An indicator of whether the request process was a success or failure.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:sequence>
        <xs:element name="ActivitiesGenerated" minOccurs="0" maxOccurs="unbounded">
          <xs:annotation>
            <xs:documentation>A wrapper element that contains the attribute named indicator. Follow-up activities inlude reminder notices, late charges, and pending cancellation and rescind pending cancellation.</xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:attribute name="indicator" use="required" type="xs:boolean">
              <xs:annotation>
                <xs:documentation>A boolean indicator of whether or not a follow-up activity is scheduled.</xs:documentation>
              </xs:annotation>
            </xs:attribute>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name="BillingFollowUp.getPCNCalculationDataRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>This request is used by the Standard PCN processing rules evaluation.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>        
        <xs:element name="PolicyTermId" maxOccurs="1" minOccurs="1" type="xs:string">
          <xs:annotation>
            <xs:documentation>Term Id associated with the Policy term being evaluated for Standard PCN</xs:documentation>
          </xs:annotation>
        </xs:element>   
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name="BillingFollowUp.getPCNCalculationDataRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>This request returns billed receivables, processed payments, policy term config for the policy term being reviewed for standard pcn processing.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation></xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="BillingFollowUp.getFollowUpEvalDataRq">
    <xs:annotation>
      <xs:documentation>The BillingFollowUp.getFollowUpEvalDataRq sends a request for the pending cancellation notice (PCN), reminder notice (RMDR), and late charge (LTCH) data associated with a specific account, policy term, or invoice. </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="AccountId" minOccurs="1" maxOccurs="1" type="xs:long" use="required">
          <xs:annotation>
            <xs:documentation>The ID of the account associated with the follow-up.</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="PolicyTermId" minOccurs="0" maxOccurs="1" type="xs:long">
          <xs:annotation>
            <xs:documentation>The ID of the policy term associated with the follow-up. If the PolicyTermId is empty or null, the request returns follow-up data for all of the policy terms on the account.</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="InvoiceId" minOccurs="0" maxOccurs="1" type="xs:long">
          <xs:annotation>
            <xs:documentation>The ID of the invoice associated with the follow-up. If the InvoiceId is empty or null, the request returns follow-up data for the three most recent invoices.</xs:documentation>
          </xs:annotation>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name="BillingFollowUp.getFollowUpEvalDataRs">
    <xs:annotation>
      <xs:documentation>
      </xs:documentation>
    </xs:annotation>
  </xs:element>
</xs:schema>