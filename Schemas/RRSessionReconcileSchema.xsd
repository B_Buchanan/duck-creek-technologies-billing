﻿<xs:schema targetNamespace="http://schemas.duckcreektech.com/Server/SessionReconcile/1.0" elementFormDefault="qualified" xmlns="http://schemas.duckcreektech.com/Server/SessionReconcile/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:annotation>
    <xs:documentation>Requests that modify and reconcile Session documents.</xs:documentation>
  </xs:annotation>
  <xs:element name="SessionReconcile.getTransactionReportRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Get the transaction list, with onset and offset points for the current of specified term of a policy.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="policyID" type="xs:int" use="optional">
        <xs:annotation>
          <xs:documentation>Database ID of the policy to load.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="TermDate" type="xs:date" use="optional">
        <xs:annotation>
          <xs:documentation>Date identifying the policy term to report. The date may be any date within the range of a term on the policy, and only the transactions from the appropriate term will be reported. If omitted or blank, all terms will be reported.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="details" type="xs:int" use="optional">
        <xs:annotation>
          <xs:documentation>Identifies the amount of detail to be returned in the report. Default is 0=none, 1 = premiums only, 2=detailed field changes for each transaction, 4=session data for each image.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="premiumClassList" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>Identifies the class string to return for premium report. premiumClassList default is "Premium", can be comma-separated list of premium classes to process.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="transGroup" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>Identifies the id of the transaction group. When used as a parameter to the request, specified to return the details of the specified transaction group only, otherwise the details are returned for all transaction entries.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="descriptor" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="SessionReconcile.getTransactionReportRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>A detailed view of the transaction report on a given policy</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="report" minOccurs="1" maxOccurs="1">
          <xs:annotation>
            <xs:documentation>The containing element for the entire report</xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:element name="entry" minOccurs="0" maxOccurs="unbounded">
                <xs:annotation>
                  <xs:documentation>An entry in a transaction report</xs:documentation>
                </xs:annotation>
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name="transactions" minOccurs="1" maxOccurs="1" type="transactionsType" />
                    <xs:element name="premiums" minOccurs="0" maxOccurs="1">
                      <xs:annotation>
                        <xs:documentation>Information on the premiums within this report</xs:documentation>
                      </xs:annotation>
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name="premium" minOccurs="0" maxOccurs="unbounded">
                            <xs:complexType>
                              <xs:attribute name="xpath" type="xs:string" use="required">
                                <xs:annotation>
                                  <xs:documentation>The XPath within the XML to the specified premium</xs:documentation>
                                </xs:annotation>
                              </xs:attribute>
                              <xs:attribute name="term" type="xs:int" use="required">
                                <xs:annotation>
                                  <xs:documentation>The term of the premium</xs:documentation>
                                </xs:annotation>
                              </xs:attribute>
                              <xs:attribute name="written" type="xs:int" use="required">
                                <xs:annotation>
                                  <xs:documentation>The pro-rated premium as written</xs:documentation>
                                </xs:annotation>
                              </xs:attribute>
                              <xs:attribute name="charge" type="xs:int" use="required">
                                <xs:annotation>
                                  <xs:documentation>The charge for this particular premium</xs:documentation>
                                </xs:annotation>
                              </xs:attribute>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name="info" minOccurs="0" maxOccurs="unbounded">
                            <xs:annotation>
                              <xs:documentation>Information on a specific premium</xs:documentation>
                            </xs:annotation>
                            <xs:complexType>
                              <xs:attribute name="xpath" type="xs:string" use="required">
                                <xs:annotation>
                                  <xs:documentation>The XPath within the XML to the specified premium</xs:documentation>
                                </xs:annotation>
                              </xs:attribute>
                              <xs:attribute name="value" type="xs:string" use="required">
                                <xs:annotation>
                                  <xs:documentation>The informational value of the premium</xs:documentation>
                                </xs:annotation>
                              </xs:attribute>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                    <xs:element name="differences" minOccurs="0" maxOccurs="1" type="differencesType" />
                    <xs:element name="session" minOccurs="0" maxOccurs="1" type="sessionType" />
                  </xs:sequence>
                  <xs:attribute name="type" use="required">
                    <xs:simpleType>
                      <xs:restriction base="xs:string">
                        <xs:enumeration value="onset" />
                        <xs:enumeration value="offset" />
                      </xs:restriction>
                    </xs:simpleType>
                  </xs:attribute>
                  <xs:attribute name="dateStamp" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>The time and day of the last action on this entry</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="charge" type="xs:int" use="required">
                    <xs:annotation>
                      <xs:documentation>The sum total of charges within this entry</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="transGroup" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>The Transaction Group ID of this entry</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="status" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Indicates success or failure of the request.</xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation>Indicates success.</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation>Indicates an error occurred.</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="SessionReconcile.listDifferencesRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>List the differences between two session or named document of XML data. Two session tags or named documents are required, indicating the two documents to compare.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="session" minOccurs="1" maxOccurs="unbounded" type="sessionType" />
      </xs:sequence>
      <xs:attribute name="ignoreTags" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>Comma-delimited list of element tags to ignore in the comparison. For example, this is used to ignore PolicyAdmin and transaction tags.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="SessionReconcile.reconcileRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Apply the listed changes to the specified session data.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="session" minOccurs="0" maxOccurs="1" type="sessionType" />
        <xs:element name="differences" minOccurs="0" maxOccurs="1" type="differencesType" />
      </xs:sequence>
      <xs:attribute name="updateSession" type="xs:int" use="optional">
        <xs:annotation>
          <xs:documentation>Flag to indicate if the session data is to be updated (true) or just read (false).</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="SessionReconcile.setWrittenPremiumRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="policyID" type="xs:int" use="optional">
        <xs:annotation>
          <xs:documentation>Database ID of the policy to load.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="updateSession" type="xs:int" use="optional">
        <xs:annotation>
          <xs:documentation>Flag to indicate if the session data is to be updated (true) or just read (false).</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="premiumClass" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>Text to search for in the class attribute of the ManuScript field that will indicate the field is a full-term premium to be processed to determine the written and charge amounts.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="round" type="xs:int" use="optional">
        <xs:annotation>
          <xs:documentation>Rounding factor to use to determine written and earned premiums. If not specified (or 0), then no rounding is performed, otherwise this rounding number will be used. This is the same rounding numbers as used in a ManuScript calculation, 1=dollar, 100=penny, etc.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="precision" type="xs:int" use="optional">
        <xs:annotation>
          <xs:documentation>Number of digits of precision to use for the written premium factor calculated based on days remaing in the term. If omitted or 0, the factor is not rounded. The factor is rounded to the specified number of digits right of the decimal point.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="OSEDate" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>XML date to indicate the Out-of-Sequence date transaction to look for. This only needs to be specified is the date of the transaction is prior to the current term's effective date. If specified, the written premium determination will include the previous terms as specified by the OSEDate.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="factor" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>Days remaining factor to use to determine written amount for each premium. If not specified (or 1), the days remaining in the term will be automatically computed and that factor will be used. If this factor is specified, then the last transaction will use the specified factor.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="factorRound" type="xs:int" use="optional">
        <xs:annotation>
          <xs:documentation>Rounding factor to use when calculating the proRate/shortRate factor. If not specified (or 0), the value for round will be used. This is the same rounding numbers as used in a ManuScript calculation, 1=dollar, 100=penny, etc.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="SessionReconcile.listDifferencesRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns the list of differences.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="differences" minOccurs="0" maxOccurs="1" type="differencesType" />
      </xs:sequence>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Indicator of success or failure of the request.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="SessionReconcile.reconcileRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns the list of differences. The list may be modified from the list sent to the request is any of the itemized differences are no longer needed or valid for subsequent transactions.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="differences" minOccurs="0" maxOccurs="1" type="differencesType" />
      </xs:sequence>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Indicator of success or failure of the request.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="SessionReconcile.setWrittenPremiumRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns the list of transactions and itemized premium values IF updateSession is false on the request.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="transactions" minOccurs="0" maxOccurs="unbounded" type="transactionsType" />
      </xs:sequence>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Indicator of success or failure of the request.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="SessionReconcile.buildChangeSummaryRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Forms an XML document showing all of the existing field changes between the two histories.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="session" minOccurs="0" maxOccurs="2" type="sessionType" />
        <xs:element name="changeItems" minOccurs="0" maxOccurs="unbounded">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="changeItem" minOccurs="0" maxOccurs="unbounded">
                <xs:complexType>
                  <xs:attribute name="manuscript" type="xs:string" use="optional">
                    <xs:annotation>
                      <xs:documentation>The target ManuScript.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="topic" type="xs:string" use="optional">
                    <xs:annotation>
                      <xs:documentation>The target topic within the ManuScript.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="sessionXPath" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>If present, the change summary will be posted at the indicated spot in the session.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="encryptionBehavior" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>The encryption behavior of encrypted fields. Default is encryption. If it is desired to decrypt encrypted fields, use the value "AlwaysDecrypt".</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="manuscript" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>The target ManuScript.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="topic" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>The target topic within the ManuScript.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="oldHistoryID" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>The history to load as the "old" copy.  This attribute is not needed if a session node is present; the node will be used instead.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="newHistoryID" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>The history to load as the "new" copy.  This attribute is not needed if two session nodes are present; the second node will be used instead.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="extendedData" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>A boolean value that identifies whether or not new extended content will be returned for change items.  Also indicates whether to use all three description paths available (@description, @descriptionAdded, @descriptionDeleted).</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="xsltBody" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>File path at which to find an xslt file that will transform the change summary content prior to inserting it into the session and/or returning it in the response.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="transformInd" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>Indicator used to determine if the ChangeSummary.xsl file will transform the change summary content prior to inserting it into the session and/or returning it in the response.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="subFields" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>Boolean value indicating whether to include additional data for the child fields of a changed group. A value of 1 indicates to include additional data for these fields. This attribute is not valid when transformInd="1".</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="subGroups" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>Boolean value indicating whether to include additional data for the child groups (and any subsequent descendent groups) of a changed group. A value of 1 indicates to include additional data for these groups. If subFields="1", the response will contain additional data for all child fields of the changed group, all child groups, and all fields in all child groups. This attribute is valid only when transformInd="0".</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="subFieldClassFilter" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>Comma-delimited list of class names specifying which classes to include additional data for. Additional data will be returned only for fields with a class listed. Requires subFields="1". This attribute is not valid when transformInd="1".</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="includeForms" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>Boolean value indicating whether to include policy forms differences between the two history records.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="SessionReconcile.buildChangeSummaryRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns the change summary as an XML document.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="changeSummary" minOccurs="1" maxOccurs="unbounded">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="group" minOccurs="0" maxOccurs="unbounded">
                <xs:complexType>
                  <xs:choice minOccurs="0" maxOccurs="unbounded">
                    <xs:element name="instance">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name="changedValue" minOccurs="1" maxOccurs="unbounded">
                            <xs:complexType>
                              <xs:attribute name="old" type="xs:string" use="required">
                                <xs:annotation>
                                  <xs:documentation>The old value.</xs:documentation>
                                </xs:annotation>
                              </xs:attribute>
                              <xs:attribute name="new" type="xs:string" use="required">
                                <xs:annotation>
                                  <xs:documentation>The new value.</xs:documentation>
                                </xs:annotation>
                              </xs:attribute>
                              <xs:attribute name="oldFormatted" type="xs:string" use="optional">
                                <xs:annotation>
                                  <xs:documentation>The old value with the field's format mask applied.</xs:documentation>
                                </xs:annotation>
                              </xs:attribute>
                              <xs:attribute name="newFormatted" type="xs:string" use="optional">
                                <xs:annotation>
                                  <xs:documentation>The new value with the field's format mask applied.</xs:documentation>
                                </xs:annotation>
                              </xs:attribute>
                              <xs:attribute name="oldDisplay" type="xs:string" use="optional">
                                <xs:annotation>
                                  <xs:documentation>The display value based on the old value as found in the defined options for the field.</xs:documentation>
                                </xs:annotation>
                              </xs:attribute>
                              <xs:attribute name="newDisplay" type="xs:string" use="optional">
                                <xs:annotation>
                                  <xs:documentation>The display value based on the new value as found in the defined options for the field.</xs:documentation>
                                </xs:annotation>
                              </xs:attribute>
                              <xs:attribute name="id" type="xs:string" use="required">
                                <xs:annotation>
                                  <xs:documentation>The id of the changed field.</xs:documentation>
                                </xs:annotation>
                              </xs:attribute>
                              <xs:attribute name="caption" type="xs:string" use="required">
                                <xs:annotation>
                                  <xs:documentation>The caption of the id field.</xs:documentation>
                                </xs:annotation>
                              </xs:attribute>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                        <xs:attribute name="caption" type="xs:string" use="required">
                          <xs:annotation>
                            <xs:documentation>The caption of the instance.</xs:documentation>
                          </xs:annotation>
                        </xs:attribute>
                      </xs:complexType>
                    </xs:element>
                    <xs:element name="addedInstance">
                      <xs:complexType>
                        <xs:attribute name="caption" type="xs:string" use="required">
                          <xs:annotation>
                            <xs:documentation>The caption of the instance.</xs:documentation>
                          </xs:annotation>
                        </xs:attribute>
                      </xs:complexType>
                    </xs:element>
                    <xs:element name="deletedInstance">
                      <xs:complexType>
                        <xs:attribute name="caption" type="xs:string" use="required">
                          <xs:annotation>
                            <xs:documentation>The caption of the instance.</xs:documentation>
                          </xs:annotation>
                        </xs:attribute>
                      </xs:complexType>
                    </xs:element>
                    <xs:annotation>
                      <xs:documentation>
                      </xs:documentation>
                    </xs:annotation>
                    <xs:any namespace="##other" processContents="lax" />
                  </xs:choice>
                  <xs:attribute name="id" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>The id of the group affected.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:complexType name="sessionType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:attribute name="historyID" type="xs:int" use="optional">
      <xs:annotation>
        <xs:documentation>Database ID of the history image to load.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="policyID" type="xs:int" use="optional">
      <xs:annotation>
        <xs:documentation>Database ID of the policy to load.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
  <xs:complexType name="differencesType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:sequence>
      <xs:element name="difference" minOccurs="0" maxOccurs="unbounded">
        <xs:complexType>
          <xs:attribute name="type" type="xs:int" use="required">
            <xs:annotation>
              <xs:documentation>Numeric indicator of the difference type: add=0, delete=1, change=2</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="domain" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>The id within the XML where the difference occured. This id will indicate the parent item from which the path of the item can be based.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="path" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>Relative path from the domain element to the item with the difference.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="fullPath" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>Full path from the document element to the item with the difference.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="oldValue" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>Original value of the item. for a DELETE difference, this will be the XML of the deleted item.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="newValue" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>new value of the item. For an ADD difference type, this will be the XML of the added item.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
        </xs:complexType>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="transactionsType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:sequence>
      <xs:element name="transaction" minOccurs="0" maxOccurs="unbounded">
        <xs:complexType>
          <xs:sequence>
            <xs:element name="premium" minOccurs="0" maxOccurs="unbounded">
              <xs:complexType>
                <xs:attribute name="xpath" type="xs:string" use="required">
                  <xs:annotation>
                    <xs:documentation>Full path from the document element to the term premium in the policy(session) XML.</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
                <xs:attribute name="term" type="xs:double" use="required">
                  <xs:annotation>
                    <xs:documentation>Term premium as appeared in the policy(session) XML.</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
                <xs:attribute name="written" type="xs:double" use="required">
                  <xs:annotation>
                    <xs:documentation>Calculated written premium.</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
                <xs:attribute name="charge" type="xs:double" use="required">
                  <xs:annotation>
                    <xs:documentation>Amount of charge, or the change in premium.</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
              </xs:complexType>
            </xs:element>
          </xs:sequence>
          <xs:attribute name="date" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>Date of the transaction.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="writtenFactor" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>Factor inidcating the days-remaining in the term for this transaction, or the specified factor from the request.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="proRateFactor" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>Factor used to apply a pro-rate or short-rate factor to the written premium.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="term" type="xs:double" use="required">
            <xs:annotation>
              <xs:documentation>Term premium as appeared in the policy(session) XML.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="written" type="xs:double" use="required">
            <xs:annotation>
              <xs:documentation>Calculated written premium.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="charge" type="xs:double" use="required">
            <xs:annotation>
              <xs:documentation>Amount of charge, or the change in premium.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
        </xs:complexType>
      </xs:element>
    </xs:sequence>
    <xs:attribute name="written" type="xs:double" use="required">
      <xs:annotation>
        <xs:documentation>Calculated written premium.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="charge" type="xs:double" use="required">
      <xs:annotation>
        <xs:documentation>Amount of charge, or the change in premium.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
</xs:schema>