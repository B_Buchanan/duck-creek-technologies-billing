<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
	<xs:element name="products">
		<xs:complexType>
			<xs:sequence>
				<xs:element maxOccurs="unbounded" ref="product">
					<xs:annotation>
						<xs:documentation>Displays one-to-many product elements based on the number of products available in the ManuScript catalog.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<xs:element name="product">
		<xs:complexType>
			<xs:sequence>
				<xs:element ref="startProductActions" minOccurs="1">
							<xs:annotation>
								<xs:documentation>Identifies the starting URI invoked when the user starts a product at runtime.</xs:documentation>
							</xs:annotation>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="description" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>A description of the product.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="effectiveDateNew" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>The new business effective date for the product. If the product does not use this element, it will be set to a string, such as "Admin" or "None".</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="effectiveDateRenewal" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>The renewal effective date for the product. If the product does not use this element, it will be set to a string, such as "Admin" or "None".</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="family" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>The product family with which the product is associated.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="lob" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>The line of business for the product.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="name" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>A unique identifier for the product.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="productCode" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>Identifies the type of product, such as data, pages, forms, rating, or none.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="state" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>The state identification assigned to the product. Multi-state ManuScripts will appear as US, and non-state specific ManuScripts may have a non-state-based label.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="version" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>The version number for the product, such as 8.5.0.0. The first digit is versioned with each base version of the product. The second digit represents the version for the line of business. The third digit represents versioning for a circular when needed. The fourth digit represents versioning for a fix when needed.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="versionDate" use="required" type="xs:date">
				<xs:annotation>
					<xs:documentation>The version date for the product.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="versionId" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>The version identifier for the product. It is used to map versions of the product back to the original.</xs:documentation>
				</xs:annotation>
			</xs:attribute>		
		</xs:complexType>
	</xs:element>
		<xs:element name="startProductActions">
		<xs:annotation>
			<xs:documentation>Identifies the starting URI invoked when the user starts a product at runtime.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="1" maxOccurs="unbounded" ref="action">
					<xs:annotation>
						<xs:documentation>Defines the actions that can be used to start a product.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<xs:element name="action">
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="1" maxOccurs="unbounded" ref="method">
					<xs:annotation>
						<xs:documentation>Defines the group of methods for the action in a sequential processing order. Methods can be skipped if processing is not needed.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="caption">
				<xs:annotation>
					<xs:documentation>The on-screen label assigned to the action button or link.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="contentType" use="required">
				<xs:annotation>
					<xs:documentation>Defines the type of content to be passed when invoking the URI against the API. Supported content types include XML and JSON.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="application/xml"/>
						<xs:enumeration value="application/json"/>
						<xs:enumeration value="application/multipart/form-data"/>
						<xs:enumeration value="multipart/form-data"/>
						<xs:enumeration value="text/xml"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="description" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>Describes the purpose of the URI relative to the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="hidden">
				<xs:annotation>
					<xs:documentation>Indicates whether the action is shown or hidden.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="0|1"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="id" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>A unique identifier for the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="method">
		<xs:complexType>
			<xs:attribute name="index" use="required" type="xs:int">
				<xs:annotation>
					<xs:documentation>Defines the index of the method to use when calling the API.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="page" type="xs:string">
				<xs:annotation>
					<xs:documentation>Defines the page associated with the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="pageSet" type="xs:string">
				<xs:annotation>
					<xs:documentation>Defines the page set associated with the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="type" use="required">
				<xs:annotation>
					<xs:documentation>Defines the REST verb (e.g. GET, PUT, POST) to use when using the URI call against the API.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="GET"/>
						<xs:enumeration value="POST"/>
						<xs:enumeration value="DELETE"/>
						<xs:enumeration value="PUT"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="uri" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>Defines the name of the URI to use when calling the API.</xs:documentation>
				</xs:annotation>
			</xs:attribute>			
		</xs:complexType>
	</xs:element>
</xs:schema>