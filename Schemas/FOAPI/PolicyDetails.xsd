<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
	<xs:element name="policyDetails">
		<xs:annotation>
			<xs:documentation>Policy details.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="0" ref="navigation">
					<xs:annotation>
						<xs:documentation>Defines all action elements related to page navigation.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="id" type="xs:string" use="required"> 
				<xs:annotation>
					<xs:documentation>Policy ID</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="status" type="xs:string" use="required"> 
				<xs:annotation>
					<xs:documentation>Status</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="effectiveDate" type="xs:string" use="required"> 
				<xs:annotation>
					<xs:documentation>Effective date</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="expirationDate" type="xs:string" use="required"> 
				<xs:annotation>
					<xs:documentation>Expiration date</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="term" type="xs:string" use="required"> 
				<xs:annotation>
					<xs:documentation>Term</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="lob" type="xs:string" use="required"> 
				<xs:annotation>
					<xs:documentation>Line of business</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="quoteNumber" type="xs:string" use="required"> 
				<xs:annotation>
					<xs:documentation>Quote number</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="name" type="xs:string" use="required"> 
				<xs:annotation>
					<xs:documentation>Name</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="dateModified" type="xs:string" use="required"> 
				<xs:annotation>
					<xs:documentation>Modified date</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="navigation">
		<xs:annotation>
			<xs:documentation>Groups all navigation actions available for a page.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="0" maxOccurs="unbounded" ref="action">
					<xs:annotation>
						<xs:documentation>Groups the actions that can be used to call a specific page.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<xs:element name="action">
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="1" maxOccurs="unbounded" ref="method">
					<xs:annotation>
						<xs:documentation>Defines the group of methods for the action in a sequential processing order. Methods can be skipped if processing is not needed.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="caption">
				<xs:annotation>
					<xs:documentation>The on-screen label assigned to the action button or link</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="contentType" use="required">
				<xs:annotation>
					<xs:documentation>Defines the type of content to be passed when invoking the URI against the API. Supported content types include XML and JSON.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="application/xml"/>
						<xs:enumeration value="application/json"/>
						<xs:enumeration value="application/multipart/form-data"/>
						<xs:enumeration value="multipart/form-data"/>
						<xs:enumeration value="text/xml"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="description" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>Describes the purpose of the URI relative to the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="hidden">
				<xs:annotation>
					<xs:documentation>Indicates whether the action is shown or hidden.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="0|1"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="id" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>A unique identifier for the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="method">
		<xs:complexType>
			<xs:attribute name="index" use="required" type="xs:int">
				<xs:annotation>
					<xs:documentation>Defines the index of the method to use when calling the API.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="page" type="xs:string">
				<xs:annotation>
					<xs:documentation>Defines the page associated with the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="pageSet" type="xs:string">
				<xs:annotation>
					<xs:documentation>Defines the page set associated with the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="type" use="required">
				<xs:annotation>
					<xs:documentation>Defines the REST verb (e.g. GET, PUT, POST) to use when using the URI call against the API.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="GET"/>
						<xs:enumeration value="POST"/>
						<xs:enumeration value="DELETE"/>
						<xs:enumeration value="PUT"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="uri" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>Defines the name of the URI to use when calling the API.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
</xs:schema>