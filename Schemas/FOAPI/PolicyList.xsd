<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
	<xs:element name="policyList">
		<xs:annotation>
			<xs:documentation>Policy list.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="0" ref="paging">
					<xs:annotation>
						<xs:documentation>Defines all paging elements related to policy search.</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:element minOccurs="0" ref="policies">
					<xs:annotation>
						<xs:documentation>Defines all policy elements related to policy search.</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:element minOccurs="0" ref="links">
					<xs:annotation>
						<xs:documentation>Defines all link elements related to policy search.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<xs:element name="paging">
		<xs:complexType>
			<xs:attribute name="page" type="xs:int">
				<xs:annotation>
					<xs:documentation>The page number to be returned.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="perPage" type="xs:int">
				<xs:annotation>
					<xs:documentation>The number of policies to return per page.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="totalCount" type="xs:int">
				<xs:annotation>
					<xs:documentation>The total number of records found.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="policies">
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="0" maxOccurs="unbounded" ref="policy">
					<xs:annotation>
						<xs:documentation>Defines the policy element that defined the high level details for the given policy and a link to get further details.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<xs:element name="policy">
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="0" maxOccurs="unbounded" ref="links">
					<xs:annotation>
						<xs:documentation>Defines the navigation links for the policy details.</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:element minOccurs="0" ref="action">
					<xs:annotation>
						<xs:documentation>Defines all link elements related to policy search.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="id" use="required" type="xs:string"> 
				<xs:annotation>
					<xs:documentation>Policy identifier.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="status" use="required" type="xs:string"> 
				<xs:annotation>
					<xs:documentation>Policy status.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="clientId" use="required" type="xs:string"> 
				<xs:annotation>
					<xs:documentation>Client ID.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="effectiveDate" use="required" type="xs:string"> 
				<xs:annotation>
					<xs:documentation>Policy effective date.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="expirationDate" use="required" type="xs:string"> 
				<xs:annotation>
					<xs:documentation>Policy expiration date.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="lob" use="required" type="xs:string"> 
				<xs:annotation>
					<xs:documentation>Policy line of business.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="startProductId" use="required" type="xs:string"> 
				<xs:annotation>
					<xs:documentation>Start product ID.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="policyNumber" use="required" type="xs:string"> 
				<xs:annotation>
					<xs:documentation>Policy number.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="producer" use="required" type="xs:string"> 
				<xs:annotation>
					<xs:documentation>producer.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="name" use="required" type="xs:string"> 
				<xs:annotation>
					<xs:documentation>Policy name.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="links">
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="0" maxOccurs="unbounded" ref="link">
					<xs:annotation>
						<xs:documentation>Defines the navigation link used to retrieve the policy details.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<xs:element name="link">
		<xs:complexType>
			<xs:attribute name="rel" type="xs:string"> 
				<xs:annotation>
					<xs:documentation>The title used for the link.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="href" type="xs:string"> 
				<xs:annotation>
					<xs:documentation>The URI.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="action">
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="1" maxOccurs="unbounded" ref="method">
					<xs:annotation>
						<xs:documentation>Defines the group of methods for the action in a sequential processing order. Methods can be skipped if processing is not needed.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="caption">
				<xs:annotation>
					<xs:documentation>The on-screen label assigned to the action button or link</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="contentType" use="required">
				<xs:annotation>
					<xs:documentation>Defines the type of content to be passed when invoking the URI against the API. Supported content types include XML and JSON.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="application/xml"/>
						<xs:enumeration value="application/json"/>
						<xs:enumeration value="application/multipart/form-data"/>
						<xs:enumeration value="multipart/form-data"/>
						<xs:enumeration value="text/xml"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="description" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>Describes the purpose of the URI relative to the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="hidden">
				<xs:annotation>
					<xs:documentation>Indicates whether the action is shown or hidden.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="0|1"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="id" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>A unique identifier for the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="method">
		<xs:complexType>			
			<xs:attribute name="index" use="required" type="xs:int">
				<xs:annotation>
					<xs:documentation>Defines the index of the method to use when calling the API.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="page" type="xs:string">
				<xs:annotation>
					<xs:documentation>Defines the page associated with the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="pageSet" type="xs:string">
				<xs:annotation>
					<xs:documentation>Defines the page set associated with the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="type" use="required">
				<xs:annotation>
					<xs:documentation>Defines the REST verb (e.g. GET, PUT, POST) to use when using the URI call against the API.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="GET"/>
						<xs:enumeration value="POST"/>
						<xs:enumeration value="DELETE"/>
						<xs:enumeration value="PUT"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="uri" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>Defines the name of the URI to use when calling the API.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
</xs:schema>