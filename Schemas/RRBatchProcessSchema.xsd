﻿<xs:schema targetNamespace="http://schemas.duckcreektech.com/Server/BatchProcess/1.0"
         elementFormDefault="qualified"
         xmlns="http://schemas.duckcreektech.com/Server/BatchProcess/1.0"
         xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:annotation>
    <xs:documentation>Requests that allow batch processing of Server requests.</xs:documentation>
  </xs:annotation>
  <xs:element name="BatchProcess.getQueryOptionsRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Used to retrieve the query options from the server.</xs:documentation>
    </xs:annotation>
  </xs:element>
  <xs:element name="BatchProcess.getQueryOptionsRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Gets the QueryOptions setting in the configuration.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:choice>
        <xs:element name="Option" type="OptionsType" minOccurs="0" maxOccurs="unbounded" />
        <xs:element name="ExampleOption" type="OptionsType" minOccurs="0" maxOccurs="unbounded" />
      </xs:choice>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchProcess.processByScriptIdRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Used to start batch processing.  The id of the batch script is passed in as part of the request, and the batch script is loaded automatically, after which processing proceeds exactly as in the BatchProcess.processRq.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="scriptId" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>The id of the batch script to load and run.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchProcess.processByScriptIdRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns the final status of the batch process on the xpath /FinalStatus.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="FinalStatus" type="xs:string" />
      </xs:sequence>
      <xs:attribute name="status">
        <xs:annotation>
          <xs:documentation>Status of the request.  Success: the request completed successfully.  Failure: an error prevented the request from being executed.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchProcess.processRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Used to start batch processing.  The batch script is passed in as part of the request.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="PreExecute" minOccurs="0">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="requests" type="requestsType" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name="Query">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="Option" type="OptionsType" />
              <xs:element name="SessionLoadType">
                <xs:annotation>
                  <xs:documentation>The contents of this element should contain a format string that will have the values of the Sql defined in the Option block substitued into it.  It should format into a valid EXAMPLE Server request that defines how a particular session is loaded.</xs:documentation>
                </xs:annotation>
                <xs:complexType mixed="true">
                  <xs:attribute name="label" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>A label that describes the Option, SessionLoadType, or Field (used in the UI)</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name="Execute">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="requests" type="requestsType" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name="PostExecute" minOccurs="0">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="requests" type="requestsType" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchProcess.processRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns the final status of the batch process on the xpath /FinalStatus.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="FinalStatus" type="xs:string" />
      </xs:sequence>
      <xs:attribute name="status">
        <xs:annotation>
          <xs:documentation>Status of the request.  Success: the request completed successfully.  Failure: an error prevented the request from being executed.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:complexType name="requestsType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:sequence>
      <xs:any minOccurs="0" maxOccurs="unbounded" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OptionsType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:sequence>
      <xs:element name="Instruction" type="xs:string" minOccurs="0" />
      <xs:element name="Sql">
        <xs:annotation>
          <xs:documentation>Provides the sql string as well as the information for the database to query.</xs:documentation>
        </xs:annotation>
        <xs:complexType mixed="true">
          <xs:attribute name="db" use="required">
            <xs:annotation>
              <xs:documentation>The method used to obtain the query string.</xs:documentation>
            </xs:annotation>
            <xs:simpleType>
              <xs:restriction base="xs:string">
                <xs:enumeration value="list" />
                <xs:enumeration value="local" />
                <xs:enumeration value="prompt" />
                <xs:enumeration value="defined" />
              </xs:restriction>
            </xs:simpleType>
          </xs:attribute>
          <xs:attribute name="dsn" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>The DSN used to connect to the database</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="uid" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>User ID to connect to the database.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="pwd" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>The password associated with uid.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="timeoutSeconds" type="xs:int" use="optional">
            <xs:annotation>
              <xs:documentation>The SQL Command timeout in seconds. Default is 30.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
        </xs:complexType>
      </xs:element>
      <xs:element name="Layout" maxOccurs="unbounded">
        <xs:complexType>
          <xs:sequence>
            <xs:element name="Instruction" type="xs:string" minOccurs="0" />
            <xs:element name="Field" maxOccurs="unbounded">
              <xs:complexType>
                <xs:attribute name="id" type="xs:IDREF" use="required">
                  <xs:annotation>
                    <xs:documentation>The id of the field</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
                <xs:attribute name="label" type="xs:string" use="required">
                  <xs:annotation>
                    <xs:documentation>A label for the field</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
                <xs:attribute name="value" type="xs:string" use="required">
                  <xs:annotation>
                    <xs:documentation>The value of the field.</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
                <xs:attribute name="height" type="xs:string" use="optional">
                  <xs:annotation>
                    <xs:documentation>The height of the field. "full" indicates fullscreen, otherwise use a number.</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
                <xs:attribute name="width" use="optional">
                  <xs:annotation>
                    <xs:documentation>Indicates whether the field should display on multiple lines.</xs:documentation>
                  </xs:annotation>
                  <xs:simpleType>
                    <xs:restriction base="xs:boolean">
                      <xs:enumeration value="0">
                        <xs:annotation>
                          <xs:documentation>Single-line only. Default</xs:documentation>
                        </xs:annotation>
                      </xs:enumeration>
                      <xs:enumeration value="1">
                        <xs:annotation>
                          <xs:documentation>Displays on multiple lines.</xs:documentation>
                        </xs:annotation>
                      </xs:enumeration>
                    </xs:restriction>
                  </xs:simpleType>
                </xs:attribute>
                <!--xs:attribute name="width" type="xs:string" use="optional">
									<xs:annotation>
										<xs:documentation>
											The width of the field. "full" indicates fullscreen, otherwise use a number.
										</xs:documentation>
									</xs:annotation>
								</xs:attribute-->
              </xs:complexType>
            </xs:element>
          </xs:sequence>
          <xs:attribute name="direction" use="required">
            <xs:annotation>
              <xs:documentation>Determines whether fields are arranged in a vertical column or a horizontal row.</xs:documentation>
            </xs:annotation>
            <xs:simpleType>
              <xs:restriction base="xs:string">
                <xs:enumeration value="down">
                  <xs:annotation>
                    <xs:documentation>Arranges fields vertically.</xs:documentation>
                  </xs:annotation>
                </xs:enumeration>
                <xs:enumeration value="across">
                  <xs:annotation>
                    <xs:documentation>Arranges fields horizontally.</xs:documentation>
                  </xs:annotation>
                </xs:enumeration>
              </xs:restriction>
            </xs:simpleType>
          </xs:attribute>
          <xs:attribute name="captions" use="required">
            <xs:annotation>
              <xs:documentation>Determines how captions are placed in relation to thier fields.</xs:documentation>
            </xs:annotation>
            <xs:simpleType>
              <xs:restriction base="xs:string">
                <xs:enumeration value="above">
                  <xs:annotation>
                    <xs:documentation>Places captions above the fields.</xs:documentation>
                  </xs:annotation>
                </xs:enumeration>
                <xs:enumeration value="beside">
                  <xs:annotation>
                    <xs:documentation>Places captions beside the fields.</xs:documentation>
                  </xs:annotation>
                </xs:enumeration>
              </xs:restriction>
            </xs:simpleType>
          </xs:attribute>
          <xs:attribute name="hspace" type="xs:int" use="optional">
            <xs:annotation>
              <xs:documentation>Sets the horizontal spacing of the fields in this layout.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="vspace" type="xs:int" use="optional">
            <xs:annotation>
              <xs:documentation>Sets the vertical spacing of the fields in this layout.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
        </xs:complexType>
      </xs:element>
    </xs:sequence>
    <xs:attribute name="label" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>A label to describe the option being defined.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
</xs:schema>