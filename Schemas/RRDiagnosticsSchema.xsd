﻿<xs:schema targetNamespace="http://schemas.duckcreektech.com/Server/Diagnostics/1.0"
         elementFormDefault="qualified"
         xmlns="http://schemas.duckcreektech.com/Server/Diagnostics/1.0"
         xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:annotation>
    <xs:documentation>Requests that provide a way of diagnosing and debuggin Server calls.The schema for documenting the Trace object requests/responses.</xs:documentation>
  </xs:annotation>
  <xs:element name="Diagnostics.connectRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:complexType>
      <xs:annotation>
        <xs:documentation>Used to connect a trace listener to the tracing system.</xs:documentation>
      </xs:annotation>
      <xs:sequence>
        <xs:element name="Listener" minOccurs="0" maxOccurs="1" type="ListenerType" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name="Diagnostics.connectRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>See Diagnostics.connectRq.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="Listener" minOccurs="1" maxOccurs="1" type="ListenerType" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name="Diagnostics.disconnectRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Used to disconnect a previously connected trace listener from the tracing system.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="listenerId" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>The guid for a listener on the current trace.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Diagnostics.disconnectRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>See Diagnostics.disconnectRq.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>"ok", "failed", or "pending" when connecting; "removed" when disconnecting.  "pending" is used when a "Socket" listenerType is specified.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Diagnostics.getApplicationListRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Displays the names of all DCT applications.</xs:documentation>
    </xs:annotation>
  </xs:element>
  <xs:element name="Diagnostics.getApplicationListRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>See Diagnostics.getApplicationListRq.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="DCTApp" minOccurs="0" maxOccurs="unbounded" type="xs:string" />
      </xs:sequence>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>"ok", "failed", or "pending" when connecting; "removed" when disconnecting.  "pending" is used when a "Socket" listenerType is specified.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Diagnostics.refreshDefaultListenersRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Reloads the default application listeners.</xs:documentation>
    </xs:annotation>
  </xs:element>
  <xs:element name="Diagnostics.refreshDefaultListenersRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>See Diagnostics.getApplicationListRq.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>"ok", "failed", or "pending" when connecting; "removed" when disconnecting.  "pending" is used when a "Socket" listenerType is specified.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Diagnostics.setRemoteMonitoringRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Activates or deactivates remote monitoring.  Requires a listener bound to a socket.  To add one, use Diagnostics.connectRq.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="RemoteMonitoring" minOccurs="1" maxOccurs="1">
          <xs:complexType>
            <xs:attribute name="port" type="xs:integer" use="required">
              <xs:annotation>
                <xs:documentation>The port number to attach the socket to.</xs:documentation>
              </xs:annotation>
            </xs:attribute>
            <xs:attribute name="useIpAddress" type="xs:boolean" use="optional">
              <xs:annotation>
                <xs:documentation>"true" to use a supplied IP Address.</xs:documentation>
              </xs:annotation>
            </xs:attribute>
            <xs:attribute name="ipAddress" use="optional">
              <xs:annotation>
                <xs:documentation>IP address for use if the useIpAddress attribute is "true".  Otherwise, this can be omitted.</xs:documentation>
              </xs:annotation>
              <xs:simpleType>
                <xs:restriction base="xs:integer">
                  <xs:pattern value="([0-9]{1,3}.){3}[0-9]{1,3}" />
                </xs:restriction>
              </xs:simpleType>
            </xs:attribute>
            <xs:attribute name="available" use="optional">
              <xs:annotation>
                <xs:documentation>"true" or "standby" to start the monitor, omit to deactivate it.</xs:documentation>
              </xs:annotation>
              <xs:simpleType>
                <xs:restriction base="xs:string">
                  <xs:enumeration value="true" />
                  <xs:enumeration value="standby" />
                </xs:restriction>
              </xs:simpleType>
            </xs:attribute>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="toggle" use="required">
        <xs:annotation>
          <xs:documentation>Toggles the remote monitoring option on or off.</xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="on" />
            <xs:enumeration value="off" />
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Diagnostics.setRemoteMonitoringRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns an indication of the state of the monitoring.</xs:documentation>
    </xs:annotation>
    <xs:complexType mixed="true">
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>"ok", "failed", or "pending" when connecting; "removed" when disconnecting.  "pending" is used when a "Socket" listenerType is specified.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:complexType name="ListenerType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:sequence>
      <xs:element name="AppSeverity" minOccurs="0" maxOccurs="unbounded">
        <xs:complexType>
          <xs:attribute name="appList" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>A comma separated list of DuckCreek specified applications.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="severity" use="required">
            <xs:annotation>
              <xs:documentation>One of the following: "Debug", "Info", "Warning", or "Error".  Severity is stepped, so that if you subscribe to "Error" level, you will receive only "Error" level messages, if you subscribe to "Warning" level, you will receive "Warning" and "Error" messages, etc.</xs:documentation>
            </xs:annotation>
            <xs:simpleType>
              <xs:restriction base="xs:string">
                <xs:enumeration value="Debug">
                  <xs:annotation>
                    <xs:documentation>Debugging notes, warnings, and errors.</xs:documentation>
                  </xs:annotation>
                </xs:enumeration>
                <xs:enumeration value="Info">
                  <xs:annotation>
                    <xs:documentation>Informational and debugging notes, warnings, and errors</xs:documentation>
                  </xs:annotation>
                </xs:enumeration>
                <xs:enumeration value="Warning">
                  <xs:annotation>
                    <xs:documentation>Warnings and Errors</xs:documentation>
                  </xs:annotation>
                </xs:enumeration>
                <xs:enumeration value="Error">
                  <xs:annotation>
                    <xs:documentation>Errors only.</xs:documentation>
                  </xs:annotation>
                </xs:enumeration>
              </xs:restriction>
            </xs:simpleType>
          </xs:attribute>
        </xs:complexType>
      </xs:element>
    </xs:sequence>
    <xs:attribute name="listenerId" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>The guid for a listener on the current trace.  Used only in responses.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="listenerType" use="required">
      <xs:annotation>
        <xs:documentation>One of the following: "File", "Eventlog", or "Socket".</xs:documentation>
      </xs:annotation>
      <xs:simpleType>
        <xs:restriction base="xs:string">
          <xs:enumeration value="File">
            <xs:annotation>
              <xs:documentation>Write diagnostics to a file.</xs:documentation>
            </xs:annotation>
          </xs:enumeration>
          <xs:enumeration value="Eventlog">
            <xs:annotation>
              <xs:documentation>Write diagnostics to an event log.</xs:documentation>
            </xs:annotation>
          </xs:enumeration>
          <xs:enumeration value="Socket">
            <xs:annotation>
              <xs:documentation>Write diagnostics to a communications socket.</xs:documentation>
            </xs:annotation>
          </xs:enumeration>
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
    <xs:attribute name="sessionList" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>A comma separated list of session IDs.  A single listener may be connected to more than one session.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="logfilePath" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>The full path and file name to the file to write to in the case of a "File" listener type.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="eventLog" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>The name of the event log to write to in the case of an "Eventlog" listener type.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="connectionId" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>A guid that is used as a connection token when the socket listener type connects to the server or its socket.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="ipAddress" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>The ip address to connect on when a "Socket" listener type is used.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="port" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>The port to connect on when a "Socket" listener type is used.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="error" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>Contains an error message if an error occured.  The specified listener will not be connected in the case of an error.  Used only in responses.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="warning" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>Contains a warning error.  Typically indicates that a value was not provided, but a default value could be used.  Used only in responses.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
</xs:schema>