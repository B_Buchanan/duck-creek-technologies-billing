﻿<xs:schema targetNamespace="http://schemas.duckcreektech.com/Server/CustomServer/1.0" elementFormDefault="qualified" xmlns="http://schemas.duckcreektech.com/Server/CustomServer/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:annotation>
    <xs:documentation>Requests for users to send multiple requests at once.</xs:documentation>
  </xs:annotation>
  <xs:element name="CustomServer.processRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Allows a user to transform and call multiple requests.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="dataRequests" minOccurs="0" maxOccurs="1" type="dataRequestsType">
          <xs:annotation>
            <xs:documentation>This would contain all the requests we would like to execute.</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="customData" minOccurs="0" maxOccurs="1" type="customDataType">
          <xs:annotation>
            <xs:documentation>XML data that we want to add to the dataRequest response.</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="xsltRequests" minOccurs="1" maxOccurs="1" type="xsltRequestsType" use="required">
          <xs:annotation>
            <xs:documentation>Requests to execute.  These requests responses would the only ones being returned.  You may provide xsl within the requests to pull data from the dataRequests block.</xs:documentation>
          </xs:annotation>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="abortOnError" use="optional" type="xs:boolean">
        <xs:annotation>
          <xs:documentation>Disabled by default.  When enabled, any error that occurs will be returned automatically, without executing any further requests within either the dataRequests or xsltRequests layer.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="CustomServer.processRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>The response on all the requests within your xsltRequests layer, unless you specify dataRequests which fail while passing the returnOnDataError attribute on the request with a value of true.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of request failed
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation>A successful response.  No errors were detected.</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation>A failed response.  Errors were detected within either the dataRequests or xsltRequests layer.</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="CustomServer.expandRequestsRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Allows a user to transform and preview a call multiple requests.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="dataRequests" minOccurs="0" maxOccurs="1" type="dataRequestsType" />
        <xs:element name="customData" minOccurs="0" maxOccurs="1" type="customDataType" />
        <xs:element name="xsltRequests" minOccurs="1" maxOccurs="1" type="xsltRequestsType" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name="CustomServer.expandRequestsRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>See request.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of request failed
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation>
                </xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation>
                </xs:documentation>
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="CustomServer.getSemaphoreRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Gets the status and content of the asynchronous semaphore.
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="semaphoreID" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>SemaphoreID (sessionID) to be checked</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="peek" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>Defines to peek. When true, the SemaphoreID record is NOT removed when retrieved. Default is false.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="final" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>Defines if the session associated with the semaphore is to closed when the semaphore is retrieved after completion. When true, the session is closed, otherwise the session remains open. Default is true.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="CustomServer.getSemaphoreRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>See request.
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="ExampleNotification" minOccurs="0" maxOccurs="unbounded">
          <xs:complexType>
            <xs:attribute name="notify" use="required">
              <xs:annotation>
                <xs:documentation>
                  Describes the status of the response:
                  invalid	- The specified semaphoreID is invalid
                  success	- The asynchronous request is still working, no response yet
                  success	- Execution of request finished and was successful
                  failure	- Execution of request finished and returned failed response
                </xs:documentation>
              </xs:annotation>
              <xs:simpleType>
                <xs:restriction base="xs:string">
                  <xs:enumeration value="invalid">
                    <xs:annotation>
                      <xs:documentation>
                      </xs:documentation>
                    </xs:annotation>
                  </xs:enumeration>
                  <xs:enumeration value="working">
                    <xs:annotation>
                      <xs:documentation>
                      </xs:documentation>
                    </xs:annotation>
                  </xs:enumeration>
                  <xs:enumeration value="success">
                    <xs:annotation>
                      <xs:documentation>
                      </xs:documentation>
                    </xs:annotation>
                  </xs:enumeration>
                  <xs:enumeration value="failure">
                    <xs:annotation>
                      <xs:documentation>
                      </xs:documentation>
                    </xs:annotation>
                  </xs:enumeration>
                </xs:restriction>
              </xs:simpleType>
            </xs:attribute>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of request failed
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation>
                </xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation>
                </xs:documentation>
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="CustomServer.listSemaphoresRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Get the list of semaphore entries.
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="startIndex" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>The index to the record at which thereafter the records will be returned.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="maxItems" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>The limit on the number of records returned.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="listAll" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>A value of '1' indicated that the request should cause all the items in the list to be returned, otherwise only successfully completed items are returned.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="CustomServer.listSemaphoresRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>See request.
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="semaphoreList" minOccurs="1" maxOccurs="unbounded">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="semaphore" minOccurs="0" maxOccurs="unbounded">
                <xs:complexType>
                  <xs:attribute name="semaphoreID" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>SemaphoreID (sessionID) to be checked</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="callingID" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>SessionID that started the semaphore.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="sessionID" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>SessionID that is performing the semaphore.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="machine" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>Machine name that is running the semaphore.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="start" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>Timestamp when the semaphore started.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="stop" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>Timestamp when the semaphore completed.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="notify" use="required">
                    <xs:annotation>
                      <xs:documentation>
                        Describes the status of the response:
                        invalid	- The specified semaphoreID is invalid
                        success	- The asynchronous request is still working, no response yet
                        success	- Execution of request finished and was successful
                        failure	- Execution of request finished and returned failed response
                      </xs:documentation>
                    </xs:annotation>
                    <xs:simpleType>
                      <xs:restriction base="xs:string">
                        <xs:enumeration value="invalid">
                          <xs:annotation>
                            <xs:documentation>
                            </xs:documentation>
                          </xs:annotation>
                        </xs:enumeration>
                        <xs:enumeration value="working">
                          <xs:annotation>
                            <xs:documentation>
                            </xs:documentation>
                          </xs:annotation>
                        </xs:enumeration>
                        <xs:enumeration value="success">
                          <xs:annotation>
                            <xs:documentation>
                            </xs:documentation>
                          </xs:annotation>
                        </xs:enumeration>
                        <xs:enumeration value="failure">
                          <xs:annotation>
                            <xs:documentation>
                            </xs:documentation>
                          </xs:annotation>
                        </xs:enumeration>
                      </xs:restriction>
                    </xs:simpleType>
                  </xs:attribute>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
            <xs:attribute name="listCount" type="xs:string" use="required">
              <xs:annotation>
                <xs:documentation>The total count of records, including those returned.</xs:documentation>
              </xs:annotation>
            </xs:attribute>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of request failed
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation>
                </xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation>
                </xs:documentation>
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:complexType name="customDataType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>XML data that we want to add to the dataRequest response.</xs:documentation>
    </xs:annotation>
  </xs:complexType>
  <xs:complexType name="xsltRequestsType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Requests to execute.  These requests responses would the only ones being returned.  You may provide xsl within the requests to pull data from the dataRequests block.</xs:documentation>
    </xs:annotation>
  </xs:complexType>
  <xs:complexType name="dataRequestsType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>This would contain all the requests we would like to execute.</xs:documentation>
    </xs:annotation>
  </xs:complexType>
</xs:schema>