﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="html" indent="yes"/>
  <xsl:template match="/">
    <html>
      <body>
        <xsl:for-each select="email">
			<xsl:call-template name="buildDiv">
				<xsl:with-param name="fontFamily" select="body/content/header/@fontFamily"/>
				<xsl:with-param name="fontSize" select="body/content/header/@fontSize"/>
				<xsl:with-param name="text" select="body/content/header"/>
		    </xsl:call-template>
			<br/>
			<xsl:variable name="paragraphsFontFamily" select="body/content/paragraphs/@fontFamily"/>
			<xsl:variable name="paragraphsFontSize" select="body/content/paragraphs/@fontSize"/>
			<xsl:for-each select="body/content/paragraphs/paragraph">
				<xsl:call-template name="buildDiv">
					<xsl:with-param name="fontFamily" select="$paragraphsFontFamily"/>
					<xsl:with-param name="fontSize" select="$paragraphsFontSize"/>
					<xsl:with-param name="text" select="."/>
				</xsl:call-template>
				<br/>
			</xsl:for-each>
			<xsl:variable name="imageSrc" select="body/content/image" />
			<img src="{$imageSrc}" style="margin-left: 0px; margin-right: 0px; margin-top: 5px; margin-bottom: 15px"/>
			<xsl:call-template name="buildDiv">
				<xsl:with-param name="fontFamily" select="body/content/footer/@fontFamily"/>
				<xsl:with-param name="fontSize" select="body/content/footer/@fontSize"/>
				<xsl:with-param name="text" select="body/content/footer"/>
			</xsl:call-template>
		</xsl:for-each> 
      </body>
    </html>
  </xsl:template>
  
  <xsl:template name="buildDiv">
	<xsl:param name="fontFamily"/>
	<xsl:param name="fontSize"/>
	<xsl:param name="text"/>
	<div style="font-family: {$fontFamily};font-size: {$fontSize}; color: black">
		<xsl:value-of select="$text"/>
	</div>
  </xsl:template>
</xsl:stylesheet>
