﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes"/>

	<xsl:key name="quoteDueDateKey" match="//Schedules/Schedule/Policy/Receivable/Installments/Installment/DueDate/text()" use="." />
	<xsl:key name="chargeDueDateKey" match="//Schedules/Schedule/Charges/Installments/Installment/DueDate/text()" use="." />
	<xsl:template match="/">
		<Schedules>
			<xsl:for-each select="//Schedules/Schedule">
				<Schedule>
					<xsl:attribute name="AccountPlanCode">
						<xsl:value-of select="@AccountPlanCode"/>
					</xsl:attribute>
					<xsl:attribute name="PolicyPlanCode">
						<xsl:value-of select="@PolicyPlanCode"/>
					</xsl:attribute>
					
					<xsl:variable name="varAccountPlan" select="@AccountPlanCode"/>
					<xsl:variable name="varPolicyPlan" select="@PolicyPlanCode"/>
					
					<Installments>
						<xsl:for-each select="current()">
							
								<Based>quote</Based>
								<xsl:for-each select="//Schedule/Policy/Receivable/Installments/Installment/DueDate/text()[generate-id() = generate-id(key('quoteDueDateKey',.)[1])] |
										  //Schedules/Schedule/Charges/Installments/Installment/DueDate/text()[generate-id() = generate-id(key('chargeDueDateKey',.)[0])]">

									<xsl:call-template name="BuildQuoteBasedInstallments">
										<xsl:with-param name="paramAccountPlan" select="$varAccountPlan"></xsl:with-param>
										<xsl:with-param name="paramPolicyPlan" select="$varPolicyPlan"></xsl:with-param>
									</xsl:call-template>

								</xsl:for-each>
															
						</xsl:for-each>
					</Installments>
				</Schedule>
			</xsl:for-each>
		</Schedules>
	</xsl:template>

	<xsl:template name="BuildQuoteBasedInstallments">
		<xsl:param name="paramAccountPlan" select="varAccountPlan"></xsl:param>
		<xsl:param name="paramPolicyPlan" select="varPolicyPlan"></xsl:param>
		<xsl:variable name="dueDate" select="."/>
		<xsl:variable name="installmentDate" select="."/>

		<xsl:if test="//Schedules/Schedule[@AccountPlanCode=$paramAccountPlan and @PolicyPlanCode=$paramPolicyPlan]/Policy/Receivable/Installments/Installment[DueDate=$dueDate]">
			<Installment>
				<xsl:for-each select="//Schedules/Schedule[@AccountPlanCode=$paramAccountPlan and @PolicyPlanCode=$paramPolicyPlan]/Policy/Receivable/Installments/Installment[DueDate=$dueDate]  | 
							  //Schedules/Schedule[@AccountPlanCode=$paramAccountPlan and @PolicyPlanCode=$paramPolicyPlan]/Charges/Installments/Installment[DueDate=$dueDate]">
					<xsl:if test="position()=1">
						<InstallmentDate>
							<xsl:value-of select="InstallmentDate"/>
						</InstallmentDate>
						<DueDate>
							<xsl:value-of select="$dueDate"/>
						</DueDate>
					</xsl:if>
					<xsl:call-template name="BuildQuoteSubInstallments"/>
				
				</xsl:for-each>
				<xsl:call-template name="BuildQuoteBasedCommittedInstallments">
					<xsl:with-param name="dueDate">
						<xsl:value-of select="$dueDate"/>
					</xsl:with-param>
				</xsl:call-template>
			</Installment>
		</xsl:if>
		<xsl:call-template name="BuildQuoteBasedCommittedInstallments">
			<xsl:with-param name="installmentDate">
				<xsl:value-of select="$installmentDate"/>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>


	<xsl:template name="BuildQuoteSubInstallments">

		<QuoteSubInstallment>
			<SubInstallmentAmount>
				<xsl:value-of select="InstallmentNetAmount"/>
			</SubInstallmentAmount>
		</QuoteSubInstallment>
	</xsl:template>

	<xsl:template name="BuildQuoteBasedCommittedInstallments">
		<xsl:param name="dueDate"/>
		<xsl:param name="installmentDate"/>
		<xsl:for-each select="//InstallmentSchedule/InstallmentDate/SubGroup">
			<xsl:if test="../SubGroup/Installment[@DueDate = $dueDate]">
				<xsl:call-template name="BuildCommittedSubGroups"/>
			</xsl:if>
		</xsl:for-each>
		<xsl:for-each select="//InstallmentSchedule/InstallmentDate/SubGroup">
			<xsl:if test="../SubGroup/Installment[@InstallmentDate = $installmentDate]">
				<xsl:call-template name="BuildCommittedSubGroups"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="BuildCommittedBasedInstallments">
		<Installment>
			<DueDate>
				<xsl:value-of select="SubGroup/Installment/@DueDate"/>
			</DueDate>
			<xsl:for-each select="SubGroup">
				<xsl:call-template name="BuildCommittedSubGroups"/>
			</xsl:for-each>
			<InstallmentDate>
				<xsl:value-of select="SubGroup/Installment/@InstallmentDate"/>
			</InstallmentDate>
			<xsl:for-each select="SubGroup">
				<xsl:call-template name="BuildCommittedSubGroups"/>
			</xsl:for-each>
		</Installment>
	</xsl:template>

	<xsl:template name="BuildCommittedSubGroups">
		<CommittedSubGroup>
			<SubGroupAmount>
				<xsl:value-of select="@InstallmentAmount"/>
			</SubGroupAmount>
			<xsl:for-each select="Installment">
				<xsl:call-template name="BuildCommittedSubInstallments"/>
			</xsl:for-each>
		</CommittedSubGroup>
	</xsl:template>

	<xsl:template name="BuildCommittedSubInstallments">
		<CommittedSubInstallment>
			<SubInstallmentAmount>
				<xsl:value-of select="@InstallmentAmount"/>
			</SubInstallmentAmount>
			<SubInstallmentClosedToCreditAmount>
				<xsl:value-of select="@ClosedToCredit"/>
			</SubInstallmentClosedToCreditAmount>
			<SubInstallmentClosedWriteOffAmount>
				<xsl:value-of select="@ClosedWriteOff"/>
			</SubInstallmentClosedWriteOffAmount>
			<SubInstallmentClosedRedistributedAmount>
				<xsl:value-of select="@ClosedRedistributed"/>
			</SubInstallmentClosedRedistributedAmount>
			<SubInstallmentTransactionTypeCode>
				<xsl:value-of select="@TransactionTypeCode"/>
			</SubInstallmentTransactionTypeCode>
			<SubInstallmentTransactionDate>
				<xsl:value-of select="@TransactionDate"/>
			</SubInstallmentTransactionDate>
		</CommittedSubInstallment>
	</xsl:template>

</xsl:stylesheet>
