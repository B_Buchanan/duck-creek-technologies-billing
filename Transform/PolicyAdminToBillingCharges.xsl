﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1">
	<xsl:output omit-xml-declaration="yes" />

	<xsl:template match="/root">
		<xsl:element name="Account">
			<xsl:element name="AccountClass" />
			<xsl:choose>
				<xsl:when test="normalize-space(_BillingInputDoc/BillingKeys/AccountId) != ''">
					<xsl:element name="AccountId">
						<xsl:value-of select="_BillingInputDoc/BillingKeys/AccountId" />
					</xsl:element>
				</xsl:when>
				<xsl:when test="normalize-space(_BillingInputDoc/BillingKeys/AttachToAccountId) != ''">
					<xsl:element name="AccountId">
						<xsl:value-of select="_BillingInputDoc/BillingKeys/AttachToAccountId"/>
					</xsl:element>
				</xsl:when>
				<xsl:otherwise>
					<xsl:element name="AccountId" />
				</xsl:otherwise>
			</xsl:choose>
			<xsl:element name="AccountLockingTS" />
			<xsl:choose>
				<xsl:when test="normalize-space(_BillingInputDoc/BillingKeys/AttachToAccountReference) != ''">
					<xsl:element name="AccountReference">
						<xsl:value-of select="_BillingInputDoc/BillingKeys/AttachToAccountReference" />
					</xsl:element>
				</xsl:when>
				<xsl:otherwise>
					<xsl:element name="AccountReference" />
				</xsl:otherwise>
			</xsl:choose>
			<xsl:element name="AccountStatus" />
			<xsl:element name="AccountTypeCode">
				<xsl:value-of select="_BillingWorkingDoc/BillAccount.updateAccountRq/Account/AccountTypeCode" />
			</xsl:element>
			<xsl:element name="CollectionMethod">
				<xsl:value-of select="_BillingInputDoc/BillingKeys/CollectionMethod" />
			</xsl:element>
			<xsl:element name="CurrencyCulture">
				<xsl:value-of select="_BillingInputDoc/Account/CurrencyCulture" />
			</xsl:element>
			<xsl:element name="HoldEndDate" />
			<xsl:element name="HoldEventId" />
			<xsl:element name="HoldReason" />
			<xsl:element name="HoldReasonCode" />
			<xsl:element name="HoldStartDate" />
			<xsl:element name="HoldTypeCode" />
			<xsl:element name="NSFCounter">
				<xsl:text><![CDATA[0]]></xsl:text>
			</xsl:element>
			<xsl:element name="OutstandingBalance">
				<xsl:value-of select="sum(_BillingInputDoc/BillItems/BillItem/ItemAmount)" />
			</xsl:element>
			<xsl:element name="ProcessingOrgUnitCode">
				<xsl:value-of select="_BillingInputDoc/BillingKeys/AccountProcessingOrgUnitCode"/>
			</xsl:element>
			<xsl:element name="TargetDueDays">
				<xsl:value-of select="_BillingInputDoc/BillingKeys/TargetDueDay"/>
			</xsl:element>
			<xsl:element name="AccountConfig">
				<xsl:copy-of select="_BillingInputDoc/Account/AccountConfig" />
			</xsl:element>
			<xsl:copy-of select="_BillingInputDoc/Account/AccountExtendedData" />
			<xsl:element name="ExistingCharges" />
			<xsl:element name="Installments" />
			<xsl:element name="Payments" />
			<xsl:element name="PolicyTerms">
				<xsl:choose>
					<xsl:when test="_BillingWorkingDoc/_requests/BillPolicyTerm.getPolicyTermRq/PolicyTerm">
						<xsl:apply-templates select="_BillingWorkingDoc/_requests/BillPolicyTerm.getPolicyTermRq/PolicyTerm" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="_BillingInputDoc/PolicyTerm" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template match="_BillingWorkingDoc/_requests/BillPolicyTerm.getPolicyTermRq/PolicyTerm">
		<xsl:element name="PolicyTerm">
			<xsl:element name="AutoRescindEquityDate" />
			<xsl:element name="ExpirationActivityDate" />
			<xsl:element name="HoldEndDate" />
			<xsl:element name="HoldReasonCode" />
			<xsl:element name="HoldStartDate" />
			<xsl:element name="HoldTypeCode" />
			<xsl:element name="IssueSystemPolicyId" />
			<xsl:element name="OutstandingBalance" />
			<xsl:element name="PolicyAggregationReference" />
			<xsl:element name="PolicyIssueSystemCode">
				<xsl:value-of select="PolicyIssueSystemCode" />
			</xsl:element>
			<xsl:element name="PolicyLineOfBusinessCode">
				<xsl:value-of select="PolicyLineOfBusinessCode" />
			</xsl:element>
			<xsl:element name="PolicyMasterCompanyCode">
				<xsl:value-of select="PolicyMasterCompanyCode" />
			</xsl:element>
			<xsl:element name="PolicyProductCode">
				<xsl:value-of select="PolicyProductCode" />
			</xsl:element>
			<xsl:element name="PolicyReference">
				<xsl:value-of select="PolicyReference" />
			</xsl:element>
			<xsl:element name="PolicyStateCode">
				<xsl:value-of select="PolicyStateCode" />
			</xsl:element>
			<xsl:element name="PolicyTermConfigTemplate">
				<xsl:value-of select="PolicyTermConfigTemplate" />
			</xsl:element>
			<xsl:element name="PolicyTermEffectiveDate">
				<xsl:value-of select="PolicyTermEffectiveDate" />
			</xsl:element>
			<xsl:element name="PolicyTermExpirationDate">
				<xsl:value-of select="PolicyTermExpirationDate" />
			</xsl:element>
			<xsl:element name="PolicyTermId">
				<xsl:value-of select="PolicyTermId" />
			</xsl:element>
			<xsl:element name="PolicyTermLockingTS">
				<xsl:value-of select="PolicyTermLockingTS" />
			</xsl:element>
			<xsl:element name="PolicyTermStatusCode">
				<xsl:value-of select="PolicyTermStatusCode" />
			</xsl:element>
			<xsl:element name="PrimaryAccountId">
				<xsl:value-of select="PrimaryAccountId" />
			</xsl:element>
			<xsl:element name="RenewalAccountId" />
			<xsl:element name="RenewalConfigTemplate" />
			<xsl:element name="SplitBillIndicator">
				<xsl:value-of select="SplitBillIndicator" />
			</xsl:element>
			<xsl:element name="PolicyTermConfig">
				<xsl:copy-of select="_BillingWorkingDoc/_requests/BillPolicyTerm.getPolicyTermRq/PolicyTerm/PolicyTermConfig" />
			</xsl:element>
			<xsl:copy-of select="_BillingWorkingDoc/_requests/BillPolicyTerm.getPolicyTermRq/PolicyTerm/Transactions" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="_BillingInputDoc/PolicyTerm">
		<xsl:element name="PolicyTerm">
			<xsl:element name="AutoRescindEquityDate" />
			<xsl:element name="ExpirationActivityDate" />
			<xsl:element name="HoldEndDate" />
			<xsl:element name="HoldReasonCode" />
			<xsl:element name="HoldStartDate" />
			<xsl:element name="HoldTypeCode" />
			<xsl:element name="IssueSystemPolicyId" />
			<xsl:element name="OutstandingBalance" />
			<xsl:element name="PolicyAggregationReference" />
			<xsl:element name="PolicyIssueSystemCode">
				<xsl:value-of select="PolicyIssueSystemCode" />
			</xsl:element>
			<xsl:element name="PolicyLineOfBusinessCode">
				<xsl:value-of select="PolicyLineOfBusinessCode" />
			</xsl:element>
			<xsl:element name="PolicyMasterCompanyCode">
				<xsl:value-of select="PolicyMasterCompanyCode" />
			</xsl:element>
			<xsl:element name="PolicyProductCode">
				<xsl:value-of select="PolicyProductCode" />
			</xsl:element>
			<xsl:element name="PolicyReference">
				<xsl:value-of select="PolicyReference" />
			</xsl:element>
			<xsl:element name="PolicyStateCode">
				<xsl:value-of select="PolicyStateCode" />
			</xsl:element>
			<xsl:element name="PolicyTermConfigTemplate">
				<xsl:value-of select="PolicyTermConfigTemplate" />
			</xsl:element>
			<xsl:element name="PolicyTermEffectiveDate">
				<xsl:value-of select="PolicyTermEffectiveDate" />
			</xsl:element>
			<xsl:element name="PolicyTermExpirationDate">
				<xsl:value-of select="PolicyTermExpirationDate" />
			</xsl:element>
			<xsl:element name="PolicyTermId">
				<xsl:value-of select="PolicyTermId" />
			</xsl:element>
			<xsl:element name="PolicyTermLockingTS">
				<xsl:value-of select="PolicyTermLockingTS" />
			</xsl:element>
			<xsl:element name="PolicyTermStatusCode">
				<xsl:value-of select="PolicyTermStatusCode" />
			</xsl:element>
			<xsl:element name="PrimaryAccountId">
				<xsl:value-of select="PrimaryAccountId" />
			</xsl:element>
			<xsl:element name="RenewalAccountId" />
			<xsl:element name="RenewalConfigTemplate" />
			<xsl:element name="SplitBillIndicator">
				<xsl:value-of select="SplitBillIndicator" />
			</xsl:element>
			<xsl:element name="PolicyTermConfig">
				<xsl:copy-of select="PolicyTermConfig" />
			</xsl:element>
			<xsl:copy-of select="Transactions" />
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>