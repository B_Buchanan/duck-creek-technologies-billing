﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-16" indent="yes" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<AccountActivityList>
			<xsl:apply-templates select="//Transaction|//Payment|//PaymentWriteOff|//ReceivableWriteOff|//Charge|//Disbursement|//Balance|//AssociatedParties|//Parties"/>
		</AccountActivityList>
	</xsl:template>

	<xsl:template match="Transaction">
		<AccountActivity>
			<xsl:call-template name="buildActivityWithParms">
				<xsl:with-param name="ActivityTypeCode" select="'POL'"/>
				<xsl:with-param name="TransactionReference" select="TransactionReference"/>
				<xsl:with-param name="TransactionTypeCode" select="TransactionTypeCode"/>
				<xsl:with-param name="TransactionReasonCode" select="ReceivableTypeCode"/>
				<xsl:with-param name="TransactionDate" select="TransactionDate"/>
				<xsl:with-param name="EffectiveDate" select="ItemEffectiveDate"/>
				<xsl:with-param name="ExpirationDate" select="ItemExpirationDate"/>
				<xsl:with-param name="PolicyReference" select="PolicyReference"/>
				<xsl:with-param name="Amount" select="ItemAmount"/>
				<xsl:with-param name="CurrencyCulture" select="CurrencyCulture"/>
				<xsl:with-param name="PolicyTermId" select="PolicyTermId"/>
				<xsl:with-param name="UserId" select="LastUpdatedUserId"/>
				<xsl:with-param name="ActivityDescription" select="Description"/>
			</xsl:call-template>
		</AccountActivity>
	</xsl:template>

	<xsl:template match="Payment">
		<AccountActivity>
			<xsl:call-template name="buildActivityWithParms">
				<xsl:with-param name="ActivityTypeCode" select="TransactionTypeCode"/>
				<xsl:with-param name="TransactionReference" select="PaymentReference"/>
				<xsl:with-param name="TransactionTypeCode"/>
				<xsl:with-param name="TransactionReasonCode"/>
				<xsl:with-param name="TransactionDate" select="TransactionDate"/>
				<xsl:with-param name="EffectiveDate"/>
				<xsl:with-param name="ExpirationDate"/>
				<xsl:with-param name="PolicyReference" select="PolicyReference"/>
				<xsl:with-param name="PaymentId" select="PaymentId"/>
				<xsl:with-param name="Amount">
					<xsl:choose>
						<xsl:when test="TransactionTypeCode = 'PYMT'">
							<xsl:value-of select="Amount * -1"></xsl:value-of>
						</xsl:when>
						<xsl:when test="TransactionTypeCode = 'RPMT'">
							<xsl:value-of select="Amount"></xsl:value-of>
						</xsl:when>
						<xsl:when test="TransactionTypeCode = 'TRIN'">
							<xsl:value-of select="Amount * -1"></xsl:value-of>
						</xsl:when>
						<xsl:when test="TransactionTypeCode = 'TROU'">
							<xsl:value-of select="Amount"></xsl:value-of>
						</xsl:when>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="CurrencyCulture" select="CurrencyCulture"/>
				<xsl:with-param name="PolicyTermId" select="PolicyTermId"/>
				<xsl:with-param name="UserId" select="UserId"/>
			</xsl:call-template>
		</AccountActivity>
	</xsl:template>

	<xsl:template match="ReceivableWriteOff">
		<AccountActivity>
			<xsl:call-template name="buildActivityWithParms">
				<xsl:with-param name="ActivityTypeCode" select="TransactionTypeCode"/>
				<xsl:with-param name="TransactionReference"/>
				<xsl:with-param name="TransactionTypeCode"/>
				<xsl:with-param name="TransactionReasonCode">
					<xsl:choose>
						<xsl:when test="TransactionTypeCode = 'MRWO'">
							<xsl:value-of select="WriteReasonCode"></xsl:value-of>
						</xsl:when>
						<xsl:when test="TransactionTypeCode = 'RWOR'">
							<xsl:value-of select="ReversalReasonCode"></xsl:value-of>
						</xsl:when>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="TransactionDate">
					<xsl:choose>
						<xsl:when test="TransactionTypeCode = 'MRWO'">
							<xsl:value-of select="WriteOffProcessedDate"></xsl:value-of>
						</xsl:when>
						<xsl:when test="TransactionTypeCode = 'RWOR'">
							<xsl:value-of select="ReversalDate"></xsl:value-of>
						</xsl:when>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="EffectiveDate"/>
				<xsl:with-param name="ExpirationDate"/>
				<xsl:with-param name="PolicyReference" select="PolicyReference"/>
				<xsl:with-param name="Amount">
					<xsl:choose>
						<xsl:when test="TransactionTypeCode = 'MRWO'">
							<xsl:value-of select="WriteOffAmount * -1"></xsl:value-of>
						</xsl:when>
						<xsl:when test="TransactionTypeCode = 'RWOR'">
							<xsl:value-of select="WriteOffAmount"></xsl:value-of>
						</xsl:when>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="CurrencyCulture" select="CurrencyCulture"/>
				<xsl:with-param name="PolicyTermId" select="PolicyTermId"/>
				<xsl:with-param name="UserId" select="LastUpdatedUserId"/>
			</xsl:call-template>
		</AccountActivity>
	</xsl:template>

	<xsl:template match="PaymentWriteOff">
		<AccountActivity>
			<xsl:call-template name="buildActivityWithParms">
				<xsl:with-param name="ActivityTypeCode" select="TransactionTypeCode"/>
				<xsl:with-param name="TransactionReference"/>
				<xsl:with-param name="TransactionTypeCode"/>
				<xsl:with-param name="TransactionReasonCode">
					<xsl:choose>
						<xsl:when test="TransactionTypeCode = 'WRTO'">
							<xsl:value-of select="WriteReasonCode"></xsl:value-of>
						</xsl:when>
						<xsl:when test="TransactionTypeCode = 'WORV'">
							<xsl:value-of select="ReversalReasonCode"></xsl:value-of>
						</xsl:when>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="TransactionDate">
					<xsl:choose>
						<xsl:when test="TransactionTypeCode = 'WRTO'">
							<xsl:value-of select="WriteOffProcessedDate"></xsl:value-of>
						</xsl:when>
						<xsl:when test="TransactionTypeCode = 'WORV'">
							<xsl:value-of select="ReversalDate"></xsl:value-of>
						</xsl:when>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="EffectiveDate"/>
				<xsl:with-param name="ExpirationDate"/>
				<xsl:with-param name="PolicyReference"/>
				<xsl:with-param name="Amount">
					<xsl:choose>
						<xsl:when test="TransactionTypeCode = 'WRTO'">
							<xsl:value-of select="WriteOffAmount"></xsl:value-of>
						</xsl:when>
						<xsl:when test="TransactionTypeCode = 'WORV'">
							<xsl:value-of select="WriteOffAmount * -1"></xsl:value-of>
						</xsl:when>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="CurrencyCulture" select="CurrencyCulture"/>
				<xsl:with-param name="PolicyTermId"/>
				<xsl:with-param name="UserId" select="LastUpdatedUserId"/>
			</xsl:call-template>
		</AccountActivity>
	</xsl:template>

	<xsl:template match="Charge">
			<AccountActivity>
			<xsl:call-template name="buildActivityWithParms">
				<xsl:with-param name="ActivityTypeCode" select="'CHRG'"/>
				<xsl:with-param name="TransactionReference"/>
				<xsl:with-param name="TransactionTypeCode" select="ChargeTypeCode"/>
				<xsl:with-param name="TransactionReasonCode"/>
				<xsl:with-param name="TransactionDate" select="PostedDate"/>
				<xsl:with-param name="EffectiveDate"/>
				<xsl:with-param name="ExpirationDate"/>
				<xsl:with-param name="PolicyReference"/>
				<xsl:with-param name="Amount" select="ChargeAmount"/>
				<xsl:with-param name="CurrencyCulture" select="CurrencyCulture"/>
				<xsl:with-param name="PolicyTermId"/>
				<xsl:with-param name="UserId" select="LastUpdatedUserId"/>
			</xsl:call-template>
		</AccountActivity>
	</xsl:template>

	<xsl:template match="Disbursement">
			<AccountActivity>
			<xsl:call-template name="buildActivityWithParms">
				<xsl:with-param name="ActivityTypeCode" select="'DISB'"/>
				<xsl:with-param name="TransactionReference"/>
				<xsl:with-param name="TransactionTypeCode"/>
				<xsl:with-param name="TransactionReasonCode"/>
				<xsl:with-param name="TransactionDate" select="DisbursementDate"/>
				<xsl:with-param name="EffectiveDate"/>
				<xsl:with-param name="ExpirationDate"/>
				<xsl:with-param name="PolicyReference"/>
				<xsl:with-param name="Amount" select="DisbursementAmount"/>
				<xsl:with-param name="CurrencyCulture" select="CurrencyCulture"/>
				<xsl:with-param name="PolicyTermId"/>
				<xsl:with-param name="UserId" select="DisbursementUserId"/>
				<xsl:with-param name="ActivityDescription" select="DisbursementDescription"/>
			</xsl:call-template>
		</AccountActivity>
	</xsl:template>

	<xsl:template name="buildActivityWithParms">
		<xsl:param name="ActivityTypeCode"/>
		<xsl:param name="TransactionReference"/>
		<xsl:param name="TransactionTypeCode"/>
		<xsl:param name="TransactionReasonCode"/>
		<xsl:param name="TransactionDate"/>
		<xsl:param name="EffectiveDate"/>
		<xsl:param name="ExpirationDate"/>
		<xsl:param name="PolicyReference"/>
		<xsl:param name="PaymentId"/>
		<xsl:param name="Amount"/>
		<xsl:param name="CurrencyCulture"/>
		<xsl:param name="PolicyTermId"/>
		<xsl:param name="UserId"/>
		<xsl:param name="ActivityDescription"/>
		<ActivityTypeCode>
			<xsl:value-of select="$ActivityTypeCode"/>
		</ActivityTypeCode>
		<ActivityTransactionGroupReference>
			<xsl:value-of select="$TransactionReference"/>
		</ActivityTransactionGroupReference>
		<TransactionTypeCode>
			<xsl:value-of select="$TransactionTypeCode"/>
		</TransactionTypeCode>
		<TransactionReasonCode>
			<xsl:value-of select="$TransactionReasonCode"/>
		</TransactionReasonCode>
		<TransactionDate>
			<xsl:value-of select="$TransactionDate"/>
		</TransactionDate>
		<EffectiveDate>
			<xsl:value-of select="$EffectiveDate"/>
		</EffectiveDate>
		<ExpirationDate>
			<xsl:value-of select="$ExpirationDate"/>
		</ExpirationDate>
		<PolicyReference>
			<xsl:value-of select="$PolicyReference"/>
		</PolicyReference>
		<PaymentId>
			<xsl:value-of select="$PaymentId"/>
		</PaymentId>
		<Amount>
			<xsl:value-of select="$Amount"/>
		</Amount>
		<CurrencyCulture>
			<xsl:value-of select="$CurrencyCulture"/>
		</CurrencyCulture>
		<PolicyTermId>
			<xsl:value-of select="$PolicyTermId"/>
		</PolicyTermId>
		<UserId>
			<xsl:value-of select="$UserId"/>
		</UserId>
		<Description>
			<xsl:value-of select="$ActivityDescription"/>
		</Description>
	</xsl:template>


	<xsl:template match="Balance">
		<AccountBalance>
			<StartingBalance>
				<xsl:value-of select="StartingBalance"/>
			</StartingBalance>
			<PendingDisbursements>
				<xsl:value-of select="PendingDisbursements"/>
			</PendingDisbursements>
			<SuspenseBalance>
				<xsl:value-of select="SuspenseBalance"/>
			</SuspenseBalance>
			<CurrencyCulture>
				<xsl:value-of select="CurrencyCulture"/>
			</CurrencyCulture>
		</AccountBalance>
	</xsl:template>

	<xsl:template match="AssociatedParties">
		<AssociatedParties>
			<xsl:for-each select="Party">
				<xsl:call-template name="PartyRow"/>
			</xsl:for-each>
		</AssociatedParties>
	</xsl:template>

	<xsl:template name="PartyRow">
		<Party>
			<PartyId>
				<xsl:value-of select="@PartyId"/>
			</PartyId>
			<Role>
				<xsl:value-of select="@Role"/>
			</Role>
			<Description>
				<xsl:value-of select="@Description"/>
			</Description>
			<ObjectId>
				<xsl:value-of select="@ObjectId"/>
			</ObjectId>
		</Party>
	</xsl:template>

	<xsl:template match="Parties">
		<Parties>
			<xsl:copy-of select="node()"/>
		</Parties>
	</xsl:template>
</xsl:stylesheet>