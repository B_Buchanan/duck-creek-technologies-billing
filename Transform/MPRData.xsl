﻿
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="PolicyTermExtendedData.xsl"/>
	<xsl:output omit-xml-declaration="yes"/>

	<!-- used for setup from the policy term - Transactions will enter through Receivable Message
	Both processes call into PolicyTermExtendedData for shared data setup -->
	<xsl:template match="/">
		<xsl:apply-templates select="root/session/data/policy"></xsl:apply-templates>
	</xsl:template>

	<xsl:template match="policy">
		<xsl:call-template name="PolicyTermExtendedData"/>
	</xsl:template>
</xsl:stylesheet>