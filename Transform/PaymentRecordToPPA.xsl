﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes"/>

	<xsl:param name="merchantId"/>
	<xsl:param name="transactionId"/>

	<xsl:template match="PaymentRecord">
		<requestMessage>
			<xsl:call-template name="merchantData"/>
			<xsl:apply-templates select="PaymentMethodDetail/*"/>
			<xsl:call-template name="requestServices"/>
		</requestMessage> 
	</xsl:template>

	<xsl:template name="merchantData">
		<merchantID><xsl:value-of select="$merchantId"/></merchantID>
		<merchantReferenceCode><xsl:value-of select="$transactionId"/></merchantReferenceCode>
	</xsl:template>

	<xsl:template name="billTo">
		<billTo>
			<firstName><xsl:value-of select="BillToFirstName"/></firstName>
			<lastName><xsl:value-of select="BillToLastName"/></lastName>
			<street1><xsl:value-of select="BillToAddressLine1"/></street1>
			<city><xsl:value-of select="BillToCity"/></city>
			<state><xsl:value-of select="BillToState"/></state>
			<postalCode><xsl:value-of select="BillToPostalCode"/></postalCode>
			<country><xsl:value-of select="BillToCountry"/></country>
			<email><xsl:value-of select="BillToEmail"/></email>
		</billTo>
	</xsl:template>

	<xsl:template name="purchaseTotals">
		<purchaseTotals>
			<currency>USD</currency>
			<!--
			<currency><xsl:value-of select="CurrencyCulture"/></currency>
			-->
			<grandTotalAmount><xsl:value-of select="../../Amount"/></grandTotalAmount>
		</purchaseTotals>
	</xsl:template>
	
	<xsl:template match="CreditCard">
		<xsl:call-template name="billTo"/>
		<xsl:call-template name="purchaseTotals"/>
		<card>
			<accountNumber><xsl:value-of select="CardNumber"/></accountNumber>
			<expirationMonth><xsl:value-of select="ExpiryMonth"/></expirationMonth>
			<expirationYear><xsl:value-of select="ExpiryYear"/></expirationYear>
		</card>
	</xsl:template>

	<xsl:template match="Check">
		<xsl:call-template name="billTo"/>
		<xsl:call-template name="purchaseTotals"/>
		<check>
			<fullName><xsl:value-of select="LastName"/></fullName>
			<accountNumber><xsl:value-of select="AccountNumber"/></accountNumber>
			<bankTransitNumber><xsl:value-of select="TransitNumber"/></bankTransitNumber>
			<checkNumber><xsl:value-of select="CheckNumber"/></checkNumber>
		</check>
	</xsl:template>

	<xsl:template name="requestServices">
		<ccAuthService run="true"/>
		<ccCaptureService run="true"/>
	</xsl:template>

</xsl:stylesheet>
