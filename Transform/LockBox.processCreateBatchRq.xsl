﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl" xmlns:LockBoxBatch="urn:LockBoxBatch">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="@* | node()">
      <BillingCashManagement.processCreateBatchRq>
        <PaymentBatchRecord>
          <BatchNumber>
            <xsl:value-of select="LockBoxBatch:getBatchNumber()"/>
          </BatchNumber>
          <ControlTotal>0</ControlTotal>
          <Description>BHHC LockBox Transmission <xsl:value-of select="LockBoxBatch:getProcessedDate()"/>
          </Description>
          <EntryUserId>Admin</EntryUserId>
          <CurrencyCulture>en-US</CurrencyCulture>
        </PaymentBatchRecord>
      </BillingCashManagement.processCreateBatchRq>
    </xsl:template>

</xsl:stylesheet>
