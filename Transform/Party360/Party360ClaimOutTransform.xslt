﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Util="urn:Util" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs msxsl xsl Util">
  <xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>
  <xsl:template match="server/responses">
    <Claims>
          <xsl:for-each select="ClaimsComponents.getClaimDetailsRs">
              <ClaimDetails>
                <xsl:call-template name="ClaimDetails" />
              </ClaimDetails>
          </xsl:for-each>
    </Claims>
  </xsl:template>


  <xsl:template name="claimsdetailRoles" >
    <RoleCodes>
      <xsl:for-each select="ParticipantRoles">
        <xsl:for-each select="ParticipantRole">
	  <xsl:sort select="." data-type="text" order="ascending"/>
          <Role>
            <RoleCode>
              <xsl:value-of select="ParticipantRole"/>
            </RoleCode>
          </Role>

        </xsl:for-each>
      </xsl:for-each>
      <xsl:if test="position()!=last()">
        <xsl:text>,</xsl:text>
      </xsl:if>
    </RoleCodes>
  </xsl:template>



  <!--Claim Involvements Templates-->
  

  <xsl:template name="ClaimRoleCodes" >
    <xsl:for-each select="Roles/RoleCode">
      <RoleCode>
        <xsl:value-of select="current()"/>
      </RoleCode>
    </xsl:for-each>
  </xsl:template>

  <!--Claim Details Templates-->
  <xsl:template name="ClaimDetails">
    <xsl:call-template name="ClaimSummary" />
    <CoveredItems>
      <xsl:call-template name="CoveredItems" />
    </CoveredItems>
    <Participants>
      <xsl:call-template name="ClaimParticipants" />
    </Participants>
    <ClaimActivities>
      <xsl:call-template name="ClaimActivities" />
    </ClaimActivities>
  </xsl:template>

  <xsl:template name="ClaimSummary">
    <ClaimId>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Claim/ClaimID"/>
    </ClaimId>
    <ClaimNumber>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Claim/ClaimNumber"/>
    </ClaimNumber>
    <ClaimStatus>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Claim/ClaimStatus"/>
    </ClaimStatus>
    <LineOfBusiness>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Claim/LineOfBusiness"/>
    </LineOfBusiness>
    <PolicyNumber>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Policy/PolicyNumber"/>
    </PolicyNumber>
    <LossDate>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Loss/LossDate"/>
    </LossDate>
    <PolicyID>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Policy/PolicyID"/>
    </PolicyID>
    <WritingCompany>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Claim/HandlingCompany"/>
    </WritingCompany>
    <PolicyNumber>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Policy/PolicyNumber"/>
    </PolicyNumber>
	<TotalLossIncurred>
      <xsl:value-of select="ClaimSummaryAndEventHistory/ClaimFinancialSummary/LossIncurredAmount"/>
    </TotalLossIncurred>
	<TotalLossReserve>
      <xsl:value-of select="ClaimSummaryAndEventHistory/ClaimFinancialSummary/LossReserveAmount"/>
    </TotalLossReserve>
  </xsl:template>

  <xsl:template name="CoveredItems">
	<Items>
	<xsl:for-each select="ClaimSummaryAndEventHistory/CoveredVehicles/VehicleItem">
	  <xsl:if test="Item/CoveredIndicator = 'true'">
        <Item>
          <ClaimID>
            <xsl:value-of select="Item/ClaimID"/>
          </ClaimID>
          <ItemID>
            <xsl:value-of select="Item/ItemID"/>
          </ItemID>
          <ItemType>
            <xsl:value-of select="Item/ItemType"/>
          </ItemType>
          <ItemDescription>
			<xsl:value-of select="concat(ItemVehicleDetail/VehicleDescription,'; ',ItemVehicleDetail/VehicleIdentificationNumberDescription)" />	
		  </ItemDescription>         
        </Item>
		</xsl:if>
      </xsl:for-each>
	<xsl:for-each select="ClaimSummaryAndEventHistory/CoveredProperties/PropertyItem">
	  <xsl:if test="Item/CoveredIndicator = 'true'">
        <Item>
          <ClaimID>
            <xsl:value-of select="Item/ClaimID"/>
          </ClaimID>
          <ItemID>
            <xsl:value-of select="Item/ItemID"/>
          </ItemID>
          <ItemType>
            <xsl:value-of select="Item/ItemType"/>
          </ItemType>
          <ItemDescription>
            <xsl:value-of select="ItemSite/SiteDetailDescription"/>
          </ItemDescription>
        </Item>
		</xsl:if>
      </xsl:for-each>
	<xsl:for-each select="ClaimSummaryAndEventHistory/RiskLocationDetails/LocationItemDetails/LocationItem">
	  <xsl:if test="Item/CoveredIndicator = 'true'">
        <Item>
          <ClaimID>
            <xsl:value-of select="Item/ClaimID"/>
          </ClaimID>
          <ItemID>
            <xsl:value-of select="Item/ItemID"/>
          </ItemID>
          <ItemType>
            <xsl:value-of select="Item/ItemType"/>
          </ItemType>
          <ItemDescription>
            <xsl:value-of select="LocationDescription"/>
          </ItemDescription>
        </Item>
		</xsl:if>
      </xsl:for-each>
	</Items>
  </xsl:template>

  <xsl:template name="ClaimParticipants">
    <xsl:for-each select="ClaimSummaryAndEventHistory/Participants/Participant">
      <Participant>
        <PartyId>
          <xsl:value-of select="ParticipantID"/>
        </PartyId>
        <PartyFullName>
          <xsl:choose>
            <xsl:when test="ParticipantFullName != ''">
              <xsl:value-of select="ParticipantFullName"/>
            </xsl:when>
            <xsl:otherwise>Unknown Participant</xsl:otherwise>
          </xsl:choose>
        </PartyFullName>
        <PartyName>
          <xsl:value-of select="ParticipantNameDetail/LastName"/>
        </PartyName>
        <PartyFirstName>
          <xsl:value-of select="ParticipantNameDetail/FirstName"/>
        </PartyFirstName>
        <PartyMiddleName>
          <xsl:value-of select="ParticipantNameDetail/MiddleName"/>
        </PartyMiddleName>
        <PartyNamePrefix>
          <xsl:value-of select="ParticipantNameDetail/PrefixCode"/>
        </PartyNamePrefix>
        <PartyNameSuffix>
          <xsl:value-of select="ParticipantNameDetail/SuffixCode"/>
        </PartyNameSuffix>
        <!--<PartyTypeCode>
          <xsl:value-of select="Party/PartyTypeCode"/>
        </PartyTypeCode>-->
        <xsl:call-template name="claimsdetailRoles" />
      </Participant>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="ClaimActivities">
    <xsl:for-each select="ClaimSummaryAndEventHistory/ClaimEventsHistory/ClaimEventHistory">
	<xsl:sort select="ClaimHistory/CreateDate" order="descending"/>
      <ClaimActivity>
        <ClaimHistoryID>
          <xsl:value-of select="ClaimHistory/ClaimHistoryID"/>
        </ClaimHistoryID>
        <ClaimID>
          <xsl:value-of select="ClaimHistory/ClaimID"/>
        </ClaimID>
        <ActivityTypeCode>
          <xsl:value-of select="ClaimHistory/EventNumber"/>
        </ActivityTypeCode>
        <Details>
          <xsl:value-of select="ClaimHistory/Description"/>
        </Details>
        <Performer>
          <xsl:value-of select="ClaimPerformerNameAssociation/FullName"/>
        </Performer>
        <Date>
          <xsl:value-of select="ClaimHistory/CreateDate"/>
        </Date>
      </ClaimActivity>
    </xsl:for-each>
  </xsl:template>

  <!--<xsl:template name="ClaimParticipantRoles">
    <RoleCode>
      <xsl:value-of select="ParticipantRole"/>
    </RoleCode>
  </xsl:template>-->

</xsl:stylesheet>