﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Util="urn:Util" xmlns:d2p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl Util">
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="/Filters">
    <GetClaimSummaryByFilterCriteriaRequest xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://claims.accenture.com/2011/01/01/Types">
      <UserName i:nil="true" />
      <ClaimID i:nil="true" />
      <xsl:variable name="ActivityFromDateCount" select="count(./Filter[@Type = 'ActivityFromDate'])"></xsl:variable>
      <xsl:variable name="ActivityToDateCount" select="count(./Filter[@Type = 'ActivityToDate'])"></xsl:variable>
      <xsl:variable name="ActivityType" select="count(./Filter[@Type = 'ActivityType'])"></xsl:variable>
      <xsl:variable name="RetrieveLossData" select="count(./Filter[@Type = 'RetrieveLossData'])"></xsl:variable>
      <xsl:variable name="RetrieveInvolvedItemsData" select="count(./Filter[@Type = 'RetrieveInvolvedItemsData'])"></xsl:variable>
      <xsl:variable name="RetrieveClaimGroupAndClaimClaimGroupAssociationData" select="count(./Filter[@Type = 'RetrieveClaimGroupAndClaimClaimGroupAssociationData'])"></xsl:variable>
      <xsl:variable name="RetrieveClaimClaimAssociationData" select="count(./Filter[@Type = 'RetrieveClaimClaimAssociationData'])"></xsl:variable>
      <xsl:variable name="RetrievePolicySummaryData" select="count(./Filter[@Type = 'RetrievePolicySummaryData'])"></xsl:variable>
      <xsl:variable name="RetrieveLineData" select="count(./Filter[@Type = 'RetrieveLineData'])"></xsl:variable>
      <xsl:variable name="RetrieveParticipantData" select="count(./Filter[@Type = 'RetrieveParticipantData'])"></xsl:variable>
      <xsl:variable name="RetrievePerformerData" select="count(./Filter[@Type = 'RetrievePerformerData'])"></xsl:variable>
      <xsl:variable name="RetrieveFinancialSummaryData" select="count(./Filter[@Type = 'RetrieveFinancialSummaryData'])"></xsl:variable>
      <xsl:variable name="RetrieveLitigationData" select="count(./Filter[@Type = 'RetrieveLitigationData'])"></xsl:variable>
      <xsl:variable name="RetrieveLiabilityData" select="count(./Filter[@Type = 'RetrieveLiabilityData'])"></xsl:variable>
      <xsl:variable name="RetrievePolicyCoverageData" select="count(./Filter[@Type = 'RetrievePolicyCoverageData'])"></xsl:variable>
      <xsl:variable name="RetrieveActivityData" select="count(./Filter[@Type = 'RetrieveActivityData'])"></xsl:variable>

      <xsl:if test="$ActivityFromDateCount > 0">
        <ActivityFromDate>
          <xsl:value-of select="string(Util:ConvertDateTimeInClaimsFormat(./Filter[@Type = 'ActivityFromDate']/@Value))" />
        </ActivityFromDate>
      </xsl:if>

      <xsl:if test="$ActivityToDateCount > 0">
        <ActivityToDate>
          <xsl:value-of select="string(Util:ConvertDateTimeInClaimsFormat(./Filter[@Type = 'ActivityToDate']/@Value))"/>
        </ActivityToDate>
      </xsl:if>

      <xsl:if test="$ActivityType > 0">
        <ActivityTypes>
          <xsl:for-each select="./Filter[@Type = 'ActivityType']/@Value">
            <d2p1:string>
              <xsl:value-of select="."/>
            </d2p1:string>
          </xsl:for-each>
        </ActivityTypes>
      </xsl:if>

      <xsl:if test="$RetrieveLossData > 0">
        <RetrieveLossData>
          <xsl:value-of select="./Filter[@Type = 'RetrieveLossData']/@Value"/>
        </RetrieveLossData>
      </xsl:if>
      <xsl:if test="$RetrieveInvolvedItemsData > 0">
        <RetrieveInvolvedItemsData>
          <xsl:value-of select="./Filter[@Type = 'RetrieveInvolvedItemsData']/@Value"/>
        </RetrieveInvolvedItemsData>
      </xsl:if>
      <xsl:if test="$RetrieveClaimGroupAndClaimClaimGroupAssociationData > 0">
        <RetrieveClaimGroupAndClaimClaimGroupAssociationData>
          <xsl:value-of select="./Filter[@Type = 'RetrieveClaimGroupAndClaimClaimGroupAssociationData']/@Value"/>
        </RetrieveClaimGroupAndClaimClaimGroupAssociationData>
      </xsl:if>
      <xsl:if test="$RetrieveClaimClaimAssociationData > 0">
        <RetrieveClaimClaimAssociationData>
          <xsl:value-of select="./Filter[@Type = 'RetrieveClaimClaimAssociationData']/@Value"/>
        </RetrieveClaimClaimAssociationData>
      </xsl:if>
      <xsl:if test="$RetrievePolicySummaryData > 0">
        <RetrievePolicySummaryData>
          <xsl:value-of select="./Filter[@Type = 'RetrievePolicySummaryData']/@Value"/>
        </RetrievePolicySummaryData>
      </xsl:if>
      <xsl:if test="$RetrieveLineData > 0">
        <RetrieveLineData>
          <xsl:value-of select="./Filter[@Type = 'RetrieveLineData']/@Value"/>
        </RetrieveLineData>
      </xsl:if>
      <xsl:if test="$RetrieveParticipantData > 0">
        <RetrieveParticipantData>
          <xsl:value-of select="./Filter[@Type = 'RetrieveParticipantData']/@Value"/>
        </RetrieveParticipantData>
      </xsl:if>
      <xsl:if test="$RetrievePerformerData > 0">
        <RetrievePerformerData>
          <xsl:value-of select="./Filter[@Type = 'RetrievePerformerData']/@Value"/>
        </RetrievePerformerData>
      </xsl:if>
      <xsl:if test="$RetrieveFinancialSummaryData > 0">
        <RetrieveFinancialSummaryData>
          <xsl:value-of select="./Filter[@Type = 'RetrieveFinancialSummaryData']/@Value"/>
        </RetrieveFinancialSummaryData>
      </xsl:if>
      <xsl:if test="$RetrieveLitigationData > 0">
        <RetrieveLitigationData>
          <xsl:value-of select="./Filter[@Type = 'RetrieveLitigationData']/@Value"/>
        </RetrieveLitigationData>
      </xsl:if>
      <xsl:if test="$RetrieveLiabilityData > 0">
        <RetrieveLiabilityData>
          <xsl:value-of select="./Filter[@Type = 'RetrieveLiabilityData']/@Value"/>
        </RetrieveLiabilityData>
      </xsl:if>
      <xsl:if test="$RetrievePolicyCoverageData > 0">
        <RetrievePolicyCoverageData>
          <xsl:value-of select="./Filter[@Type = 'RetrievePolicyCoverageData']/@Value"/>
        </RetrievePolicyCoverageData>
      </xsl:if>
      <xsl:if test="$RetrieveActivityData > 0">
        <RetrieveActivityData>
          <xsl:value-of select="./Filter[@Type = 'RetrieveActivityData']/@Value"/>
        </RetrieveActivityData>
      </xsl:if>


    </GetClaimSummaryByFilterCriteriaRequest>
  </xsl:template>

</xsl:stylesheet>
