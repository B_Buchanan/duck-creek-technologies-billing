﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Util="urn:Util" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs msxsl xsl Util">
  <xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>
  <xsl:template match="server/responses">
    <Policies>
      <ReturnCount>
        <xsl:variable name="ReturnCount">
          <xsl:choose>
            <xsl:when test="PartyInvolvement.getPolicyInvolvementsSummaryRs/PartyInvolvements/Paging/ReturnCount">
              <xsl:value-of select="PartyInvolvement.getPolicyInvolvementsSummaryRs/PartyInvolvements/Paging/ReturnCount"/>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
		<xsl:value-of select="$ReturnCount"/>
      </ReturnCount>
      <TotalCount>
        <xsl:variable name="TotalCount">
          <xsl:choose>
            <xsl:when test="PartyInvolvement.getPolicyInvolvementsSummaryRs/PartyInvolvements/Paging/TotalCount">
              <xsl:value-of select="PartyInvolvement.getPolicyInvolvementsSummaryRs/PartyInvolvements/Paging/TotalCount"/>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="$TotalCount"/> 
      </TotalCount>
      <xsl:for-each select="PartyInvolvement.getPolicyInvolvementsSummaryRs/PartyInvolvements/PartyInvolvement">
        <Policy xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
          <xsl:attribute name="PolicyId">
            <xsl:value-of select="ObjectId"/>
          </xsl:attribute>
          <Source>
            <xsl:choose>
              <xsl:when test="Source">
                <xsl:value-of select="Source" />
              </xsl:when>
              <xsl:otherwise>ADCPOL</xsl:otherwise>
            </xsl:choose>
          </Source>
          <NavigationLink>
            <xsl:choose>
              <xsl:when test="NavigationLink">
                <xsl:value-of select="NavigationLink" />
              </xsl:when>
              <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
          </NavigationLink>
          <xsl:variable name="varPolicyId" select="PolicyId" />
          <xsl:variable name="varPolicyNumber" select="ObjectReference" />
          <xsl:call-template name="policyinvolvementsandSummary">
          </xsl:call-template>
        </Policy>
      </xsl:for-each>
    </Policies>
    <BillingAccounts>
      <ReturnCount>
        <xsl:variable name="ReturnCount">
          <xsl:choose>
            <xsl:when test="PartyInvolvement.getBillingInvolvementsSummaryRs/PartyInvolvements/Paging/ReturnCount">
              <xsl:value-of select="PartyInvolvement.getBillingInvolvementsSummaryRs/PartyInvolvements/Paging/ReturnCount"/>
            </xsl:when>
			<xsl:when test="Send.postRs/server/responses/PartyInvolvement.getBillingInvolvementsSummaryRs/PartyInvolvements/Paging/ReturnCount">
              <xsl:value-of select="Send.postRs/server/responses/PartyInvolvement.getBillingInvolvementsSummaryRs/PartyInvolvements/Paging/ReturnCount"/>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="$ReturnCount"/>
      </ReturnCount>
      <TotalCount>
        <xsl:variable name="TotalCount">
          <xsl:choose>
		    <xsl:when test="PartyInvolvement.getBillingInvolvementsSummaryRs/PartyInvolvements/Paging/TotalCount">
              <xsl:value-of select="PartyInvolvement.getBillingInvolvementsSummaryRs/PartyInvolvements/Paging/TotalCount"/>
            </xsl:when>
            <xsl:when test="Send.postRs/server/responses/PartyInvolvement.getBillingInvolvementsSummaryRs/PartyInvolvements/Paging/TotalCount">
              <xsl:value-of select="Send.postRs/server/responses/PartyInvolvement.getBillingInvolvementsSummaryRs/PartyInvolvements/Paging/TotalCount"/>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="$TotalCount"/>
      </TotalCount>
      <xsl:for-each select="PartyInvolvement.getBillingInvolvementsSummaryRs/PartyInvolvements/PartyInvolvement | Send.postRs/server/responses/PartyInvolvement.getBillingInvolvementsSummaryRs/PartyInvolvements/PartyInvolvement">
        <BillingAccount xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
          <xsl:attribute name="AccountId">
            <xsl:value-of select="Account/AccountId"/>
          </xsl:attribute>
          <Source>
            <xsl:choose>
              <xsl:when test="Source">
                <xsl:value-of select="Source" />
              </xsl:when>
              <xsl:otherwise>ADCBIL</xsl:otherwise>
            </xsl:choose>
          </Source>
          <NavigationLink>
            <xsl:choose>
              <xsl:when test="NavigationLink">
                <xsl:value-of select="NavigationLink" />
              </xsl:when>
              <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
          </NavigationLink>
          <xsl:variable name="varAccountId" select="Account/AccountId" />

          <xsl:call-template name="billinginvolvementsandSummary" />

        </BillingAccount>
      </xsl:for-each>
    </BillingAccounts>
    <Claims>
      <TotalCount>
        <xsl:variable name="TotalCount">
          <xsl:choose>
            <xsl:when test="ClaimsComponents.getClaimInvolvementsRs/ClaimsInvolvements/TotalCount">
              <xsl:value-of select="ClaimsComponents.getClaimInvolvementsRs/ClaimsInvolvements/TotalCount"/>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="$TotalCount"/>
      </TotalCount>
      <xsl:for-each select="ClaimsComponents.getClaimInvolvementsRs/ClaimsInvolvements/ClaimsInvolvement">
        <Claim xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
          <xsl:attribute name="ClaimID">
            <xsl:value-of select="ClaimID"/>
          </xsl:attribute>
          <Source>
            <xsl:choose>
              <xsl:when test="Source">
                <xsl:value-of select="Source" />
              </xsl:when>
              <xsl:otherwise>ADCCLAIM</xsl:otherwise>
            </xsl:choose>
          </Source>
          <NavigationLink>
            <xsl:choose>
              <xsl:when test="NavigationLink">
                <xsl:value-of select="NavigationLink" />
              </xsl:when>
              <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
          </NavigationLink>
          <xsl:variable name="varClaimid" select="ClaimID" />
          <xsl:call-template name="ClaimInvolvementsandSummary" />
        </Claim>
      </xsl:for-each>
    </Claims>
  </xsl:template>


  <!--Policies Templates-->
  <xsl:template name="policyinvolvementsandSummary" >
    <ObjectId>
      <xsl:value-of select="ObjectId"/>
    </ObjectId>
    <PartyId>
      <xsl:value-of select="PartyId"/>
    </PartyId>
    <xsl:choose>
      <xsl:when test ="Policy/Status='Quote' or Policy/Status='Application'">
        <QuoteNumber>
          <xsl:value-of select="Policy/PolicyNumber"/>
        </QuoteNumber>
      </xsl:when>
      <xsl:otherwise>
        <PolicyNumber>
          <xsl:value-of select="Policy/PolicyNumber"/>
        </PolicyNumber>
      </xsl:otherwise>
    </xsl:choose>
    <LineOfBusiness>
      <xsl:value-of select="Policy/LOB"/>
    </LineOfBusiness>
    <xsl:choose>
      <xsl:when test ="Policy/Status='Quote' or Policy/Status='Application'">
        <Status>Quote</Status>
      </xsl:when>
      <xsl:otherwise>
        <Status>
          <xsl:value-of select="Policy/Status"/>
        </Status>
      </xsl:otherwise>
    </xsl:choose>
    <PolicyEffectiveDate>
      <xsl:value-of select="Policy/EffectiveDate"/>
    </PolicyEffectiveDate>
    <PolicyExpirationDate>
      <xsl:value-of select="Policy/ExpirationDate"/>
    </PolicyExpirationDate>
    <InceptionDate>
      <xsl:value-of select="Policy/InceptionDate"/>
    </InceptionDate>
    <xsl:call-template name="partydetailRoles" />
    <WrittenPremium>
      <xsl:value-of select="Policy/WrittenPremium"/>
    </WrittenPremium>
    <FullTermPremium>
      <xsl:value-of select="Policy/FullTermPremium"/>
    </FullTermPremium>
  </xsl:template>

  <!--Billing Templates-->
  <xsl:template name="billinginvolvementsandSummary" >
    <AccountId>
      <xsl:value-of select="Account/AccountId"/>
    </AccountId>
    <AccountReference>
      <xsl:value-of select="Account/AccountReference"/>
    </AccountReference>
    <AccountStatus>
      <xsl:value-of select="Account/AccountStatus"/>
    </AccountStatus>
    <ObjectId>
      <xsl:value-of select="ObjectId"/>
    </ObjectId>
    <PartyId>
      <xsl:value-of select="PartyId"/>
    </PartyId>
    <InvoiceDate>
      <xsl:value-of select="Account/Invoices/Invoice/InvoiceDate"/>
    </InvoiceDate>
    <InvoiceAmount>
      <xsl:value-of select="Account/Invoices/Invoice/InvoiceTotalAmount"/>
    </InvoiceAmount>
    <DueDate>
      <xsl:value-of select="Account/Invoices/Invoice/DueDate"/>
    </DueDate>
    <OutstandingBalance>
      <xsl:value-of select="Account/OutstandingBalance"/>
    </OutstandingBalance>
    <CurrentDueAmount>
      <xsl:value-of select="Account/CurrentDueAmount"/>
    </CurrentDueAmount>
    <PastDueAmount>
      <xsl:value-of select="Account/PastDueAmount"/>
    </PastDueAmount>
    <xsl:call-template name="partydetailRoles" />
  </xsl:template>

  <!--Claim Involvements Templates-->
  <xsl:template name="ClaimInvolvementsandSummary">
    <ClaimId>
      <xsl:value-of select="ClaimID"/>
    </ClaimId>
    <ClaimNumber>
      <xsl:value-of select="ClaimsNumber"/>
    </ClaimNumber>
    <LOB>
      <xsl:value-of select="LOB"/>
    </LOB>
    <ClaimType>
      <xsl:value-of select="LOBCategory"/>
    </ClaimType>
    <LossDate>
      <xsl:value-of select="LossDate"/>
    </LossDate>
    <ClaimSensitivity>
      <xsl:value-of select="ClaimSensitivity"/>
    </ClaimSensitivity>
    <ClaimStatus>
      <xsl:value-of select="ClaimStatus"/>
    </ClaimStatus>
    <xsl:call-template name="claimsdetailRoles" />
    <!--<RoleCodes>
      <xsl:call-template name="ClaimRoleCodes" />
    </RoleCodes>-->
  </xsl:template>

  <!--Roles Templates-->
  <xsl:template name="partydetailRoles" >
    <RoleCodes>
      <xsl:variable name="varParticipantId" select="PartyId" />
      <xsl:for-each select="RoleCodes">
        <xsl:for-each select="RoleCode">
		<xsl:sort select="." data-type="text" order="ascending"/>
          <Role>
            <RoleCode>
              <xsl:value-of select="current()"/>
            </RoleCode>
          </Role>

        </xsl:for-each>
      </xsl:for-each>
      <xsl:if test="position()!=last()">
        <xsl:text>,</xsl:text>
      </xsl:if>
    </RoleCodes>
  </xsl:template>

  <xsl:template name="claimsdetailRoles" >
    <RoleCodes>
      <!--<xsl:variable name="varParticipantId" select="PartyId" />-->
      <xsl:for-each select="Roles">
        <xsl:for-each select="RoleCode">
		<xsl:sort select="." data-type="text" order="ascending"/>
          <Role>
            <RoleCode>
              <xsl:value-of select="current()"/>
            </RoleCode>
          </Role>

        </xsl:for-each>
      </xsl:for-each>
      <xsl:if test="position()!=last()">
        <xsl:text>,</xsl:text>
      </xsl:if>
    </RoleCodes>
  </xsl:template>

  <!--<xsl:template name="RoleCodes" >
    <xsl:for-each select="RoleCodes/RoleCode">
      <RoleCode>
        <xsl:value-of select="current()"/>
      </RoleCode>
    </xsl:for-each>
  </xsl:template>-->

</xsl:stylesheet>
