﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:d4p1="http://claims.accenture.com/2011/01/01/Types" xmlns:datacontract="http://schemas.datacontract.org/2004/07/AFS.AEF"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:Util="urn:Util"
                exclude-result-prefixes="xs msxsl xsl d4p1 datacontract Util" >
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="GetClaimSummaryByFilterCriteriaResponse">
    <ClaimSummaryAndEventHistory>
      <xsl:call-template name="claimTemplate" />
      <xsl:call-template name="claimLossTemplate" />
      <xsl:call-template name="workersCompensationLossTemplate" />
      <xsl:call-template name="riskLocationTemplate" />
      <xsl:call-template name="coveredVehicleTemplate" />
      <xsl:call-template name="coveredPropertyTemplate" />
      <xsl:call-template name="coveredPersonTemplate" />
      <xsl:call-template name="claimClaimGroupAssociationTemplate" />
      <xsl:call-template name="claimClaimAssociationTemplate" />

      <xsl:call-template name="claimGroupDetailTemplate" />
      <xsl:call-template name="policyTemplate" />
      <xsl:call-template name="linesTemplate" />
      <xsl:call-template name="participantsTemplate" />
      <xsl:call-template name="performerDetailTemplate" />
      <xsl:call-template name="claimFinancialSummaryDetailTemplate" />
      <xsl:call-template name="litigationDetailTemplate" />
      <xsl:call-template name="liabilityDetailTemplate" />
      <xsl:call-template name="policyCoverageAndRemainingCoveragesTemplate" />

      <ClaimEventsHistory>
        <xsl:call-template name="claimEventHistoryTemplate" />
      </ClaimEventsHistory>
      <xsl:call-template name="ResponseInfo">
        <xsl:with-param name="ReturnCode">0</xsl:with-param>
        <xsl:with-param name="NotificationResponse"></xsl:with-param>
      </xsl:call-template>
    </ClaimSummaryAndEventHistory>
  </xsl:template>

  <xsl:template name="claimLossTemplate">
    <Loss>
      <xsl:copy-of select="Loss/AuthorityReported" />
      <xsl:copy-of select="Loss/AuthorityReportedName" />
      <xsl:copy-of select="Loss/AuthorityReportNumber" />
      <xsl:copy-of select="Loss/CauseOfLoss" />
      <xsl:copy-of select="Loss/Country" />
      <xsl:copy-of select="Loss/DrugOrAlcohol" />
      <xsl:copy-of select="Loss/FaultyProductDescription" />
      <xsl:copy-of select="Loss/FaultyProductWorkmanship" />
      <xsl:copy-of select="Loss/FaultyProductWorkmanshipDescription" />
      <xsl:copy-of select="Loss/IntersectionIndicator" />
      <xsl:copy-of select="Loss/LastTransactionID" />
      <xsl:variable name="lossDate" select="Loss/LossDate" />
      <xsl:choose>
        <xsl:when test="$lossDate != ''">
          <LossDate>
            <xsl:value-of select="string(Util:ConvertDateToDuckFormat($lossDate))" />
          </LossDate>
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy-of select="Loss/LossDate" />
        </xsl:otherwise>
      </xsl:choose>
      <xsl:copy-of select="Loss/LossDescription" />
      <xsl:copy-of select="Loss/LossID" />
      <LossLocation>
        <xsl:for-each select="Loss/LossLocation">
          <xsl:call-template name="commonClaimsAddressTemplate" />
        </xsl:for-each>
      </LossLocation>
      <xsl:copy-of select="Loss/LossLocationDescription" />
      <xsl:copy-of select="Loss/LossLocationID" />
      <xsl:copy-of select="Loss/LossTime" />
      <xsl:copy-of select="Loss/LossType" />
      <xsl:copy-of select="Loss/NegligenceRule" />
      <xsl:copy-of select="Loss/ReportedBy" />
      <xsl:variable name="reportedDate" select="Loss/ReportedDate" />
      <xsl:choose>
        <xsl:when test="$reportedDate != ''">
          <ReportedDate>
            <xsl:value-of select="string(Util:ConvertDateToDuckFormat($reportedDate))" />
          </ReportedDate>
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy-of select="Loss/ReportedDate" />
        </xsl:otherwise>
      </xsl:choose>
      <xsl:copy-of select="Loss/State" />
      <xsl:copy-of select="Loss/StreetIntersection" />
    </Loss>
  </xsl:template>

  <xsl:template name="claimTemplate">
    <Claim>
      <xsl:copy-of select="Claim/AccountLocation"/>
      <xsl:copy-of select="Claim/AccountLocationDescription"/>
      <xsl:copy-of select="Claim/AllowAutomatedPaymentIndicator"/>
      <xsl:variable name="claimGroupPushDate" select="Claim/ClaimGroupPushDate" />
      <xsl:choose>
        <xsl:when test="$claimGroupPushDate != ''">
          <ClaimGroupPushDate>
            <xsl:value-of select="string(Util:ConvertDateToDuckFormat($claimGroupPushDate))" />
          </ClaimGroupPushDate>
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy-of select="Claim/ClaimGroupPushDate"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:copy-of select="Claim/ClaimID"/>
      <xsl:copy-of select="Claim/ClaimNumber"/>
      <xsl:copy-of select="Claim/ClaimSourceType"/>
      <xsl:copy-of select="Claim/ClaimStatus"/>
      <xsl:copy-of select="Claim/ClaimToSuitIndicator"/>
      <xsl:copy-of select="Claim/ClientSatisfaction"/>
      <xsl:copy-of select="Claim/Complexity"/>
      <xsl:copy-of select="Claim/ConvertedClaimIndicator"/>
      <xsl:variable name="dateClosed" select="Claim/DateClosed" />
      <xsl:choose>
        <xsl:when test="$dateClosed != ''">
          <DateClosed>
            <xsl:value-of select="string(Util:ConvertDateToDuckFormat($dateClosed))" />
          </DateClosed>
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy-of select="Claim/DateClosed"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="dateEntered" select="Claim/DateEntered" />
      <xsl:choose>
        <xsl:when test="$dateEntered != ''">
          <DateEntered>
            <xsl:value-of select="string(Util:ConvertDateToDuckFormat($dateEntered))" />
          </DateEntered>
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy-of select="Claim/DateEntered"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="dateReopened" select="Claim/DateReopened" />
      <xsl:choose>
        <xsl:when test="$dateReopened != ''">
          <DateReopened>
            <xsl:value-of select="string(Util:ConvertDateToDuckFormat($dateReopened))" />
          </DateReopened>
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy-of select="Claim/DateReopened"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:copy-of select="Claim/DefaultCurrency"/>
      <xsl:copy-of select="Claim/EnteredByID"/>
      <xsl:copy-of select="Claim/FileType"/>
      <xsl:copy-of select="Claim/HandlingCompany"/>
      <xsl:copy-of select="Claim/HandlingDepartment"/>
      <xsl:variable name="insuredNotificationDate" select="Claim/InsuredNotificationDate" />
      <xsl:choose>
        <xsl:when test="$insuredNotificationDate != ''">
          <InsuredNotificationDate>
            <xsl:value-of select="string(Util:ConvertDateToDuckFormat($insuredNotificationDate))" />
          </InsuredNotificationDate>
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy-of select="Claim/InsuredNotificationDate" />
        </xsl:otherwise>
      </xsl:choose>
      <xsl:copy-of select="Claim/InternationalClaimNumber"/>
      <xsl:copy-of select="Claim/LastTransactionID"/>
      <xsl:copy-of select="Claim/LineOfBusiness"/>
      <xsl:copy-of select="Claim/LineOfBusinessCategory"/>
      <xsl:copy-of select="Claim/LossID"/>
      <xsl:copy-of select="Claim/MarkInErrorIndicator"/>
      <xsl:copy-of select="Claim/PolicyID"/>
      <xsl:copy-of select="Claim/RecoveryScore"/>
      <xsl:copy-of select="Claim/RegulatoryCountry"/>
      <xsl:copy-of select="Claim/RegulatoryJurisdiction"/>
      <xsl:copy-of select="Claim/RegulatoryTerritory"/>
      <xsl:copy-of select="Claim/Sensitivity"/>
      <xsl:copy-of select="Claim/SuitFiledIndicator"/>
    </Claim>
  </xsl:template>

  <xsl:template name="coveredVehicleTemplate" >
    <CoveredVehicles>
      <xsl:for-each select="CoveredVehicles/VehicleItemDTO">
        <xsl:choose>
          <xsl:when test="ItemDTO[CoveredIndicator='true']" >
            <xsl:call-template name="vehicleItemCommonTemplate" />
          </xsl:when>
        </xsl:choose>
      </xsl:for-each>
    </CoveredVehicles>
  </xsl:template>

  <xsl:template name="vehicleItemCommonTemplate">
    <VehicleItem>
      <xsl:for-each select="ItemDTO">
        <Item>
          <xsl:call-template name="commonItemTemplate" />
        </Item>
      </xsl:for-each>
      <xsl:for-each select="ItemVehicleDetail">
        <ItemVehicleDetail>
          <xsl:copy-of select="AdditionalInformation"/>
          <xsl:copy-of select="BodyType"/>
          <xsl:copy-of select="DamagedIndicator"/>
          <xsl:copy-of select="DrivableIndicator"/>
          <xsl:copy-of select="DriverName"/>
          <xsl:copy-of select="DriverParticipantID"/>
          <xsl:copy-of select="EstimatesObtained"/>
          <xsl:copy-of select="EstimatesReceivedFromName"/>
          <xsl:copy-of select="EstimatesReceviedFrom"/>
          <xsl:copy-of select="ExternalItemID"/>
          <xsl:copy-of select="ItemID"/>
          <xsl:copy-of select="LastTransactionID"/>
          <xsl:copy-of select="Make"/>
          <xsl:copy-of select="Mileage"/>
          <xsl:copy-of select="MileageUnitOfMeasure"/>
          <xsl:copy-of select="ModelMaker"/>
          <xsl:copy-of select="OwnedByInsuredIndicator"/>
          <xsl:copy-of select="OwnerName"/>
          <xsl:copy-of select="OwnerParticipantID"/>
          <xsl:copy-of select="Ownership"/>
          <xsl:copy-of select="Passengers"/>
          <xsl:copy-of select="Plate"/>
          <xsl:copy-of select="PlateCountry"/>
          <xsl:copy-of select="PlateState"/>
          <xsl:copy-of select="PurposeOfUse"/>
          <xsl:copy-of select="RentalItemRequiredIndicator"/>
          <xsl:copy-of select="RepairIndicator"/>
          <xsl:copy-of select="RepairStatus"/>
          <xsl:copy-of select="TowingServiceIndicator"/>
          <xsl:copy-of select="Type"/>
          <xsl:copy-of select="UsedWithPermissionIndicator"/>
          <xsl:copy-of select="VehicleDescription"/>
          <xsl:copy-of select="VehicleIdentificationNumber"/>
          <xsl:copy-of select="VehicleIdentificationNumberDescription"/>
          <xsl:copy-of select="Year"/>
        </ItemVehicleDetail>
      </xsl:for-each>
      <ItemVehicleDamages>
        <xsl:for-each select="ItemVehicleDamageDTO/ItemVehicleDamageDTO">
          <ItemVehicleDamage>
            <ItemID>
              <xsl:copy-of select="ItemID"/>
            </ItemID>
            <LastTransactionID>
              <xsl:copy-of select="LastTransactionID"/>
            </LastTransactionID>
            <VehicleDamage>
              <xsl:copy-of select="VehicleDamage"/>
            </VehicleDamage>
            <VehicleDamageID>
              <xsl:copy-of select="VehicleDamageID"/>
            </VehicleDamageID>
            <VehicleImpactLocation>
              <xsl:copy-of select="VehicleImpactLocation"/>
            </VehicleImpactLocation>
          </ItemVehicleDamage>
        </xsl:for-each>
      </ItemVehicleDamages>
    </VehicleItem>
  </xsl:template>

  <xsl:template name="policyTemplate">
    <Policy>
      <xsl:copy-of select="Policy/AddressID" />
      <xsl:copy-of select="Policy/AgentPartyID" />
      <xsl:copy-of select="Policy/ClaimPolicyType" />
      <xsl:copy-of select="Policy/ClaimsMadeCoverageTrigger" />
      <xsl:copy-of select="Policy/ClaimsMadePolicy" />
      <xsl:copy-of select="Policy/CompanyClassStatBlock" />
      <xsl:copy-of select="Policy/Country" />
      <xsl:copy-of select="Policy/CoverageState" />
      <xsl:copy-of select="Policy/ExecutiveContactFirstName" />
      <xsl:copy-of select="Policy/ExecutiveContactFirstNameLast" />
      <xsl:copy-of select="Policy/ExecutiveContactFirstNamePhonetic" />
      <xsl:copy-of select="Policy/ExecutiveContactFirstNamePhoneticLast" />
      <xsl:copy-of select="Policy/ExecutiveContactLastName" />
      <xsl:copy-of select="Policy/ExecutiveContactLastNameFirst" />
      <xsl:copy-of select="Policy/ExecutiveContactLastNamePhonetic" />
      <xsl:copy-of select="Policy/ExecutiveContactLastNamePhoneticFirst" />
      <xsl:copy-of select="Policy/ExternalPolicyHolderID" />
      <xsl:copy-of select="Policy/ExternalPolicySource" />
      <xsl:copy-of select="Policy/ExternalSystemPolicyID" />
      <xsl:copy-of select="Policy/FilingInitial" />
      <xsl:copy-of select="Policy/FilingName" />
      <xsl:call-template name="insuredAddressTemplate" />
      <xsl:copy-of select="Policy/LastTransaction" />
      <xsl:copy-of select="Policy/LineTypeStatBlock" />
      <xsl:copy-of select="Policy/ManualIndicator" />
      <xsl:copy-of select="Policy/NamedInsured" />
      <xsl:copy-of select="Policy/Organization" />
      <xsl:variable name="policyEffectiveDate" select="Policy/PolicyEffectiveDate" />
      <xsl:choose>
        <xsl:when test="$policyEffectiveDate != ''">
          <PolicyEffectiveDate>
            <xsl:value-of select="string(Util:ConvertDateToDuckFormat($policyEffectiveDate))" />
          </PolicyEffectiveDate>
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy-of select="Policy/PolicyEffectiveDate"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="policyExpirationDate" select="Policy/PolicyExpirationDate" />
      <xsl:choose>
        <xsl:when test="$policyExpirationDate != ''">
          <PolicyExpirationDate>
            <xsl:value-of select="string(Util:ConvertDateToDuckFormat($policyExpirationDate))" />
          </PolicyExpirationDate>
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy-of select="Policy/PolicyExpirationDate"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:copy-of select="Policy/PolicyID" />
      <xsl:copy-of select="Policy/PolicyNumber" />
      <xsl:copy-of select="Policy/PolicyOccurenceNumber" />
      <xsl:copy-of select="Policy/PolicySearchType" />
      <xsl:copy-of select="Policy/PolicyStatus" />
      <xsl:copy-of select="Policy/PremiumPaymentStatus" />
      <xsl:copy-of select="Policy/PrimaryContactFirstName" />
      <xsl:copy-of select="Policy/PrimaryContactFirstNameLast" />
      <xsl:copy-of select="Policy/PrimaryContactFirstNamePhonetic" />
      <xsl:copy-of select="Policy/PrimaryContactFirstNamePhoneticLast" />
      <xsl:copy-of select="Policy/PrimaryContactLastName" />
      <xsl:copy-of select="Policy/PrimaryContactLastNameFirst" />
      <xsl:copy-of select="Policy/PrimaryContactLastNamePhonetic" />
      <xsl:copy-of select="Policy/PrimaryContactLastNamePhoneticFirst" />
      <xsl:copy-of select="Policy/ProducerBranch" />
      <xsl:copy-of select="Policy/ReinsuranceAssumedCompany" />
      <xsl:copy-of select="Policy/ReinsuranceCededContract" />
      <xsl:copy-of select="Policy/RetrievalStatus" />
      <xsl:copy-of select="Policy/State" />
      <xsl:copy-of select="Policy/TaxID" />
    </Policy>
  </xsl:template>

  <xsl:template name="insuredAddressTemplate">
    <InsuredAddress>
      <xsl:for-each select="Policy/InsuredAddressDTO">
        <xsl:call-template name="commonClaimsAddressTemplate" />
      </xsl:for-each>
    </InsuredAddress>
  </xsl:template>

  <xsl:template name="linesTemplate" >
    <Lines>
      <xsl:for-each select="Lines/LineDTO">
        <Line>
          <xsl:copy-of select="AdditionalFinalPayment"/>
          <xsl:copy-of select="AddressID"/>
          <xsl:copy-of select="AutomatedLineGenIndicator"/>
          <xsl:copy-of select="AutoReserveIndicator"/>
          <xsl:copy-of select="CauseOfLoss"/>
          <xsl:copy-of select="ClaimantName"/>
          <xsl:copy-of select="ClaimID"/>
          <xsl:copy-of select="ContributingFactor"/>
          <xsl:copy-of select="CoverageMatchStatus"/>
          <xsl:copy-of select="DamageDescription"/>
          <xsl:copy-of select="ExpenseOnlyIndicator"/>
          <xsl:copy-of select="HandlingStatus"/>
          <xsl:copy-of select="InvolvementRoleID"/>
          <xsl:copy-of select="LastTransactionID"/>
          <xsl:copy-of select="LegacySalvageStatus"/>
          <xsl:copy-of select="LegacySubrogationStatus"/>
          <xsl:copy-of select="LiableItemID"/>
          <xsl:copy-of select="LineCloseReason"/>
          <xsl:copy-of select="LineComplexity"/>
          <xsl:copy-of select="LineFinancialCloseReason"/>
          <xsl:variable name="lineFinancialsCloseDate" select="LineFinancialsCloseDate" />
          <xsl:choose>
            <xsl:when test="$lineFinancialsCloseDate != ''">
              <LineFinancialsCloseDate>
                <xsl:value-of select="string(Util:ConvertDateToDuckFormat($lineFinancialsCloseDate))" />
              </LineFinancialsCloseDate>
            </xsl:when>
            <xsl:otherwise>
              <xsl:copy-of select="LineFinancialsCloseDate"/>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:variable name="lineFinancialsReopenDate" select="LineFinancialsReopenDate" />
          <xsl:choose>
            <xsl:when test="$lineFinancialsReopenDate != ''">
              <LineFinancialsReopenDate>
                <xsl:value-of select="string(Util:ConvertDateToDuckFormat($lineFinancialsReopenDate))" />
              </LineFinancialsReopenDate>
            </xsl:when>
            <xsl:otherwise>
              <xsl:copy-of select="LineFinancialsReopenDate"/>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:copy-of select="LineFinancialStatus"/>
          <xsl:copy-of select="LineID"/>
          <xsl:copy-of select="LineLocationOfLoss"/>
          <xsl:copy-of select="LineRegulatoryCountry"/>
          <xsl:copy-of select="LineRegulatoryCounty"/>
          <xsl:copy-of select="LineRegulatoryState"/>
          <xsl:copy-of select="LineReserveMethod"/>
          <xsl:copy-of select="LineType"/>
          <xsl:copy-of select="LineTypeDescription"/>
          <xsl:copy-of select="LineTypeOverrideIndicator"/>
          <xsl:copy-of select="LineTypeOverridingOrgEntity"/>
          <xsl:copy-of select="LineUOM"/>
          <xsl:copy-of select="LossCharacteristic"/>
          <xsl:copy-of select="MedicareIndicator"/>
          <xsl:copy-of select="NatureOfOperation"/>
          <xsl:copy-of select="PrimaryExcessLine"/>
          <xsl:copy-of select="PropertyType"/>
          <xsl:copy-of select="RecoveryIndicator"/>
          <xsl:copy-of select="ReserveCategory"/>
          <xsl:copy-of select="SettlementMethod"/>
          <xsl:copy-of select="SIUIndicator"/>
          <xsl:copy-of select="ThirdPartyIndicator"/>
          <xsl:copy-of select="ValidProductCatalogLineIndicator"/>
          <xsl:copy-of select="VehicleLocationDetail"/>
        </Line>
      </xsl:for-each>
    </Lines>
  </xsl:template>

  <xsl:template name="participantsTemplate" >
    <Participants>
      <xsl:for-each select="Participants/ParticipantDTO">
        <Participant>
          <xsl:call-template name="participantCommonDetailTemplate" />
        </Participant>
      </xsl:for-each>
    </Participants>
  </xsl:template>

  <xsl:template name="participantCommonDetailTemplate">
    <xsl:copy-of select="AnalyticScore" />
    <xsl:variable name="cIBRequestDate" select="CIBRequestDate" />
    <xsl:choose>
      <xsl:when test="$cIBRequestDate != ''">
        <CIBRequestDate>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($cIBRequestDate))" />
        </CIBRequestDate>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="CIBRequestDate"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:copy-of select="ClaimID" />
    <xsl:copy-of select="ContactNote" />
    <xsl:copy-of select="CoverageConfirmed" />
    <xsl:variable name="participantDateOfBirth" select="DateOfBirth" />
    <xsl:choose>
      <xsl:when test="$participantDateOfBirth != ''">
        <DateOfBirth>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($participantDateOfBirth))" />
        </DateOfBirth>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="DateOfBirth"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:copy-of select="DependentsUnder18" />
    <xsl:copy-of select="DriverCountry" />
    <xsl:copy-of select="DriversLicenseNumber" />
    <xsl:copy-of select="DriverState" />
    <xsl:copy-of select="EFTActive" />
    <xsl:copy-of select="Gender" />
    <xsl:copy-of select="InsuranceExist" />
    <xsl:copy-of select="LastTransactionID" />
    <xsl:variable name="participantLastUpdateDate" select="LastUpdateDate" />
    <xsl:choose>
      <xsl:when test="$participantLastUpdateDate != ''">
        <LastUpdateDate>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($participantLastUpdateDate))" />
        </LastUpdateDate>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="LastUpdateDate"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:copy-of select="ParticipantFullName" />
    <xsl:copy-of select="ParticipantID" />
    <ParticipantNameDetail>
      <xsl:for-each select="ParticipantNameDetailDTO">
        <xsl:call-template name="commonParticipantNameDetailTemplate" />
      </xsl:for-each>
    </ParticipantNameDetail>
    <ParticipantRoles>
      <xsl:for-each select="ParticipantRolesDTO/ParticipantRoleDTO">
        <ParticipantRole>
          <xsl:call-template name="commonParticipantRoleTemplate"/>
        </ParticipantRole>
      </xsl:for-each>
    </ParticipantRoles>
    <xsl:copy-of select="PartyID" />
    <xsl:copy-of select="PolicyAdded" />
    <xsl:copy-of select="PrefAddressID" />
    <xsl:copy-of select="PrefEmailID" />
    <xsl:copy-of select="PreferredContactType" />
    <xsl:copy-of select="PrefNameID" />
    <xsl:copy-of select="PrefPhoneID" />
    <xsl:copy-of select="RecordDeleteInd" />
    <xsl:copy-of select="ReportedAge" />
    <xsl:copy-of select="TaxID" />
    <xsl:copy-of select="ThirdPartyPolicyNumber" />
  </xsl:template>

  <xsl:template name ="performerDetailTemplate">
    <Performers>
      <xsl:for-each select="Performers/PerformerDTO">
        <Performer>
          <xsl:copy-of select="ClaimantRoleID" />
          <xsl:copy-of select="ClaimID" />
          <xsl:copy-of select="LastTransactionID" />
          <xsl:copy-of select="LineID" />
          <xsl:variable name="organizationActivationDate" select="OrganizationActivationDate" />
          <xsl:choose>
            <xsl:when test="$organizationActivationDate != ''">
              <OrganizationActivationDate>
                <xsl:value-of select="string(Util:ConvertDateToDuckFormat($organizationActivationDate))" />
              </OrganizationActivationDate>
            </xsl:when>
            <xsl:otherwise>
              <xsl:copy-of select="OrganizationActivationDate"/>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:copy-of select="OrganizationCorporateName" />
          <xsl:variable name="organizationDeactivationDate" select="OrganizationDeactivationDate" />
          <xsl:choose>
            <xsl:when test="$organizationDeactivationDate != ''">
              <OrganizationDeactivationDate>
                <xsl:value-of select="string(Util:ConvertDateToDuckFormat($organizationDeactivationDate))" />
              </OrganizationDeactivationDate>
            </xsl:when>
            <xsl:otherwise>
              <xsl:copy-of select="OrganizationDeactivationDate"/>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:copy-of select="OrganizationEmail" />
          <xsl:copy-of select="OrganizationEntityID" />
          <xsl:copy-of select="OrganizationFirstName" />
          <xsl:copy-of select="OrganizationLastName" />
          <xsl:copy-of select="OrganizationMiddleName" />
          <xsl:copy-of select="OrganizationType" />
          <xsl:variable name="performerActivationDate" select="PerformerActivationDate" />
          <xsl:choose>
            <xsl:when test="$performerActivationDate != ''">
              <PerformerActivationDate>
                <xsl:value-of select="string(Util:ConvertDateToDuckFormat($performerActivationDate))" />
              </PerformerActivationDate>
            </xsl:when>
            <xsl:otherwise>
              <xsl:copy-of select="PerformerActivationDate"/>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:variable name="performerDeactivationDate" select="PerformerDeactivationDate" />
          <xsl:choose>
            <xsl:when test="$performerDeactivationDate != ''">
              <PerformerDeactivationDate>
                <xsl:value-of select="string(Util:ConvertDateToDuckFormat($performerDeactivationDate))" />
              </PerformerDeactivationDate>
            </xsl:when>
            <xsl:otherwise>
              <xsl:copy-of select="PerformerDeactivationDate"/>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:copy-of select="PerformerFullName" />
          <xsl:copy-of select="PerformerID" />
          <xsl:copy-of select="PerformerDTO/PerformerDTO" />
          <PerformerNameDetail>
            <xsl:for-each select="PerformerNameDetailDTO">
              <xsl:call-template name="commonParticipantNameDetailTemplate" />
            </xsl:for-each>
          </PerformerNameDetail>
          <xsl:copy-of select="PerformerRole" />
        </Performer>
      </xsl:for-each>
    </Performers>
  </xsl:template>

  <xsl:template name="policyCoverageAndRemainingCoveragesTemplate">
    <PolicyCoverageAndRemainingCoverages>
      <xsl:for-each select="PolicyCoverageAndRemainingCoverages/PolicyCoverageAndRemainingCoveragesDTO">
        <PolicyCoverageAndRemainingCoverage>
          <xsl:for-each select="PolicyCoverageDetail">
            <PolicyCoverageDetail>
              <xsl:copy-of select="AdditionalLimitAmount" />
              <xsl:copy-of select="CoverageDeductibleType" />
              <xsl:copy-of select="CoverageLevel" />
              <xsl:copy-of select="CoverageMaximumDeductibleAmount" />
              <xsl:copy-of select="CoverageMinimumDeductibleAmount" />
              <xsl:copy-of select="CurrencyUnitOfMeasure" />
              <xsl:variable name="policyCoverageDetailEffectiveDate" select="EffectiveDate" />
              <xsl:choose>
                <xsl:when test="$policyCoverageDetailEffectiveDate != ''">
                  <EffectiveDate>
                    <xsl:value-of select="string(Util:ConvertDateToDuckFormat($policyCoverageDetailEffectiveDate))" />
                  </EffectiveDate>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:copy-of select="EffectiveDate"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:copy-of select="ErodeParentLimit" />
              <xsl:variable name="policyCoverageDetailExpirationDate" select="ExpirationDate" />
              <xsl:choose>
                <xsl:when test="$policyCoverageDetailExpirationDate != ''">
                  <ExpirationDate>
                    <xsl:value-of select="string(Util:ConvertDateToDuckFormat($policyCoverageDetailExpirationDate))" />
                  </ExpirationDate>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:copy-of select="ExpirationDate"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:copy-of select="ExternalPolicyCoverageID" />
              <xsl:copy-of select="ItemGroupID" />
              <xsl:copy-of select="ItemID" />
              <xsl:copy-of select="LastTransactionID" />
              <xsl:copy-of select="ParentPolicyCoverageID" />
              <xsl:copy-of select="PolicyCoverage" />
              <xsl:copy-of select="PolicyCoverageDeductibleAmount" />
              <xsl:copy-of select="PolicyCoverageDescription" />
              <xsl:copy-of select="PolicyCoverageID" />
              <xsl:copy-of select="PolicyCoverageLimitAmount" />
              <xsl:copy-of select="PolicyCoverageSource" />
              <xsl:copy-of select="PolicyID" />
            </PolicyCoverageDetail>
          </xsl:for-each>
          <xsl:for-each select="PolicyCoverageRemainingLimitsAndDeductibleDTOs">
            <PolicyCoverageRemainingLimitsAndDeductibles>
              <xsl:for-each select="PolicyCoverageRemainingLimitsAndDeductibleDTO">
                <PolicyCoverageRemainingLimitAndDeductible>
                  <xsl:copy-of select="ClaimantRoleID" />
                  <xsl:copy-of select="CoverageDeductiblePaidToDateAmount" />
                  <xsl:copy-of select="CoverageLossPaidToDateAmount" />
                  <xsl:copy-of select="CoverageRemainingDeductibleAmount" />
                  <xsl:copy-of select="CoverageRemainingLimitAmount" />
                  <xsl:copy-of select="CurrencyUnitOfMeasure" />
                  <xsl:copy-of select="LastTransactionID" />
                  <xsl:copy-of select="LineID" />
                  <xsl:copy-of select="PolicyCoverageID" />
                  <xsl:copy-of select="PolicyCoverageRemainingID" />
                </PolicyCoverageRemainingLimitAndDeductible>
              </xsl:for-each>
            </PolicyCoverageRemainingLimitsAndDeductibles>
          </xsl:for-each>
        </PolicyCoverageAndRemainingCoverage>
      </xsl:for-each>
    </PolicyCoverageAndRemainingCoverages>
  </xsl:template>

  <xsl:template name="workersCompensationLossTemplate" >
    <WorkersCompensationLoss>
      <xsl:copy-of select="WorkersCompensationLoss/CompensationDenied" />
      <xsl:copy-of select="WorkersCompensationLoss/DrugOrAlcoholReason" />
      <xsl:variable name="employerNotificationDate" select="WorkersCompensationLoss/EmployerNotificationDate" />
      <xsl:choose>
        <xsl:when test="$employerNotificationDate != ''">
          <EmployerNotificationDate>
            <xsl:value-of select="string(Util:ConvertDateToDuckFormat($employerNotificationDate))" />
          </EmployerNotificationDate>
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy-of select="WorkersCompensationLoss/EmployerNotificationDate"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:copy-of select="WorkersCompensationLoss/EmployerNotificationTime" />
      <xsl:copy-of select="WorkersCompensationLoss/FederalActReported" />
      <xsl:copy-of select="WorkersCompensationLoss/IntentionalAct" />
      <xsl:copy-of select="WorkersCompensationLoss/InvolvedToolObjectName" />
      <xsl:copy-of select="WorkersCompensationLoss/LastTransactionID" />
      <xsl:copy-of select="WorkersCompensationLoss/LossCoverage" />
      <xsl:copy-of select="WorkersCompensationLoss/LossID" />
      <xsl:copy-of select="WorkersCompensationLoss/MachineDefectDescription" />
      <xsl:copy-of select="WorkersCompensationLoss/MachineDefective" />
      <xsl:copy-of select="WorkersCompensationLoss/MachineName" />
      <xsl:copy-of select="WorkersCompensationLoss/MachinePartName" />
      <xsl:copy-of select="WorkersCompensationLoss/NumberOfInjuredEmployees" />
      <xsl:copy-of select="WorkersCompensationLoss/RegulatoryControl" />
      <xsl:copy-of select="WorkersCompensationLoss/SafetyEquipmentSupplied" />
      <xsl:copy-of select="WorkersCompensationLoss/SafetyEquipmentUsed" />
      <xsl:copy-of select="WorkersCompensationLoss/SafetyViolation" />
      <xsl:copy-of select="WorkersCompensationLoss/ScheduledIndemnityWeeks" />
      <xsl:copy-of select="WorkersCompensationLoss/TemporaryBenefitWeeks" />
      <xsl:copy-of select="WorkersCompensationLoss/WorkersCompensationLossType" />
    </WorkersCompensationLoss>
  </xsl:template>

  <xsl:template name="riskLocationTemplate">
    <RiskLocationDetails>
      <xsl:for-each select="RiskLocationDetails/LocationItemDetailsDTO">
        <LocationItemDetails>
          <xsl:for-each select="LocationItemDTO">
            <LocationItem>
              <xsl:call-template name="CommonLocationItemTemplate"/>
              <xsl:copy-of select="LocationDescription" />
            </LocationItem>
          </xsl:for-each>
          <ItemJobClasses>
            <xsl:for-each select="ItemJobClassDTO/ItemJobClassDTO">
              <ItemJobClass>
                <xsl:copy-of select="ItemID" />
                <xsl:copy-of select="ItemJobClassID" />
                <xsl:copy-of select="JobClassCode" />
                <xsl:copy-of select="JobClassDescription" />
                <xsl:copy-of select="LastTransactionID" />
                <xsl:copy-of select="ManualIndicator" />
                <xsl:copy-of select="SelectedIndicator" />
              </ItemJobClass>
            </xsl:for-each>
          </ItemJobClasses>
        </LocationItemDetails>
      </xsl:for-each>
    </RiskLocationDetails>
  </xsl:template>

  <xsl:template name ="commonAddressTypeAssociationTemplate">
    <AddressTypeAssociations>
      <xsl:for-each select="AddressTypeAssociations">
        <AddressTypeAssociation>
          <xsl:for-each select="ClaimsAddressTypeAssociationDTO">
            <xsl:copy-of select="AddressID" />
            <xsl:copy-of select="AddressType" />
            <xsl:copy-of select="AddressTypeAssociationID" />
            <xsl:variable name="claimsAddressTypeAssociationEffectiveDate" select="EffectiveDate" />
            <xsl:choose>
              <xsl:when test="$claimsAddressTypeAssociationEffectiveDate != ''">
                <EffectiveDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($claimsAddressTypeAssociationEffectiveDate))" />
                </EffectiveDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="EffectiveDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="claimsAddressTypeAssociationExpirationDate" select="ExpirationDate" />
            <xsl:choose>
              <xsl:when test="$claimsAddressTypeAssociationExpirationDate != ''">
                <ExpirationDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($claimsAddressTypeAssociationExpirationDate))" />
                </ExpirationDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="ExpirationDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="LastTransactionID" />
            <xsl:variable name="claimsAddressTypeAssociationLastUpdatedDate" select="LastUpdatedDate" />
            <xsl:choose>
              <xsl:when test="$claimsAddressTypeAssociationLastUpdatedDate != ''">
                <LastUpdatedDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($claimsAddressTypeAssociationLastUpdatedDate))" />
                </LastUpdatedDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="LastUpdatedDate"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </AddressTypeAssociation>
      </xsl:for-each>
    </AddressTypeAssociations>
  </xsl:template>

  <xsl:template name="CommonLocationItemTemplate">
    <xsl:for-each select="ItemDTO">
      <Item>
        <xsl:call-template name="commonItemTemplate" />
      </Item>
    </xsl:for-each>
    <xsl:for-each select="ItemSiteDTO">
      <ItemSite>
        <xsl:copy-of select="AddressID" />
        <xsl:copy-of select="ItemID" />
        <xsl:copy-of select="LastTransactionID" />
        <xsl:for-each select="SiteAddressDTO">
          <SiteAddress>
            <xsl:call-template name="commonClaimsAddressTemplate" />
          </SiteAddress>
        </xsl:for-each>
        <xsl:copy-of select="SiteDescription" />
        <xsl:copy-of select="SiteDetailDescription" />
        <xsl:copy-of select="State" />
        <xsl:copy-of select="StatePlaceCode" />
        <xsl:copy-of select="StateZoneCode" />
      </ItemSite>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="claimFinancialSummaryDetailTemplate">
    <ClaimFinancialSummary>
      <xsl:for-each select="ClaimFinancialSummary" >
        <xsl:copy-of select="ClaimID" />
        <xsl:copy-of select="ClaimStatus" />
        <xsl:copy-of select="ExpenseIncurredAmount" />
        <xsl:copy-of select="ExpensePaidAmount" />
        <xsl:copy-of select="ExpenseReceivedAmount" />
        <xsl:copy-of select="ExpenseReserveAmount" />
        <LineFinancialSummaries>
          <xsl:for-each select="LineFinancialSummaryDTO/LineFinancialSummaryDTO">
            <LineFinancialSummary>
              <xsl:copy-of select="ClaimantName" />
              <xsl:copy-of select="ClaimantRoleID" />
              <xsl:copy-of select="CoverageMatchStatus" />
              <xsl:copy-of select="ExpenseCurrentAmount" />
              <xsl:copy-of select="ExpenseIncurredAmount" />
              <xsl:copy-of select="ExpenseOutstandingAmount" />
              <xsl:copy-of select="ExpensePaidAmount" />
              <xsl:copy-of select="ExpenseReceivedAmount" />
              <xsl:copy-of select="ExpenseReserveAmount" />
              <xsl:copy-of select="InitialExpenseReserveAmount" />
              <xsl:copy-of select="InitialLossReserveAmount" />
              <xsl:copy-of select="LineFinancialStatus" />
              <xsl:copy-of select="LineID" />
              <xsl:copy-of select="LineType" />
              <xsl:copy-of select="LossCurrentAmount" />
              <xsl:copy-of select="LossIncurredAmount" />
              <xsl:copy-of select="LossOutstandingAmount" />
              <xsl:copy-of select="LossPaidAmount" />
              <xsl:copy-of select="LossReceivedAmount" />
              <xsl:copy-of select="LossReserveAmount" />
              <xsl:copy-of select="TotalCurrentAmount" />
              <xsl:copy-of select="TotalIncurredAmount" />
              <xsl:copy-of select="TotalInitialReserveAmount" />
              <xsl:copy-of select="TotalOutstandingAmount" />
              <xsl:copy-of select="TotalOutstandingReserveAmount" />
              <xsl:copy-of select="TotalPaidAmount" />
              <xsl:copy-of select="TotalReceivedAmount" />
              <xsl:copy-of select="TotalReserveAmount	" />
            </LineFinancialSummary>
          </xsl:for-each>
        </LineFinancialSummaries>
        <xsl:copy-of select="LossIncurredAmount" />
        <xsl:copy-of select="LossPaidAmount" />
        <xsl:copy-of select="LossReceivedAmount" />
        <xsl:copy-of select="LossReserveAmount" />
        <xsl:copy-of select="TotalIncurredAmount" />
        <xsl:copy-of select="TotalOutstandingReserveAmount" />
        <xsl:copy-of select="TotalPaidAmount" />
        <xsl:copy-of select="TotalReceivedAmount" />
        <xsl:copy-of select="TotalReserveAmount" />
      </xsl:for-each>
    </ClaimFinancialSummary>
  </xsl:template>

  <xsl:template name="coveredPropertyTemplate">
    <xsl:for-each select="CoveredProperties">
      <CoveredProperties>
        <xsl:for-each select="PropertyItemDTO">
          <xsl:choose>
            <xsl:when test="ItemDTO[CoveredIndicator='true']" >
              <PropertyItem>
                <xsl:call-template name="CommonLocationItemTemplate" />
              </PropertyItem>
            </xsl:when>
          </xsl:choose>
        </xsl:for-each>
      </CoveredProperties>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="coveredPersonTemplate">
    <xsl:for-each select="CoveredPersons">
      <CoveredPersons>
        <xsl:for-each select="PersonItemDTO">
          <PersonItem>
            <ExternalPolicyParty>
              <xsl:copy-of select="ExternalPolicyParty/EmailAddress" />
              <xsl:copy-of select="ExternalPolicyParty/EmailType" />
              <xsl:copy-of select="ExternalPolicyParty/ExternalID" />
              <xsl:copy-of select="ExternalPolicyParty/ExternalPartyEmailID" />
              <xsl:copy-of select="ExternalPolicyParty/ExternalPartyNameID" />
              <xsl:copy-of select="ExternalPolicyParty/ExternalPartyPhoneID" />
              <xsl:copy-of select="ExternalPolicyParty/FaxCountry" />
              <xsl:copy-of select="ExternalPolicyParty/FaxNumber" />
              <xsl:copy-of select="ExternalPolicyParty/FirstName" />
              <xsl:copy-of select="ExternalPolicyParty/GenderCode" />
              <xsl:copy-of select="ExternalPolicyParty/InternalPartyID" />
              <xsl:copy-of select="ExternalPolicyParty/ItemDetailedType" />
              <xsl:copy-of select="ExternalPolicyParty/LastName" />
              <xsl:copy-of select="ExternalPolicyParty/MiddleName" />
              <xsl:copy-of select="ExternalPolicyParty/NameSuffix" />
              <xsl:copy-of select="ExternalPolicyParty/OrganizationName" />
              <PartyAddress>
                <xsl:copy-of select="ExternalPolicyParty/PartyAddress/AdditionalAddress1" />
                <AdditionalAddressParameters>
                  <xsl:for-each select="ExternalPolicyParty/PartyAddress/AdditionalAddressParametersDTO/AdditionalAddressParametersDTO">
                    <AdditionalAddressParameter>
                      <xsl:copy-of select="AddressFieldName" />
                      <xsl:copy-of select="AddressFieldValue" />
                    </AdditionalAddressParameter>
                  </xsl:for-each>
                </AdditionalAddressParameters>
                <xsl:copy-of select="ExternalPolicyParty/PartyAddress/City" />
                <xsl:copy-of select="ExternalPolicyParty/PartyAddress/Country" />
                <xsl:copy-of select="ExternalPolicyParty/PartyAddress/County" />
                <xsl:copy-of select="ExternalPolicyParty/PartyAddress/ExternalPartyAddressID" />
                <xsl:copy-of select="ExternalPolicyParty/PartyAddress/State" />
                <xsl:copy-of select="ExternalPolicyParty/PartyAddress/Street" />
                <xsl:copy-of select="ExternalPolicyParty/PartyAddress/ZipCode" />
              </PartyAddress>
              <xsl:copy-of select="ExternalPolicyParty/PhoneCountry" />
              <xsl:copy-of select="ExternalPolicyParty/PhoneNumber" />
              <xsl:copy-of select="ExternalPolicyParty/PolicyHolderDOB" />
              <xsl:copy-of select="ExternalPolicyParty/TaxID" />
            </ExternalPolicyParty>
            <xsl:call-template name="commonPersonTemplate" />
          </PersonItem>
        </xsl:for-each>
      </CoveredPersons>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="commonPersonTemplate" >
    <xsl:for-each select="ItemDTO">
      <Item>
        <xsl:call-template name="commonItemTemplate" />
      </Item>
    </xsl:for-each>
    <xsl:for-each select="ItemGroupDTO">
      <ItemGroup>
        <xsl:copy-of select="ClaimID" />
        <xsl:copy-of select="ItemGroupID" />
        <xsl:copy-of select="ItemGroupName" />
        <xsl:copy-of select="LastTransactionID" />
      </ItemGroup>
    </xsl:for-each>
    <xsl:for-each select="ItemPersonDTO">
      <ItemPerson>
        <xsl:copy-of select="ItemID" />
        <xsl:copy-of select="LastTransactionID" />
        <xsl:copy-of select="ParticipantID" />
      </ItemPerson>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="commonBusinessPartyTemplate" >
    <xsl:for-each select="PartyBusinessDetail">
      <PartyBusinessDetail>
        <xsl:for-each select="Party">
          <Party>
            <xsl:call-template name="commonPartyDetailsTemplate" />
          </Party>
        </xsl:for-each>
        <xsl:call-template name="commonPartyAddressTemplate" />
        <xsl:for-each select="PartyAgency">
          <PartyAgency>
            <xsl:copy-of select="AgencySpecialityType" />
            <xsl:copy-of select="AgencySubCode" />
            <xsl:variable name="partyAgencyEffectiveDate" select="EffectiveDate" />
            <xsl:choose>
              <xsl:when test="$partyAgencyEffectiveDate != ''">
                <EffectiveDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyAgencyEffectiveDate))" />
                </EffectiveDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="EffectiveDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="partyAgencyExpirationDate" select="ExpirationDate" />
            <xsl:choose>
              <xsl:when test="$partyAgencyExpirationDate != ''">
                <ExpirationDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyAgencyExpirationDate))" />
                </ExpirationDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="ExpirationDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="LastTransactionID" />
            <xsl:variable name="partyAgencyLastUpdatedDate" select="LastUpdateDate" />
            <xsl:choose>
              <xsl:when test="$partyAgencyLastUpdatedDate != ''">
                <LastUpdatedDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyAgencyLastUpdatedDate))" />
                </LastUpdatedDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="LastUpdateDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="PartyID" />
            <xsl:copy-of select="ProducerNumber" />
          </PartyAgency>
        </xsl:for-each>
        <xsl:for-each select="PartyBusiness">
          <PartyBusiness>
            <xsl:copy-of select="BusinessTypeCode" />
            <xsl:variable name="partyBusinessEffectiveDate" select="EffectiveDate" />
            <xsl:choose>
              <xsl:when test="$partyBusinessEffectiveDate != ''">
                <EffectiveDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyBusinessEffectiveDate))" />
                </EffectiveDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="EffectiveDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="partyBusinessExpirationDate" select="ExpirationDate" />
            <xsl:choose>
              <xsl:when test="$partyBusinessExpirationDate != ''">
                <ExpirationDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyBusinessExpirationDate))" />
                </ExpirationDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="ExpirationDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="IsAgency" />
            <xsl:copy-of select="IsSupplier" />
            <xsl:copy-of select="LastTransactionID" />
            <xsl:variable name="partyBusinessLastUpdatedDate" select="LastUpdateDate" />
            <xsl:choose>
              <xsl:when test="$partyBusinessLastUpdatedDate != ''">
                <LastUpdatedDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyBusinessLastUpdatedDate))" />
                </LastUpdatedDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="LastUpdateDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="PartyID" />
            <xsl:copy-of select="PreferredContactType" />
            <xsl:copy-of select="PreferredLanguage" />
            <xsl:copy-of select="RegistrationID1" />
            <xsl:copy-of select="RegistrationID2" />
            <xsl:copy-of select="RegistrationID3" />
          </PartyBusiness>
        </xsl:for-each>
        <PartyBusNamesDetail>
          <xsl:for-each select="PartyBusNameDetail/ClaimsPartyBusNameDetailDTO">
            <PartyBusNameDetail>
              <xsl:for-each select="PartyBusName">
                <PartyBusName>
                  <xsl:copy-of select="Comment" />
                  <xsl:copy-of select="LastTransactionID" />
                  <xsl:variable name="partyBusNameLastUpdatedDate" select="LastUpdateDate" />
                  <xsl:choose>
                    <xsl:when test="$partyBusNameLastUpdatedDate != ''">
                      <LastUpdatedDate>
                        <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyBusNameLastUpdatedDate))" />
                      </LastUpdatedDate>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:copy-of select="LastUpdateDate"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:copy-of select="Name" />
                  <xsl:copy-of select="NamePhonetic" />
                  <xsl:copy-of select="NameSoundex" />
                  <xsl:copy-of select="NameTypeCode" />
                  <xsl:copy-of select="NameUpperCase" />
                  <xsl:copy-of select="PartyBusNameID" />
                </PartyBusName>
              </xsl:for-each>
              <xsl:for-each select="PartyName">
                <PartyName>
                  <xsl:copy-of select="AllowPartial" />
                  <xsl:copy-of select="CountryCode" />
                  <xsl:variable name="partyNameEffectiveDate" select="EffectiveDate" />
                  <xsl:choose>
                    <xsl:when test="$partyNameEffectiveDate != ''">
                      <EffectiveDate>
                        <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyNameEffectiveDate))" />
                      </EffectiveDate>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:copy-of select="EffectiveDate"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:variable name="partyNameExpirationDate" select="ExpirationDate" />
                  <xsl:choose>
                    <xsl:when test="$partyNameExpirationDate != ''">
                      <ExpirationDate>
                        <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyNameExpirationDate))" />
                      </ExpirationDate>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:copy-of select="ExpirationDate"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:copy-of select="ExternalPartyNameID" />
                  <xsl:copy-of select="GeographicFormatCode" />
                  <xsl:copy-of select="IsDefault" />
                  <xsl:copy-of select="IsPrimary" />
                  <xsl:copy-of select="IsTaxName" />
                  <xsl:copy-of select="LastTransactionID" />
                  <xsl:variable name="partyNameLastUpdatedDate" select="LastUpdateDate" />
                  <xsl:choose>
                    <xsl:when test="$partyNameLastUpdatedDate != ''">
                      <LastUpdatedDate>
                        <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyNameLastUpdatedDate))" />
                      </LastUpdatedDate>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:copy-of select="LastUpdateDate"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:copy-of select="PartyID" />
                  <xsl:copy-of select="PartyNameID" />
                </PartyName>
              </xsl:for-each>
            </PartyBusNameDetail>
          </xsl:for-each>
        </PartyBusNamesDetail>
        <xsl:call-template name="commonPartyCommentTemplate" />
        <xsl:call-template name="commonPartyEmailTemplate" />
        <xsl:call-template name="commonPartyLicenseTemplate" />
        <xsl:call-template name="commonPartyMembershipTemplate" />
        <xsl:call-template name="commonPartyPhoneTemplate" />
        <xsl:call-template name="commonPartyRelationTemplate" />
      </PartyBusinessDetail>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="commonIndividualPartyTemplate" >
    <xsl:for-each select="PartyIndividualDetail">
      <PartyIndividualDetail>
        <xsl:for-each select="Party">
          <Party>
            <xsl:call-template name="commonPartyDetailsTemplate" />
          </Party>
        </xsl:for-each>
        <xsl:call-template name="commonPartyAddressTemplate" />
        <xsl:for-each select="PartyAgent">
          <PartyAgent>
            <xsl:variable name="partyAgentEffectiveDate" select="EffectiveDate" />
            <xsl:choose>
              <xsl:when test="$partyAgentEffectiveDate != ''">
                <EffectiveDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyAgentEffectiveDate))" />
                </EffectiveDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="EffectiveDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="partyAgentExpirationDate" select="ExpirationDate" />
            <xsl:choose>
              <xsl:when test="$partyAgentExpirationDate != ''">
                <ExpirationDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyAgentExpirationDate))" />
                </ExpirationDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="ExpirationDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="LastTransactionID" />
            <xsl:variable name="partyAgentLastUpdatedDate" select="LastUpdateDate" />
            <xsl:choose>
              <xsl:when test="$partyAgentLastUpdatedDate != ''">
                <LastUpdatedDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyAgentLastUpdatedDate))" />
                </LastUpdatedDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="LastUpdateDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="PartyID" />
            <xsl:copy-of select="ProducerNumber" />
            <xsl:copy-of select="ProducerSubCode" />
            <xsl:copy-of select="SpecialtyType" />
          </PartyAgent>
        </xsl:for-each>
        <xsl:call-template name="commonPartyCommentTemplate" />
        <xsl:call-template name="commonPartyEmailTemplate" />
        <xsl:for-each select="PartyIndividual">
          <PartyIndividual>
            <xsl:variable name="partyIndividualDateOfBirth" select="DateOfBirth" />
            <xsl:choose>
              <xsl:when test="$partyIndividualDateOfBirth != ''">
                <DateOfBirth>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyIndividualDateOfBirth))" />
                </DateOfBirth>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="DateOfBirth"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="partyIndividualDateOfDeath" select="DateOfDeath" />
            <xsl:choose>
              <xsl:when test="$partyIndividualDateOfDeath != ''">
                <DateOfDeath>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyIndividualDateOfDeath))" />
                </DateOfDeath>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="DateOfDeath"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="partyIndividualEffectiveDate" select="EffectiveDate" />
            <xsl:choose>
              <xsl:when test="$partyIndividualEffectiveDate != ''">
                <EffectiveDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyIndividualEffectiveDate))" />
                </EffectiveDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="EffectiveDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="partyIndividualExpirationDate" select="ExpirationDate" />
            <xsl:choose>
              <xsl:when test="$partyIndividualExpirationDate != ''">
                <ExpirationDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyIndividualExpirationDate))" />
                </ExpirationDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="ExpirationDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="GenderCode" />
            <xsl:copy-of select="IsAgent" />
            <xsl:copy-of select="IsCompanyPersonnel" />
            <xsl:copy-of select="LastTransactionID" />
            <xsl:variable name="partyIndividualLastUpdatedDate" select="LastUpdatedDate" />
            <xsl:choose>
              <xsl:when test="$partyIndividualLastUpdatedDate != ''">
                <LastUpdatedDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyIndividualLastUpdatedDate))" />
                </LastUpdatedDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="LastUpdatedDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="MaritalStatusCode" />
            <xsl:copy-of select="NationalID" />
            <xsl:copy-of select="NumberOfDependents" />
            <xsl:copy-of select="PartyID" />
            <xsl:copy-of select="PreferredContactType" />
            <xsl:copy-of select="PreferredLanguage" />
            <xsl:copy-of select="ProfessionCode" />
            <xsl:copy-of select="SpecificPurposeID"/>
          </PartyIndividual>
        </xsl:for-each>
        <xsl:for-each select="PartyIndividualDriverLicense">
          <PartyIndividualDriverLicense>
            <xsl:variable name="partyIndDrivingLicEffectiveDate" select="EffectiveDate" />
            <xsl:choose>
              <xsl:when test="$partyIndDrivingLicEffectiveDate != ''">
                <EffectiveDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyIndDrivingLicEffectiveDate))" />
                </EffectiveDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="EffectiveDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="partyIndDrivingLicExpirationDate" select="ExpirationDate" />
            <xsl:choose>
              <xsl:when test="$partyIndDrivingLicExpirationDate != ''">
                <ExpirationDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyIndDrivingLicExpirationDate))" />
                </ExpirationDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="ExpirationDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="partyIndDrivingLicExpirationLicenseDate" select="ExpirationLicenseDate" />
            <xsl:choose>
              <xsl:when test="$partyIndDrivingLicExpirationDate != ''">
                <ExpirationLicenseDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyIndDrivingLicExpirationDate))" />
                </ExpirationLicenseDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="ExpirationLicenseDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="LastTransactionID" />
            <xsl:variable name="partyIndDrivingLicLastUpdatedDate" select="LastUpdatedDate" />
            <xsl:choose>
              <xsl:when test="$partyIndDrivingLicLastUpdatedDate != ''">
                <LastUpdatedDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyIndDrivingLicLastUpdatedDate))" />
                </LastUpdatedDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="LastUpdatedDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="PartyID" />
            <xsl:copy-of select="PartyLicenseCountry" />
            <xsl:copy-of select="PartyLicenseID" />
            <xsl:copy-of select="PartyLicenseIss" />
            <xsl:copy-of select="PartyLicenseNumber" />
            <xsl:copy-of select="PartyLicensePlcIss" />
            <xsl:copy-of select="PartyLicenseStatus" />
          </PartyIndividualDriverLicense>
        </xsl:for-each>
        <PartyIndNamesDetail>
          <xsl:for-each select="PartyIndNameDetail/ClaimsPartyIndNameDetailDTO">
            <PartyIndNameDetail>
              <xsl:for-each select="PartyName">
                <PartyName>
                  <xsl:copy-of select="AllowPartial" />
                  <xsl:copy-of select="CountryCode" />
                  <xsl:variable name="partyNameEffectiveDate" select="EffectiveDate" />
                  <xsl:choose>
                    <xsl:when test="$partyNameEffectiveDate != ''">
                      <EffectiveDate>
                        <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyNameEffectiveDate))" />
                      </EffectiveDate>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:copy-of select="EffectiveDate"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:variable name="partyNameExpirationDate" select="ExpirationDate" />
                  <xsl:choose>
                    <xsl:when test="$partyNameExpirationDate != ''">
                      <ExpirationDate>
                        <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyNameExpirationDate))" />
                      </ExpirationDate>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:copy-of select="ExpirationDate"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:copy-of select="ExternalPartyNameID" />
                  <xsl:copy-of select="GeographicFormatCode" />
                  <xsl:copy-of select="IsDefault" />
                  <xsl:copy-of select="IsPrimary" />
                  <xsl:copy-of select="IsTaxName" />
                  <xsl:copy-of select="LastTransactionID" />
                  <xsl:variable name="partyNameLastUpdatedDate" select="LastUpdatedDate" />
                  <xsl:choose>
                    <xsl:when test="$partyNameLastUpdatedDate != ''">
                      <LastUpdatedDate>
                        <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyNameLastUpdatedDate))" />
                      </LastUpdatedDate>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:copy-of select="LastUpdatedDate"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:copy-of select="PartyID" />
                  <xsl:copy-of select="PartyNameID" />
                </PartyName>
              </xsl:for-each>
              <xsl:for-each select="PartyIndName">
                <PartyIndName>
                  <xsl:copy-of select="Comment" />
                  <xsl:copy-of select="FirstName" />
                  <xsl:copy-of select="FirstNamePhonetic" />
                  <xsl:copy-of select="FirstNameSoundex" />
                  <xsl:copy-of select="FirstNameUpperCase" />
                  <xsl:copy-of select="FullName" />
                  <xsl:copy-of select="LastName" />
                  <xsl:copy-of select="LastNamePhonetic" />
                  <xsl:copy-of select="LastNameSoundex" />
                  <xsl:copy-of select="LastNameUpperCase" />
                  <xsl:copy-of select="LastTransactionID" />
                  <xsl:variable name="partyIndNameLastUpdatedDate" select="LastUpdatedDate" />
                  <xsl:choose>
                    <xsl:when test="$partyIndNameLastUpdatedDate != ''">
                      <LastUpdatedDate>
                        <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyIndNameLastUpdatedDate))" />
                      </LastUpdatedDate>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:copy-of select="LastUpdatedDate"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:copy-of select="MiddleName" />
                  <xsl:copy-of select="MiddleNamePhonetic" />
                  <xsl:copy-of select="MiddleNameSoundex" />
                  <xsl:copy-of select="MiddleNameUpperCase" />
                  <xsl:copy-of select="NameTypeCode" />
                  <xsl:copy-of select="PartyIndNameID" />
                  <xsl:copy-of select="PrefixCode" />
                  <xsl:copy-of select="SuffixCode" />
                </PartyIndName>
              </xsl:for-each>
            </PartyIndNameDetail>
          </xsl:for-each>
        </PartyIndNamesDetail>
        <xsl:call-template name="commonPartyLicenseTemplate" />
        <xsl:call-template name="commonPartyMembershipTemplate" />
        <xsl:call-template name="commonPartyPhoneTemplate" />
        <xsl:call-template name="commonPartyRelationTemplate" />
      </PartyIndividualDetail>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="commonInternalPartyTemplate" >
    <xsl:for-each select="PartyInternalDetail">
      <PartyInternalDetail>
        <xsl:for-each select="Party">
          <Party>
            <xsl:call-template name="commonPartyDetailsTemplate" />
          </Party>
        </xsl:for-each>
        <xsl:call-template name="commonPartyAddressTemplate" />
        <xsl:for-each select="PartyBusiness">
          <PartyBusiness>
            <xsl:copy-of select="BusinessTypeCode" />
            <xsl:variable name="partyInternalEffectiveDate" select="EffectiveDate" />
            <xsl:choose>
              <xsl:when test="$partyInternalEffectiveDate != ''">
                <EffectiveDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyInternalEffectiveDate))" />
                </EffectiveDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="EffectiveDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="partyInternalExpirationDate" select="ExpirationDate" />
            <xsl:choose>
              <xsl:when test="$partyInternalExpirationDate != ''">
                <ExpirationDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyInternalExpirationDate))" />
                </ExpirationDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="ExpirationDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="IsAgency" />
            <xsl:copy-of select="IsSupplier" />
            <xsl:copy-of select="LastTransactionID" />
            <xsl:variable name="partyInternalLastUpdatedDate" select="LastUpdatedDate" />
            <xsl:choose>
              <xsl:when test="$partyInternalLastUpdatedDate != ''">
                <LastUpdatedDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyInternalLastUpdatedDate))" />
                </LastUpdatedDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="LastUpdatedDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="PartyID" />
            <xsl:copy-of select="PreferredContactType" />
            <xsl:copy-of select="PreferredLanguage" />
            <xsl:copy-of select="RegistrationID1" />
            <xsl:copy-of select="RegistrationID2" />
            <xsl:copy-of select="RegistrationID3" />
          </PartyBusiness>
        </xsl:for-each>
        <PartyBusNamesDetail>
          <xsl:for-each select="PartyBusNameDetail/ClaimsPartyBusNameDetailDTO">
            <PartyBusNameDetail>
              <xsl:for-each select="PartyName">
                <PartyName>
                  <xsl:copy-of select="AllowPartial" />
                  <xsl:copy-of select="CountryCode" />
                  <xsl:variable name="partyInternalNameEffectiveDate" select="EffectiveDate" />
                  <xsl:choose>
                    <xsl:when test="$partyInternalNameEffectiveDate != ''">
                      <EffectiveDate>
                        <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyInternalNameEffectiveDate))" />
                      </EffectiveDate>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:copy-of select="EffectiveDate"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:variable name="partyInternalNameExpirationDate" select="ExpirationDate" />
                  <xsl:choose>
                    <xsl:when test="$partyInternalNameExpirationDate != ''">
                      <ExpirationDate>
                        <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyInternalNameExpirationDate))" />
                      </ExpirationDate>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:copy-of select="ExpirationDate"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:copy-of select="ExternalPartyNameID" />
                  <xsl:copy-of select="GeographicFormatCode" />
                  <xsl:copy-of select="IsDefault" />
                  <xsl:copy-of select="IsPrimary" />
                  <xsl:copy-of select="IsTaxName" />
                  <xsl:copy-of select="LastTransactionID" />
                  <xsl:variable name="partyInternalNameLastUpdatedDate" select="LastUpdatedDate" />
                  <xsl:choose>
                    <xsl:when test="$partyInternalNameLastUpdatedDate != ''">
                      <LastUpdatedDate>
                        <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyInternalNameLastUpdatedDate))" />
                      </LastUpdatedDate>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:copy-of select="LastUpdatedDate"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:copy-of select="PartyID" />
                  <xsl:copy-of select="PartyNameID" />
                </PartyName>
              </xsl:for-each>
              <xsl:for-each select="PartyBusName">
                <PartyBusName>
                  <xsl:copy-of select="Comment" />
                  <xsl:copy-of select="LastTransactionID" />
                  <xsl:variable name="partyIntNameLastUpdatedDate" select="LastUpdatedDate" />
                  <xsl:choose>
                    <xsl:when test="$partyIntNameLastUpdatedDate != ''">
                      <LastUpdatedDate>
                        <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyIntNameLastUpdatedDate))" />
                      </LastUpdatedDate>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:copy-of select="LastUpdatedDate"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:copy-of select="Name" />
                  <xsl:copy-of select="NamePhonetic" />
                  <xsl:copy-of select="NameSoundex" />
                  <xsl:copy-of select="NameTypeCode" />
                  <xsl:copy-of select="NameUpperCase" />
                  <xsl:copy-of select="PartyBusNameID" />
                </PartyBusName>
              </xsl:for-each>
            </PartyBusNameDetail>
          </xsl:for-each>
        </PartyBusNamesDetail>
        <xsl:call-template name="commonPartyCommentTemplate" />
        <xsl:call-template name="commonPartyEmailTemplate" />
        <xsl:call-template name="commonPartyLicenseTemplate" />
        <xsl:call-template name="commonPartyMembershipTemplate" />
        <xsl:for-each select="PartyOrgInfo">
          <PartyOrgInfo>
            <xsl:variable name="partyOrgInfoActiveDate" select="ActiveDate" />
            <xsl:choose>
              <xsl:when test="$partyOrgInfoActiveDate != ''">
                <ActiveDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyOrgInfoActiveDate))" />
                </ActiveDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="ActiveDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="partyOrgInfoDeActiveDate" select="DeActiveDate" />
            <xsl:choose>
              <xsl:when test="$partyOrgInfoDeActiveDate != ''">
                <DeActiveDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyOrgInfoDeActiveDate))" />
                </DeActiveDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="DeActiveDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="partyOrgInfoEffectiveDate" select="EffectiveDate" />
            <xsl:choose>
              <xsl:when test="$partyOrgInfoEffectiveDate != ''">
                <EffectiveDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyOrgInfoEffectiveDate))" />
                </EffectiveDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="EffectiveDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="partyOrgInfoExpirationDate" select="ExpirationDate" />
            <xsl:choose>
              <xsl:when test="$partyOrgInfoExpirationDate != ''">
                <ExpirationDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyOrgInfoExpirationDate))" />
                </ExpirationDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="ExpirationDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="IsPerformer" />
            <xsl:copy-of select="IsVirtualOffice" />
            <xsl:copy-of select="JobTitleCode" />
            <xsl:copy-of select="LastTransactionID" />
            <xsl:variable name="partyOrgInfoLastUpdatedDate" select="LastUpdatedDate" />
            <xsl:choose>
              <xsl:when test="$partyOrgInfoLastUpdatedDate != ''">
                <LastUpdatedDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyOrgInfoLastUpdatedDate))" />
                </LastUpdatedDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="LastUpdatedDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="OrgTypeCode" />
            <xsl:copy-of select="PartyID" />
            <xsl:copy-of select="UserProfileID" />
          </PartyOrgInfo>
        </xsl:for-each>
        <xsl:call-template name="commonPartyPhoneTemplate" />
        <xsl:call-template name="commonPartyRelationTemplate" />
      </PartyInternalDetail>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="commonPartyPhoneTemplate">
    <xsl:for-each select="PartyPhone">
      <PartyPhones>
        <xsl:for-each select="ClaimsPartyPhoneDTO">
          <PartyPhone>
            <xsl:copy-of select="Comment" />
            <xsl:copy-of select="CountryCode" />
            <xsl:variable name="partyPhoneEffectiveDate" select="EffectiveDate" />
            <xsl:choose>
              <xsl:when test="$partyPhoneEffectiveDate != ''">
                <EffectiveDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyPhoneEffectiveDate))" />
                </EffectiveDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="EffectiveDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="partyPhoneExpirationDate" select="ExpirationDate" />
            <xsl:choose>
              <xsl:when test="$partyPhoneExpirationDate != ''">
                <ExpirationDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyPhoneExpirationDate))" />
                </ExpirationDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="ExpirationDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="ExternalPartyPhoneID" />
            <xsl:copy-of select="FullPhoneNumber" />
            <xsl:copy-of select="GeographicFormatCode" />
            <xsl:copy-of select="IsPrimary" />
            <xsl:copy-of select="LastTransactionID" />
            <xsl:variable name="partyPhoneLastUpdatedDate" select="LastUpdatedDate" />
            <xsl:choose>
              <xsl:when test="$partyPhoneLastUpdatedDate != ''">
                <LastUpdatedDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyPhoneLastUpdatedDate))" />
                </LastUpdatedDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="LastUpdatedDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="PartyID" />
            <xsl:copy-of select="PartyPhoneID" />
            <xsl:copy-of select="PhoneExtension" />
            <xsl:copy-of select="PhoneTypeCode" />
          </PartyPhone>
        </xsl:for-each>
      </PartyPhones>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="commonPartyEmailTemplate">
    <xsl:for-each select="PartyEmail">
      <PartyEmails>
        <xsl:for-each select="ClaimsPartyEmailDTO">
          <PartyEmail>
            <xsl:copy-of select="Comment" />
            <xsl:variable name="partyEmailEffectiveDate" select="EffectiveDate" />
            <xsl:choose>
              <xsl:when test="$partyEmailEffectiveDate != ''">
                <EffectiveDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyEmailEffectiveDate))" />
                </EffectiveDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="EffectiveDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="EmailAddress" />
            <xsl:copy-of select="EmailTypeCode" />
            <xsl:variable name="partyEmailExpirationDate" select="ExpirationDate" />
            <xsl:choose>
              <xsl:when test="$partyEmailExpirationDate != ''">
                <ExpirationDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyEmailExpirationDate))" />
                </ExpirationDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="ExpirationDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="ExternalPartyEmailID" />
            <xsl:copy-of select="IsPrimary" />
            <xsl:copy-of select="LastTransactionID" />
            <xsl:variable name="partyEmailLastUpdatedDate" select="LastUpdatedDate" />
            <xsl:choose>
              <xsl:when test="$partyEmailLastUpdatedDate != ''">
                <LastUpdatedDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyEmailLastUpdatedDate))" />
                </LastUpdatedDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="LastUpdatedDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="PartyEmailID" />
            <xsl:copy-of select="PartyID" />
          </PartyEmail>
        </xsl:for-each>
      </PartyEmails>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="litigationDetailTemplate">
    <LitigationDetailsList>
      <xsl:for-each select="LitigationDetails/LitigationDetailsDTO">
        <LitigationDetails>
          <xsl:for-each select="LitigationDTO">
            <Litigation>
              <AttorneyLitigationAssociations>
                <xsl:for-each select="AttorneyLitigationAssociationDTO/AttorneyLitigationAssociationDTO">
                  <AttorneyLitigationAssociation>
                    <xsl:copy-of select="AttorneyName" />
                    <xsl:for-each select="AttorneyNameDetailDTO">
                      <AttorneyNameDetails>
                        <xsl:call-template name="commonClaimsNameDetailTemplate" />
                      </AttorneyNameDetails>
                    </xsl:for-each>
                    <xsl:copy-of select="LastTransactionID" />
                    <xsl:copy-of select="LitigationID" />
                    <xsl:copy-of select="ParticipantID" />
                  </AttorneyLitigationAssociation>
                </xsl:for-each>
              </AttorneyLitigationAssociations>
              <xsl:copy-of select="Caption" />
              <xsl:copy-of select="ClaimID" />
              <xsl:variable name="litigationCloseDate" select="CloseDate" />
              <xsl:choose>
                <xsl:when test="$litigationCloseDate != ''">
                  <CloseDate>
                    <xsl:value-of select="string(Util:ConvertDateToDuckFormat($litigationCloseDate))" />
                  </CloseDate>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:copy-of select="CloseDate"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:copy-of select="Country" />
              <xsl:copy-of select="County" />
              <xsl:copy-of select="DateToAttorney" />
              <xsl:copy-of select="DefenseType" />
              <xsl:variable name="litigationDisclosureDate" select="DisclosureDate" />
              <xsl:choose>
                <xsl:when test="$litigationDisclosureDate != ''">
                  <DisclosureDate>
                    <xsl:value-of select="string(Util:ConvertDateToDuckFormat($litigationDisclosureDate))" />
                  </DisclosureDate>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:copy-of select="DisclosureDate"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:copy-of select="Disposition" />
              <xsl:copy-of select="LastTransactionID" />
              <LineLitigationAssociations>
                <xsl:for-each select="LineLitigationAssociationDTOs/LineLitigationAssociationDTO">
                  <LineLitigationAssociation>
                    <xsl:copy-of select="ClaimantLineName" />
                    <xsl:copy-of select="LastTransactionID" />
                    <xsl:copy-of select="LineIDDetail" />
                    <xsl:copy-of select="LitigationIDDetail" />
                  </LineLitigationAssociation>
                </xsl:for-each>
              </LineLitigationAssociations>
              <LitigationAndParticipantAssociations>
                <xsl:for-each select="LitigationAndParticipantAssociationDTO/LitigationAndParticipantAssociationDTO">
                  <LitigationAndParticipantAssociation>
                    <xsl:copy-of select="LastTransactionID" />
                    <xsl:copy-of select="LastUpdate" />
                    <xsl:copy-of select="LitigationID" />
                    <xsl:copy-of select="ParticipantFullName" />
                    <xsl:copy-of select="ParticipantID" />
                    <xsl:for-each select="ParticipantNameDetailDTO">
                      <ParticipantNameDetail>
                        <xsl:call-template name="commonParticipantNameDetailTemplate" />
                      </ParticipantNameDetail>
                    </xsl:for-each>
                    <xsl:copy-of select="RecordDeleteIndicator" />
                  </LitigationAndParticipantAssociation>
                </xsl:for-each>
              </LitigationAndParticipantAssociations>
              <xsl:copy-of select="LitigationID" />
              <xsl:copy-of select="LitigationType" />
              <xsl:copy-of select="MarkInErrorIndicator" />
              <xsl:copy-of select="State" />
              <xsl:copy-of select="Status" />
              <xsl:variable name="litigationTrialDate" select="TrialDate" />
              <xsl:choose>
                <xsl:when test="$litigationTrialDate != ''">
                  <TrialDate>
                    <xsl:value-of select="string(Util:ConvertDateToDuckFormat($litigationTrialDate))" />
                  </TrialDate>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:copy-of select="TrialDate"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:copy-of select="Venue" />
            </Litigation>
          </xsl:for-each>

          <AttorneyLitigationAssociations>
            <xsl:for-each select="AttorneyLitigationAssociationDTO/AttorneyLitigationAssociationDTO">
              <AttorneyLitigationAssociation>
                <xsl:copy-of select="AttorneyName" />
                <xsl:for-each select="AttorneyNameDetailDTO">
                  <AttorneyNameDetails>
                    <xsl:call-template name="commonClaimsNameDetailTemplate" />
                  </AttorneyNameDetails>
                </xsl:for-each>
                <xsl:copy-of select="LastTransactionID" />
                <xsl:copy-of select="LitigationID" />
                <xsl:copy-of select="ParticipantID" />
              </AttorneyLitigationAssociation>
            </xsl:for-each>
          </AttorneyLitigationAssociations>
        </LitigationDetails>
      </xsl:for-each>
    </LitigationDetailsList>
  </xsl:template>

  <xsl:template name="liabilityDetailTemplate">
    <LiabilityDetails>
      <xsl:for-each select="LiabilityDetails/LiabilityDetailDTO">
        <Liability>
          <xsl:copy-of select="Liability/AssessmentCreator" />
          <xsl:copy-of select="Liability/ClaimID" />
          <xsl:copy-of select="Liability/Comments" />
          <xsl:variable name="liabilityDate" select="Liability/Date" />
          <xsl:choose>
            <xsl:when test="$liabilityDate != ''">
              <Date>
                <xsl:value-of select="string(Util:ConvertDateToDuckFormat($liabilityDate))" />
              </Date>
            </xsl:when>
            <xsl:otherwise>
              <xsl:copy-of select="Liability/Date"/>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:copy-of select="Liability/Direction" />
          <xsl:copy-of select="Liability/Establishment" />
          <xsl:copy-of select="Liability/LastTransactionID" />
          <xsl:copy-of select="Liability/LiabilityID" />
          <xsl:copy-of select="Liability/ParticipantID" />
          <xsl:copy-of select="Liability/Percentage" />
          <xsl:copy-of select="Liability/PercentageIndicator" />
          <xsl:copy-of select="Liability/PercentageType" />
        </Liability>
		<LiabilityLineAssociations>
			<xsl:for-each select="LiabilityLineAssociations/LiabilityLineAssociationDTO">
				<LiabilityLineAssociation>
					<xsl:copy-of select="LiabilityLineAssociationID" />
					<xsl:copy-of select="LiabilityID" />
					<xsl:copy-of select="LineID" />
				</LiabilityLineAssociation>
			</xsl:for-each>
		</LiabilityLineAssociations>
		<xsl:copy-of select="LineIDs" />
      </xsl:for-each>
    </LiabilityDetails>
  </xsl:template>

  <xsl:template name="commonClaimsAddressTemplate">
    <xsl:copy-of select="AddressID" />
    <xsl:copy-of select="AddressType" />
    <xsl:call-template name="commonAddressTypeAssociationTemplate" />
    <xsl:copy-of select="AdminDivisionPrimary" />
    <xsl:copy-of select="AdminDivisionSecondary" />
    <xsl:copy-of select="AdminDivisionTertiary" />
    <xsl:copy-of select="AllowPartial" />
    <xsl:copy-of select="Attention" />
    <xsl:copy-of select="CountryCode" />
    <xsl:variable name="claimAddressEffectiveDate" select="EffectiveDate" />
    <xsl:choose>
      <xsl:when test="$claimAddressEffectiveDate != ''">
        <EffectiveDate>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($claimAddressEffectiveDate))" />
        </EffectiveDate>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="EffectiveDate"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:variable name="claimAddressExpirationDate" select="ExpirationDate" />
    <xsl:choose>
      <xsl:when test="$claimAddressExpirationDate != ''">
        <ExpirationDate>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($claimAddressExpirationDate))" />
        </ExpirationDate>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="ExpirationDate"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:copy-of select="FullAddress" />
    <xsl:copy-of select="GeographicFormatCode" />
    <xsl:copy-of select="LastTransactionID" />
    <xsl:variable name="claimAddressLastUpdatedDate" select="LastUpdatedDate" />
    <xsl:choose>
      <xsl:when test="$claimAddressLastUpdatedDate != ''">
        <LastUpdatedDate>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($claimAddressLastUpdatedDate))" />
        </LastUpdatedDate>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="LastUpdatedDate"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:copy-of select="Latitude" />
    <xsl:copy-of select="LocationDetailsLine1" />
    <xsl:copy-of select="LocationDetailsLine2" />
    <xsl:copy-of select="LocationDetailsLine3" />
    <xsl:copy-of select="LocationDetailsLine4" />
    <xsl:copy-of select="Longitude" />
    <xsl:copy-of select="NationalDivisionPrimary" />
    <xsl:copy-of select="NationalDivisionSecondary" />
    <xsl:copy-of select="OldAddressCountryCode" />
    <xsl:copy-of select="PostalCode" />
    <xsl:copy-of select="SubAddressType" />
    <xsl:copy-of select="SubAdminDivisionPrimary" />
    <xsl:copy-of select="SubAdminDivisionSecondary" />
    <xsl:copy-of select="SubAdminDivisionTertiary" />
    <xsl:copy-of select="SubAttention" />
    <xsl:copy-of select="SubCountryCode" />
    <xsl:variable name="claimAddressSubEffectiveDate" select="SubEffectiveDate" />
    <xsl:choose>
      <xsl:when test="$claimAddressSubEffectiveDate != ''">
        <SubEffectiveDate>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($claimAddressSubEffectiveDate))" />
        </SubEffectiveDate>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="SubEffectiveDate"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:variable name="claimAddressSubExpirationDate" select="SubExpirationDate" />
    <xsl:choose>
      <xsl:when test="$claimAddressSubExpirationDate != ''">
        <SubExpirationDate>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($claimAddressSubExpirationDate))" />
        </SubExpirationDate>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="SubExpirationDate"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:copy-of select="SubLastTransactionID" />
    <xsl:variable name="claimAddressSubLastUpdatedDate" select="SubLastUpdatedDate" />
    <xsl:choose>
      <xsl:when test="$claimAddressSubLastUpdatedDate != ''">
        <SubLastUpdatedDate>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($claimAddressSubLastUpdatedDate))" />
        </SubLastUpdatedDate>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="SubLastUpdatedDate"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:copy-of select="SubLatitude" />
    <xsl:copy-of select="SubLocationDetailsLine1" />
    <xsl:copy-of select="SubLocationDetailsLine2" />
    <xsl:copy-of select="SubLocationDetailsLine3" />
    <xsl:copy-of select="SubLocationDetailsLine4" />
    <xsl:copy-of select="SubLongitude" />
    <xsl:copy-of select="SubNationalDivisionPrimary" />
    <xsl:copy-of select="SubNationalDivisionSecondary" />
    <xsl:copy-of select="SubPostalCode" />
  </xsl:template>

  <xsl:template name="claimGroupDetailTemplate">
    <ClaimGroupDetail>
      <xsl:copy-of select="ClaimGroupDetail/AggregateDeductable" />
      <xsl:copy-of select="ClaimGroupDetail/AggregateLimit" />
      <xsl:copy-of select="ClaimGroupDetail/CataStropheCodeStatus" />
      <CauseOfLosses>
        <xsl:for-each select="ClaimGroupDetail/CauseOfLosses">
          <CauseOfLoss>
            <xsl:value-of select="." />
          </CauseOfLoss>
        </xsl:for-each>
      </CauseOfLosses>
      <xsl:copy-of select="ClaimGroupDetail/ClaimGroupID" />
      <xsl:copy-of select="ClaimGroupDetail/Code" />
      <xsl:copy-of select="ClaimGroupDetail/CoinsuranceContract" />
      <xsl:copy-of select="ClaimGroupDetail/CoinsuranceExists" />
      <xsl:copy-of select="ClaimGroupDetail/CoinsuranceThreshold" />
      <xsl:copy-of select="ClaimGroupDetail/Country" />
      <xsl:copy-of select="ClaimGroupDetail/CurrencyUOM" />
      <xsl:copy-of select="ClaimGroupDetail/Description" />
      <xsl:variable name="claimGroupDetailEndDate" select="ClaimGroupDetail/EndDate" />
      <xsl:choose>
        <xsl:when test="$claimGroupDetailEndDate != ''">
          <EndDate>
            <xsl:value-of select="string(Util:ConvertDateToDuckFormat($claimGroupDetailEndDate))" />
          </EndDate>
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy-of select="ClaimGroupDetail/EndDate"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:copy-of select="ClaimGroupDetail/LastTransactionID" />
      <LineOfBusinesses>
        <xsl:for-each select="ClaimGroupDetail/LineOfBusinesses" >
          <LineOfBusiness>
            <xsl:value-of select="." />
          </LineOfBusiness>
        </xsl:for-each>
      </LineOfBusinesses>
      <xsl:copy-of select="ClaimGroupDetail/Name" />
      <xsl:copy-of select="ClaimGroupDetail/NameUpperCase" />
      <xsl:copy-of select="ClaimGroupDetail/PartyID" />
      <PolicyNumbers>
        <xsl:for-each select="ClaimGroupDetail/PolicyNumbers">
          <PolicyNumber>
            <xsl:value-of select="." />
          </PolicyNumber>
        </xsl:for-each>
      </PolicyNumbers>
      <xsl:copy-of select="ClaimGroupDetail/ReinsuranceContract" />
      <xsl:copy-of select="ClaimGroupDetail/ReinsuranceExists" />
      <xsl:copy-of select="ClaimGroupDetail/ReinsuranceThreshold" />
      <xsl:variable name="claimGroupDetailStartDate" select="ClaimGroupDetail/StartDate" />
      <xsl:choose>
        <xsl:when test="$claimGroupDetailStartDate != ''">
          <StartDate>
            <xsl:value-of select="string(Util:ConvertDateToDuckFormat($claimGroupDetailStartDate))" />
          </StartDate>
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy-of select="ClaimGroupDetail/StartDate"/>
        </xsl:otherwise>
      </xsl:choose>
      <States>
        <xsl:for-each select="ClaimGroupDetail/States">
          <State>
            <xsl:value-of select="." />
          </State>
        </xsl:for-each>
      </States>
      <xsl:copy-of select="ClaimGroupDetail/TotalPayment" />
      <xsl:copy-of select="ClaimGroupDetail/TotalReceipt" />
      <xsl:copy-of select="ClaimGroupDetail/TotalReserve" />
      <xsl:copy-of select="ClaimGroupDetail/Type" />
    </ClaimGroupDetail>
  </xsl:template>

  <xsl:template name="commonClaimsNameDetailTemplate">
    <xsl:copy-of select="CountryCode" />
    <xsl:copy-of select="FirstName" />
    <xsl:copy-of select="LastName" />
    <xsl:copy-of select="MiddleName" />
    <xsl:copy-of select="PrefixCode" />
    <xsl:copy-of select="SuffixCode" />
    <xsl:copy-of select="LastTransactionID" />
  </xsl:template>

  <xsl:template name="claimClaimAssociationTemplate" >
    <ClaimClaimAssociations>
      <xsl:for-each select="ClaimClaimAssociations/ClaimClaimAssociationDTO">
        <ClaimClaimAssociation>
          <xsl:copy-of select="ClaimID1" />
          <xsl:copy-of select="ClaimID2" />
          <xsl:copy-of select="LastTransactionID" />
        </ClaimClaimAssociation>
      </xsl:for-each>
    </ClaimClaimAssociations>
  </xsl:template>

  <xsl:template name="claimClaimGroupAssociationTemplate" >
    <ClaimClaimGroupAssociations>
      <xsl:for-each select="ClaimClaimGroupAssociations/ClaimClaimGroupAssociationDTO">
        <ClaimClaimGroupAssociation>
          <xsl:copy-of select="ClaimGroupID" />
          <xsl:copy-of select="ClaimID" />
          <xsl:copy-of select="LastTransactionID" />
        </ClaimClaimGroupAssociation>
      </xsl:for-each>
    </ClaimClaimGroupAssociations>
  </xsl:template>

  <xsl:template name="claimEventHistoryTemplate">
    <xsl:for-each select="ClaimEventsHistory/ClaimEventHistory" >
      <ClaimEventHistory>
        <ClaimHistory>
          <xsl:copy-of select="ClaimHistory/ClaimHistoryID" />
          <xsl:copy-of select="ClaimHistory/ClaimID"  />
          <xsl:variable name="claimHistoryCreateDate" select="ClaimHistory/CreateDate" />
          <xsl:choose>
            <xsl:when test="$claimHistoryCreateDate != ''">
              <CreateDate>
                <xsl:value-of select="string(Util:ConvertDateToDuckTimeFormat($claimHistoryCreateDate))" />
              </CreateDate>
            </xsl:when>
            <xsl:otherwise>
              <xsl:copy-of select="ClaimHistory/CreateDate"/>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:copy-of select="ClaimHistory/Description"  />
          <xsl:copy-of select="ClaimHistory/EventNumber"  />
          <xsl:copy-of select="ClaimHistory/LastTransactionID"  />
          <xsl:copy-of select="ClaimHistory/LineID"  />
          <xsl:copy-of select="ClaimHistory/ParticipantRoleID"  />
          <xsl:copy-of select="ClaimHistory/PerformerID"  />
        </ClaimHistory>
        <ClaimPerformerNameAssociation>
          <xsl:copy-of select="ClaimPerformerNameAssociation/BusinessName"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/BusinessNamePhonetic"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/BusinessNameSoundex"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/BusinessNameUC"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/CountryCode"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/FirstName"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/FirstNamePhonetic"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/FirstNameSoundex"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/FirstNameUC"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/FullName"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/IsGroup"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/IsPerformer"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/LastName"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/LastNamePhonetic"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/LastNameUC"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/MiddleName"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/MiddleNamePhonetic"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/MiddleNameSoundex"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/MiddleNameUC"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/NamePrefix"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/NameSuffix"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/PartyType"  />
          <xsl:copy-of select="ClaimPerformerNameAssociation/PerformerID"  />
        </ClaimPerformerNameAssociation>
      </ClaimEventHistory>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="commonPartyAddressTemplate">
    <xsl:for-each select="PartyAddressDetail">
      <PartyAddressesDetail>
        <xsl:for-each select="ClaimsPartyAddressDetailDTO">
          <PartyAddressDetail>
            <xsl:for-each select="PartyAddress">
              <PartyAddress>
                <xsl:copy-of select="AddressID" />
                <xsl:copy-of select="Comment" />
                <xsl:variable name="partyAddressEffectiveDate" select="EffectiveDate" />
                <xsl:choose>
                  <xsl:when test="$partyAddressEffectiveDate != ''">
                    <EffectiveDate>
                      <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyAddressEffectiveDate))" />
                    </EffectiveDate>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:copy-of select="EffectiveDate"/>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:variable name="partyAddressExpirationDate" select="ExpirationDate" />
                <xsl:choose>
                  <xsl:when test="$partyAddressExpirationDate != ''">
                    <ExpirationDate>
                      <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyAddressExpirationDate))" />
                    </ExpirationDate>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:copy-of select="ExpirationDate"/>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:copy-of select="ExternalPartyAddressID" />
                <xsl:copy-of select="IsDefault" />
                <xsl:copy-of select="IsPrimary" />
                <xsl:copy-of select="IsTaxAddress" />
                <xsl:copy-of select="LastTransactionID" />
                <xsl:variable name="partyAddressLastUpdatedDate" select="LastUpdatedDate" />
                <xsl:choose>
                  <xsl:when test="$partyAddressLastUpdatedDate != ''">
                    <LastUpdatedDate>
                      <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyAddressLastUpdatedDate))" />
                    </LastUpdatedDate>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:copy-of select="LastUpdatedDate"/>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:copy-of select="PartyAddressID" />
                <xsl:copy-of select="PartyID" />
                <xsl:copy-of select="SubComment" />
              </PartyAddress>
            </xsl:for-each>
            <xsl:for-each select="Address">
              <Address>
                <xsl:call-template name="commonClaimsAddressTemplate" />
              </Address>
            </xsl:for-each>
          </PartyAddressDetail>
        </xsl:for-each>
      </PartyAddressesDetail>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="commonPartyMembershipTemplate">
    <xsl:for-each select="PartyMembership">
      <PartyMemberships>
        <xsl:for-each select="ClaimsPartyMembershipDTO">
          <PartyMembership>
            <xsl:copy-of select="LastTransactionID" />
            <xsl:variable name="partyMembershipLastUpdatedDate" select="LastUpdatedDate" />
            <xsl:choose>
              <xsl:when test="$partyMembershipLastUpdatedDate != ''">
                <LastUpdatedDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyMembershipLastUpdatedDate))" />
                </LastUpdatedDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="LastUpdatedDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="MembershipID" />
            <xsl:copy-of select="MembershipTypeCode" />
            <xsl:copy-of select="PartyID" />
            <xsl:copy-of select="PartyMembershipID" />
          </PartyMembership>
        </xsl:for-each>
      </PartyMemberships>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="commonPartyRelationTemplate">
    <xsl:for-each select="PartyRelation">
      <PartyRelations>
        <xsl:for-each select="ClaimsPartyRelationDTO">
          <PartyRelation>
            <xsl:copy-of select="IsRelatedPartyLeader" />
            <xsl:copy-of select="LastTransactionID" />
            <xsl:variable name="partyRelationLastUpdatedDate" select="LastUpdateDate" />
            <xsl:choose>
              <xsl:when test="$partyRelationLastUpdatedDate != ''">
                <LastUpdatedDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyRelationLastUpdatedDate))" />
                </LastUpdatedDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="LastUpdateDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="PartyID" />
            <xsl:copy-of select="RelatedPartyID" />
            <xsl:copy-of select="RelationshipID" />
            <xsl:copy-of select="RelationshipTypeCode" />
          </PartyRelation>
        </xsl:for-each>
      </PartyRelations>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="commonPartyLicenseTemplate">
    <xsl:for-each select="PartyLicenses">
      <PartyLicenses>
        <xsl:for-each select="ClaimsPartyLicenseDTO">
          <PartyLicense>
            <xsl:variable name="partyLicenseEffectiveDate" select="EffectiveDate" />
            <xsl:choose>
              <xsl:when test="$partyLicenseEffectiveDate != ''">
                <EffectiveDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyLicenseEffectiveDate))" />
                </EffectiveDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="EffectiveDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="partyLicenseExpirationDate" select="ExpirationDate" />
            <xsl:choose>
              <xsl:when test="$partyLicenseExpirationDate != ''">
                <ExpirationDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyLicenseExpirationDate))" />
                </ExpirationDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="ExpirationDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="LastTransactionID" />
            <xsl:variable name="partyLicenseLastUpdatedDate" select="LastUpdatedDate" />
            <xsl:choose>
              <xsl:when test="$partyLicenseLastUpdatedDate != ''">
                <LastUpdatedDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyLicenseLastUpdatedDate))" />
                </LastUpdatedDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="LastUpdatedDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="partyLicenseExpDate" select="LicenseExpirationDate" />
            <xsl:choose>
              <xsl:when test="$partyLicenseExpDate != ''">
                <LicenseExpirationDate>
                  <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyLicenseExpDate))" />
                </LicenseExpirationDate>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="LicenseExpirationDate"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:copy-of select="LineOfBusiness" />
            <xsl:copy-of select="PartyID" />
            <xsl:copy-of select="PartyLicenseCountry" />
            <xsl:copy-of select="PartyLicenseID" />
            <xsl:copy-of select="PartyLicenseIssuedDate" />
            <xsl:copy-of select="PartyLicenseNumber" />
            <xsl:copy-of select="PartyLicensePlaceIssued" />
            <xsl:copy-of select="PartyLicenseStatus" />
            <xsl:copy-of select="PartyLicenseType" />
          </PartyLicense>
        </xsl:for-each>
      </PartyLicenses>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="commonPartyCommentTemplate">
    <xsl:for-each select="PartyComment">
      <PartyComment>
        <xsl:copy-of select="Comment" />
        <xsl:variable name="partyCommentEffectiveDate" select="EffectiveDate" />
        <xsl:choose>
          <xsl:when test="$partyCommentEffectiveDate != ''">
            <EffectiveDate>
              <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyCommentEffectiveDate))" />
            </EffectiveDate>
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy-of select="EffectiveDate"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:variable name="partyCommentExpirationDate" select="ExpirationDate" />
        <xsl:choose>
          <xsl:when test="$partyCommentExpirationDate != ''">
            <ExpirationDate>
              <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyCommentExpirationDate))" />
            </ExpirationDate>
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy-of select="ExpirationDate"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:copy-of select="LastTransactionID" />
        <xsl:variable name="partyCommentLastUpdatedDate" select="LastUpdatedDate" />
        <xsl:choose>
          <xsl:when test="$partyCommentLastUpdatedDate != ''">
            <LastUpdatedDate>
              <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyCommentLastUpdatedDate))" />
            </LastUpdatedDate>
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy-of select="LastUpdatedDate"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:copy-of select="PartyID" />
      </PartyComment>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="commonClaimantEmploymentTemplate">
    <xsl:copy-of select="AdjAvgWeeklyWage"/>
    <xsl:copy-of select="AdjAvgWeeklyWageUOM"/>
    <xsl:copy-of select="AvgWeeklyWageOvrd"/>
    <xsl:copy-of select="AvgWeeklyWageOvrdUOM"/>
    <xsl:copy-of select="BaseWageType"/>
    <xsl:copy-of select="ClaimantID"/>
    <xsl:copy-of select="ConcurrentEmployment"/>
    <xsl:copy-of select="DateFringeBenefitEnd"/>
    <xsl:variable name="claimantEmpDateHired" select="DateHired" />
    <xsl:choose>
      <xsl:when test="$claimantEmpDateHired != ''">
        <DateHired>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($claimantEmpDateHired))" />
        </DateHired>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="DateHired"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:variable name="claimantEmpDateInCurrJob" select="DateInCurrJob" />
    <xsl:choose>
      <xsl:when test="$claimantEmpDateInCurrJob != ''">
        <DateInCurrJob>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($claimantEmpDateInCurrJob))" />
        </DateInCurrJob>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="DateInCurrJob"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:variable name="claimantEmpDateLastWorked" select="DateLastWorked" />
    <xsl:choose>
      <xsl:when test="$claimantEmpDateLastWorked != ''">
        <DateLastWorked>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($claimantEmpDateLastWorked))" />
        </DateLastWorked>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="DateLastWorked"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:copy-of select="DaysWorkedWeekly"/>
    <xsl:copy-of select="DepartmentDesc"/>
    <xsl:copy-of select="DispatchState"/>
    <xsl:copy-of select="EmployeeType"/>
    <xsl:copy-of select="EmployerName"/>
    <xsl:copy-of select="EmployerRoleID"/>
    <xsl:copy-of select="EmployerType"/>
    <xsl:copy-of select="EmploymentID"/>
    <xsl:copy-of select="EmploymentStatus"/>
    <xsl:copy-of select="FringeBenefitAmt"/>
    <xsl:copy-of select="FringeBenefitsContinued"/>
    <xsl:copy-of select="FringeBenefitUOM"/>
    <xsl:copy-of select="HiredState"/>
    <xsl:copy-of select="HoursWorkedDaily"/>
    <xsl:copy-of select="HoursWorkedWeekly"/>
    <xsl:copy-of select="JobClass"/>
    <xsl:copy-of select="JobDescription"/>
    <xsl:copy-of select="JobTitle"/>
    <xsl:copy-of select="LastTransactionID"/>
    <xsl:variable name="claimantEmpLastUpdateDate" select="LastUpdateDate" />
    <xsl:choose>
      <xsl:when test="$claimantEmpLastUpdateDate != ''">
        <LastUpdateDate>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($claimantEmpLastUpdateDate))" />
        </LastUpdateDate>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="LastUpdateDate"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:copy-of select="MonthsInCurrJob"/>
    <xsl:copy-of select="MonthsInEmploy"/>
    <xsl:copy-of select="OccupationDesc"/>
    <xsl:copy-of select="PayFrequency"/>
    <xsl:copy-of select="RecordDeleteInd"/>
    <xsl:copy-of select="SupervisorName"/>
    <xsl:copy-of select="WageAmount"/>
    <xsl:copy-of select="WageUOM"/>
    <xsl:copy-of select="WorkEndTime"/>
    <xsl:copy-of select="WorkLocCounty"/>
    <xsl:copy-of select="WorkLocState"/>
    <xsl:copy-of select="WorkStartTime"/>
    <xsl:copy-of select="YearsInCurrJob"/>
    <xsl:copy-of select="YearsInEmploy"/>
  </xsl:template>

  <xsl:template name="commonParticipantNameDetailTemplate">
    <xsl:copy-of select="CountryCode" />
    <xsl:copy-of select="FirstName" />
    <xsl:copy-of select="LastName" />
    <xsl:copy-of select="MiddleName" />
    <xsl:copy-of select="PrefixCode" />
    <xsl:copy-of select="SuffixCode" />
  </xsl:template>

  <xsl:template name="commonParticipantRoleTemplate">
    <xsl:variable name="participantRoleActivationDate" select="ActivationDate" />
    <xsl:choose>
      <xsl:when test="$participantRoleActivationDate != ''">
        <ActivationDate>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($participantRoleActivationDate))" />
        </ActivationDate>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="ActivationDate" />
      </xsl:otherwise>
    </xsl:choose>
    <xsl:copy-of select="ClaimID" />
    <xsl:variable name="participantRoleDeactivationDate" select="DeactivationDate" />
    <xsl:choose>
      <xsl:when test="$participantRoleDeactivationDate != ''">
        <DeactivationDate>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($participantRoleDeactivationDate))" />
        </DeactivationDate>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="DeactivationDate" />
      </xsl:otherwise>
    </xsl:choose>
    <xsl:copy-of select="LastTransactionID" />
    <xsl:variable name="participantRoleLastUpdatedDate" select="LastUpdatedDate" />
    <xsl:choose>
      <xsl:when test="$participantRoleLastUpdatedDate != ''">
        <LastUpdatedDate>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($participantRoleLastUpdatedDate))" />
        </LastUpdatedDate>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="LastUpdatedDate" />
      </xsl:otherwise>
    </xsl:choose>
    <xsl:copy-of select="ParticipantID" />
    <xsl:copy-of select="ParticipantRole" />
    <xsl:copy-of select="ParticipantRoleID" />
    <xsl:copy-of select="RecordDeleteInd" />
  </xsl:template>

  <xsl:template name="commonPartyDetailsTemplate">
    <xsl:copy-of select="AchPartyExternalID" />
    <xsl:copy-of select="BusinessPermitted" />
    <xsl:copy-of select="CountryCode" />
    <xsl:copy-of select="ExternalID" />
    <xsl:copy-of select="GeographicFormatCode" />
    <xsl:copy-of select="IsInActive" />
    <xsl:copy-of select="LastTransactionID" />
    <xsl:variable name="partyDetailsLastUpdatedDate" select="LastUpdatedDate" />
    <xsl:choose>
      <xsl:when test="$partyDetailsLastUpdatedDate != ''">
        <LastUpdatedDate>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat($partyDetailsLastUpdatedDate))" />
        </LastUpdatedDate>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="LastUpdatedDate" />
      </xsl:otherwise>
    </xsl:choose>
    <xsl:copy-of select="Number" />
    <xsl:copy-of select="PartyID" />
    <xsl:copy-of select="PartyTypeCode" />
    <xsl:copy-of select="SkipLoadingExtendedData" />
  </xsl:template>

  <xsl:template name="commonItemTemplate">
    <xsl:copy-of select="ClaimID" />
    <xsl:copy-of select="CoveredIndicator" />
    <xsl:copy-of select="DamageDescription" />
    <xsl:copy-of select="ExternalItemID" />
    <xsl:copy-of select="InvolvedIndicator" />
    <xsl:copy-of select="ItemDetailedType" />
    <xsl:copy-of select="ItemGroupID" />
    <xsl:copy-of select="ItemID" />
    <xsl:copy-of select="ItemType" />
    <xsl:copy-of select="LastTransactionID" />
    <xsl:copy-of select="ReplacedIndicator" />
    <xsl:copy-of select="SubjectOfInsuranceIndicator" />
    <xsl:copy-of select="SubjectOfInsuranceManualIndicator" />
    <xsl:copy-of select="ThirdPartyIndicator" />
  </xsl:template>

  <xsl:template name="ResponseInfo">
    <xsl:param name="ReturnCode">
    </xsl:param>
    <xsl:param name="NotificationResponse"></xsl:param>
    <ResponseInfo>
      <ReturnCode>
        <xsl:value-of select="$ReturnCode"/>
      </ReturnCode>
      <xsl:choose>
        <xsl:when test="$NotificationResponse != ''">
          <NotificationResponse>
            <xsl:value-of select="$NotificationResponse"/>
          </NotificationResponse>
        </xsl:when>
        <xsl:otherwise>
          <NotificationResponse />
        </xsl:otherwise>
      </xsl:choose>
    </ResponseInfo>
  </xsl:template>
</xsl:stylesheet>
