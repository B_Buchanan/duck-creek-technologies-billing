﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Util="urn:Util" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs msxsl xsl Util">
  <xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>
  <xsl:template match="server/responses">
    <Policies>
      <ReturnCount>
        <xsl:value-of select="PartyInvolvement.getPolicyInvolvementsSummaryRs/PartyInvolvements/Paging/ReturnCount"/>
      </ReturnCount>
      <TotalCount>
        <xsl:value-of select="PartyInvolvement.getPolicyInvolvementsSummaryRs/PartyInvolvements/Paging/TotalCount"/>
      </TotalCount>
      <xsl:for-each select="PartyInvolvement.getPolicyInvolvementsSummaryRs/PartyInvolvements/PartyInvolvement">
        <Policy xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
          <xsl:attribute name="PolicyId">
            <xsl:value-of select="PolicyId"/>
          </xsl:attribute>
          <xsl:variable name="varPolicyId" select="PolicyId" />
          <xsl:variable name="varPolicyNumber" select="ObjectReference" />
          <xsl:call-template name="policyinvolvementsandSummary">
          </xsl:call-template>
          <!--<xsl:for-each select="../../../OnlineData.getPolicyXMLRs/session">
            <xsl:if test ="@id=$varPolicyId" >-->
              <PolicyDetails xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <!--<xsl:call-template name="policySummary" />-->

                  <xsl:for-each select="../../../TransACT.getPolicySummaryRs">
                    <xsl:if test ="PolicySummary/Field[@name='PolicyNumber']//@value=$varPolicyNumber or PolicySummary/Field[@name='QuoteNumber']//@value=$varPolicyNumber" >
                        <PolicyNumber>
                          <xsl:value-of select="PolicySummary/Field[@name='PolicyNumber']/@value"/>
                        </PolicyNumber>
                        <QuoteNumber>
                          <xsl:value-of select="PolicySummary/Field[@name='QuoteNumber']/@value"/>
                        </QuoteNumber>
                        <Status>
                          <xsl:value-of select="PolicySummary/Field[@name='PolicyStatus']/@value"/>
                        </Status>
                        <PolicyEffectiveDate>
                          <xsl:value-of select="PolicySummary/Field[@name='EffectiveDate']/@value"/>
                        </PolicyEffectiveDate>
                        <PolicyExpirationDate>
                          <xsl:value-of select="PolicySummary/Field[@name='ExpirationDate']/@value"/>
                        </PolicyExpirationDate>
                        <InceptionDate>
                          <xsl:value-of select="PolicySummary/Field[@name='InceptionDate']/@value"/>
                        </InceptionDate>
                        <FullTermPremium>
                          <xsl:value-of select="PolicySummary/Field[@name='FullTermPremium']/@value"/>
                        </FullTermPremium>
                        <PremiumWritten>
                          <xsl:value-of select="PolicySummary/Field[@name='WrittenPremium']/@value"/>
                        </PremiumWritten>
                    
                  
                <Transactions>
                  <!--<xsl:call-template name="policyTransactions" />-->

                  <xsl:for-each select="following-sibling::TransACT.listTransactionsRs[1]">

                    <xsl:for-each select="transactions/transaction">
                      
                      <xsl:if test="(State='open' or State='committed' or State='closed') and (DeprecatedBy='' or not(DeprecatedBy) or ((Type='Report' or Type='Audit') and (DeprecatedBy!='') and (DeprecatedBy =ReplacementID)))">
                        <Transaction xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                      <xsl:call-template name="policyTransactionsNew" />
                    </Transaction>
                      </xsl:if>
                    </xsl:for-each>
                  </xsl:for-each>

                </Transactions>
                    </xsl:if>
                  </xsl:for-each>
                <!--<Participants>
                  <xsl:call-template name="policyParticipants" />
                </Participants>
                <xsl:call-template name="policyActivities" />-->
              </PolicyDetails>
            <!--</xsl:if>
          </xsl:for-each>-->
        </Policy>
      </xsl:for-each>
    </Policies>
    <BillingAccounts>
      <ReturnCount>
        <xsl:value-of select="PartyInvolvement.getBillingInvolvementsSummaryRs/PartyInvolvements/Paging/ReturnCount"/>
      </ReturnCount>
      <TotalCount>
        <xsl:value-of select="PartyInvolvement.getBillingInvolvementsSummaryRs/PartyInvolvements/Paging/TotalCount"/>
      </TotalCount>
      <xsl:for-each select="PartyInvolvement.getBillingInvolvementsSummaryRs/PartyInvolvements/PartyInvolvement">
        <BillingAccount xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
          <xsl:attribute name="AccountId">
            <xsl:value-of select="Account/AccountId"/>
          </xsl:attribute>

          <xsl:variable name="varAccountId" select="Account/AccountId" />

          <xsl:call-template name="billinginvolvementsandSummary" />

          <xsl:for-each select="../../../BillingObj.getAccountSummaryRs/Account">
            <xsl:if test ="AccountId=$varAccountId" >

              <BillingAccountDetails xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <xsl:call-template name="accountSummary" />
                <PolicyTerms>
                  <xsl:call-template name="accountPolicyTerms" />
                </PolicyTerms>
                <Participants>
                  <xsl:call-template name="accountParticipants" />
                </Participants>
              </BillingAccountDetails>

              <xsl:for-each select="../following-sibling::BillingCashManagement.paymentSearchRs[1]">

                <Payments xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                  <xsl:call-template name="accountPayments" />
                </Payments>

              </xsl:for-each>

              <xsl:for-each select="../following-sibling::BillingObj.searchActivityLogRs[1]">
                <BillingAccountActivities xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                  <xsl:call-template name="accountActivity" />
                </BillingAccountActivities>
              </xsl:for-each>

            </xsl:if>


          </xsl:for-each>

        </BillingAccount>
      </xsl:for-each>
    </BillingAccounts>
    <Claims>
      <xsl:for-each select="ClaimsComponents.getClaimInvolvementsRs/ClaimsInvolvements/ClaimsInvolvement">
        <Claim xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
          <xsl:attribute name="ClaimID">
            <xsl:value-of select="ClaimID"/>
          </xsl:attribute>
          <xsl:variable name="varClaimid" select="ClaimID" />
          <xsl:call-template name="ClaimInvolvementsandSummary" />
          <xsl:if test="ClaimSensitivity !='03'">
          <xsl:for-each select="../../../ClaimsComponents.getClaimDetailsRs">
            <!--<xsl:if test ="ClaimSummaryAndEventHistory/ClaimSummary/ClaimDetails/Claim/ClaimID=$varClaimid" >-->
              <xsl:if test ="ClaimSummaryAndEventHistory/Claim/ClaimID=$varClaimid" >
              <ClaimDetails>
                <xsl:call-template name="ClaimDetails" />
              </ClaimDetails>
            </xsl:if>
          </xsl:for-each>
          </xsl:if>
        </Claim>
      </xsl:for-each>
    </Claims>
  </xsl:template>


  <!--Policies Templates-->
  <xsl:template name="policyinvolvementsandSummary" >
    <ObjectId>
      <xsl:value-of select="ObjectId"/>
    </ObjectId>
    <PartyId>
      <xsl:value-of select="PartyId"/>
    </PartyId>
    <xsl:choose>
      <xsl:when test ="Policy/Status='Quote' or Policy/Status='Application'">
        <QuoteNumber>
          <xsl:value-of select="Policy/PolicyNumber"/>
        </QuoteNumber>
      </xsl:when>
      <xsl:otherwise>
        <PolicyNumber>
          <xsl:value-of select="Policy/PolicyNumber"/>
        </PolicyNumber>
      </xsl:otherwise>
    </xsl:choose>
    <LineOfBusiness>
      <xsl:value-of select="Policy/LOB"/>
    </LineOfBusiness>
    <Status>
      <xsl:value-of select="Policy/Status"/>
    </Status>
    <PolicyEffectiveDate>
      <xsl:value-of select="Policy/EffectiveDate"/>
    </PolicyEffectiveDate>
    <PolicyExpirationDate>
      <xsl:value-of select="Policy/ExpirationDate"/>
    </PolicyExpirationDate>
    <InceptionDate>
      <xsl:value-of select="Policy/InceptionDate"/>
    </InceptionDate>
    <xsl:call-template name="partydetailRoles" />
    <!--<Premium>
      <xsl:value-of select="Policy/CurrentPremium"/>
    </Premium>
    <PremiumWritten>
      <xsl:value-of select="Policy/WrittenPremium"/>
    </PremiumWritten>-->
  </xsl:template>

  <!--Roles Templates-->
  <xsl:template name="partydetailRoles" >
    <RoleCodes>
      <xsl:variable name="varParticipantId" select="PartyId" />
      <xsl:for-each select="RoleCodes">
        <xsl:for-each select="RoleCode">
          <Role>
            <RoleCode>
              <xsl:value-of select="current()"/>
            </RoleCode>
          </Role>

        </xsl:for-each>
      </xsl:for-each>
      <xsl:if test="position()!=last()">
        <xsl:text>,</xsl:text>
      </xsl:if>
    </RoleCodes>
  </xsl:template>

  <xsl:template name="claimsdetailRoles" >
    <RoleCodes>
      <!--<xsl:variable name="varParticipantId" select="PartyId" />-->
      <xsl:for-each select="Roles">
        <xsl:for-each select="RoleCode">
          <Role>
            <RoleCode>
              <xsl:value-of select="current()"/>
            </RoleCode>
          </Role>

        </xsl:for-each>
      </xsl:for-each>
      <xsl:if test="position()!=last()">
        <xsl:text>,</xsl:text>
      </xsl:if>
    </RoleCodes>
  </xsl:template>

  <xsl:template name="RoleCodes" >
    <xsl:for-each select="RoleCodes/RoleCode">
      <RoleCode>
        <xsl:value-of select="current()"/>
      </RoleCode>
    </xsl:for-each>
  </xsl:template>
  <xsl:template name="policySummary" >
    <PolicyNumber>
      <xsl:value-of select="data/policy/PolicyNumber"/>
    </PolicyNumber>
    <QuoteNumber>
      <xsl:value-of select="data/policy/QuoteNumber"/>
    </QuoteNumber>
    <LineOfBusinessCode>
      <xsl:value-of select="data/policy/LineOfBusinessCode"/>
    </LineOfBusinessCode>
    <Status>
      <xsl:value-of select="data/policy/Status"/>
    </Status>
    <PolicyEffectiveDate>
      <xsl:value-of select="data/policy/EffectiveDate"/>
    </PolicyEffectiveDate>
    <PolicyExpirationDate>
      <xsl:value-of select="data/policy/ExpirationDate"/>
    </PolicyExpirationDate>
    <InceptionDate>
      <xsl:value-of select="data/policy/InceptionDate"/>
    </InceptionDate>

    <xsl:choose>
      <xsl:when test ="data/policy/Status='Quote' or data/policy/Status='Application'">
        <FullTermPremium>
        </FullTermPremium>
      </xsl:when>
      <xsl:otherwise>
        <FullTermPremium>
          <xsl:value-of select="data/policy/Premium"/>
        </FullTermPremium>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:choose>
      <xsl:when test ="data/policy/Status='Quote' or data/policy/Status='Application'">
        <PremiumWritten>
          <xsl:value-of select="data/policy/PremiumWritten"/>
        </PremiumWritten>
      </xsl:when>
      <xsl:otherwise>
        <PremiumWritten>
          <xsl:value-of select="data/policy/PremiumWritten"/>
        </PremiumWritten>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>
  <xsl:template name="policyTransactions" >
    <xsl:for-each select="data/policyAdmin/transactions/transaction">
      <Transaction>
        <TypeCaption>
          <xsl:value-of select="TypeCaption"/>
        </TypeCaption>
        <Type>
          <xsl:value-of select="Type"/>
        </Type>
        <EffectiveDate>
          <xsl:value-of select="EffectiveDate"/>
        </EffectiveDate>
        <NewPremium>
          <xsl:value-of select="NewPremium"/>
        </NewPremium>
        <Charge>
          <xsl:value-of select="Charge"/>
        </Charge>
      </Transaction>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="policyTransactionsNew" >
        <TypeCaption>
          <xsl:value-of select="TypeCaption"/>
        </TypeCaption>
        <Type>
          <xsl:value-of select="Type"/>
        </Type>
        <EffectiveDate>
          <xsl:value-of select="EffectiveDate"/>
        </EffectiveDate>
        <NewPremium>
          <xsl:value-of select="NewPremium"/>
        </NewPremium>
        <Charge>
          <xsl:value-of select="Charge"/>
        </Charge>
  </xsl:template>
  
  <xsl:template name="policyParticipants" >
    <xsl:for-each select="data/account/additionalOtherInterest">
      <Participant>
        <PartyId>
          <xsl:value-of select="@DatabaseId"/>
        </PartyId>
        <PartyFullName>
          <xsl:value-of select="CorrespondenceName"/>
        </PartyFullName>
        <PartyRoleCode>
          <xsl:value-of select="Type"/>
        </PartyRoleCode>
      </Participant>
    </xsl:for-each>
  </xsl:template>
  <xsl:template name="policyActivities"></xsl:template>

  <xsl:template name="policyParticipantsnew" >
    <xsl:variable name="varPolicyId" select="PolicyId" />
    <xsl:for-each select="../../CustomServer.processRs">
      <xsl:for-each select="server/responses/PartyInvolvement.getParticipantsRs/Participants/Participant">
        <xsl:if test ="ObjectId=$varPolicyId" >
          <xsl:variable name="varParticipantId" select="ParticipantId" />
          <Participant>
            <PartyId>
              <xsl:value-of select="ParticipantId"/>
            </PartyId>
            <xsl:for-each select="../../PartyObj.getPartyRs/PartyRecord">
              <xsl:if test ="PartyRecordPartyId=$varParticipantId" >
                <PartyFullName>
                  <xsl:value-of select="PartyName"/>
                </PartyFullName>
              </xsl:if>
            </xsl:for-each>
            <PartyRoleCode>
              <xsl:value-of select="Type"/>
            </PartyRoleCode>
          </Participant>
        </xsl:if>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>

  <!--Billing Templates-->
  <xsl:template name="billinginvolvementsandSummary" >
    <AccountId>
      <xsl:value-of select="Account/AccountId"/>
    </AccountId>
    <AccountReference>
      <xsl:value-of select="Account/AccountReference"/>
    </AccountReference>
    <AccountStatus>
      <xsl:value-of select="Account/AccountStatus"/>
    </AccountStatus>
    <ObjectId>
      <xsl:value-of select="ObjectId"/>
    </ObjectId>
    <PartyId>
      <xsl:value-of select="PartyId"/>
    </PartyId>
    <InvoiceDate>
      <xsl:value-of select="Account/Invoices/Invoice/InvoiceDate"/>
    </InvoiceDate>
    <InvoiceAmount>
      <xsl:value-of select="Account/Invoices/Invoice/InvoiceTotalAmount"/>
    </InvoiceAmount>
    <DueDate>
      <xsl:value-of select="Account/Invoices/Invoice/DueDate"/>
    </DueDate>
    <xsl:call-template name="partydetailRoles" />
    <!-- <RoleCodes>
      <xsl:call-template name="RoleCodes" />
    </RoleCodes> -->
  </xsl:template>
  <xsl:template name="accountSummary" >
    <AccountId>
      <xsl:value-of select="AccountId"/>
    </AccountId>
    <AccountReference>
      <xsl:value-of select="AccountReference"/>
    </AccountReference>
    <AccountStatus>
      <xsl:value-of select="AccountStatus"/>
    </AccountStatus>
    <CurrentInvoice>
      <xsl:call-template name="CurrentInvoiceDetails" />
    </CurrentInvoice>
    <NextScheduledInvoice>
      <xsl:call-template name="NextScheduledInvoice" />
    </NextScheduledInvoice>
  </xsl:template>
  <xsl:template name="CurrentInvoiceDetails" >
    <InvoiceId>
      <xsl:value-of select="CurrentInvoice/ObjectId"/>
    </InvoiceId>
    <InvoiceDate>
      <xsl:value-of select="CurrentInvoice/InvoiceDate"/>
    </InvoiceDate>
    <DueDate>
      <xsl:value-of select="CurrentInvoice/DueDate"/>
    </DueDate>
    <AccountId>
      <xsl:value-of select="CurrentInvoice/AccountId"/>
    </AccountId>
    <CurrentDueAmount>
      <xsl:value-of select="CurrentInvoice/CurrentDueAmount"/>
    </CurrentDueAmount>
    <InvoiceTotalAmount>
      <xsl:value-of select="CurrentInvoice/InvoiceTotalAmount"/>
    </InvoiceTotalAmount>
  </xsl:template>
  <xsl:template name="NextScheduledInvoice" >
    <InvoiceDate>
      <xsl:value-of select="NextScheduledInvoice/InvoiceDate"/>
    </InvoiceDate>
    <Amount>
      <xsl:value-of select="NextScheduledInvoice/Amount"/>
    </Amount>
    <InvoiceDueDate>
      <xsl:value-of select="NextScheduledInvoice/InvoiceDueDate"/>
    </InvoiceDueDate>
  </xsl:template>
  <xsl:template name="accountPayments" >
    <TotalCount>
      <xsl:value-of select="PaymentList/SearchControl/Paging/TotalCount"/>
    </TotalCount>
    <xsl:for-each select="PaymentList/PaymentItem">
      <Payment>
        <PaymentId>
          <xsl:value-of select="ItemData/PaymentId"/>
        </PaymentId>
        <PaymentReceivedDate>
          <xsl:value-of select="ItemData/DateReceived"/>
        </PaymentReceivedDate>
        <Amount>
          <xsl:value-of select="ItemData/Amount"/>
        </Amount>
      </Payment>
    </xsl:for-each>
  </xsl:template>
  <xsl:template name="accountPolicyTerms" >
    <xsl:for-each select="PolicyTerms/PolicyTerm">
      <PolicyTerm>
        <PolicyTermId>
          <xsl:value-of select="PolicyTermId"/>
        </PolicyTermId>
        <PolicyReference>
          <xsl:value-of select="PolicyReference"/>
        </PolicyReference>
        <PolicyTermEffectiveDate>
          <xsl:value-of select="PolicyTermEffectiveDate"/>
        </PolicyTermEffectiveDate>
        <PolicyTermExpirationDate>
          <xsl:value-of select="PolicyTermExpirationDate"/>
        </PolicyTermExpirationDate>
        <PolicyTermStatus>
          <xsl:value-of select="PolicyTermStatus"/>
        </PolicyTermStatus>
        <CurrentDueAmount>
          <xsl:value-of select="CurrentDueAmount"/>
        </CurrentDueAmount>
        <PastDueAmount>
          <xsl:value-of select="PastDueAmount"/>
        </PastDueAmount>
        <OutstandingBalance>
          <xsl:value-of select="OutstandingBalance"/>
        </OutstandingBalance>
        <PASPolicyId>
          <xsl:value-of select="PASPolicyId"/>
        </PASPolicyId>
      </PolicyTerm>
    </xsl:for-each>
  </xsl:template>
  <xsl:template name="accountParticipants" >
    <xsl:for-each select="AssociatedParties/Party">
      <Participant>
        <PartyId>
          <xsl:value-of select="@PartyId"/>
        </PartyId>
        <PartyFullName>
          <xsl:value-of select="CorrespondenceName"/>
        </PartyFullName>
        <PartyRoleCode>
          <xsl:value-of select="@Role"/>
        </PartyRoleCode>
      </Participant>
    </xsl:for-each>
  </xsl:template>

  <!--Billing Activities-->
  <xsl:template name="accountActivity" >
    <TotalCount>
      <xsl:value-of select="CandidateList/SearchControl/Paging/TotalCount"/>
    </TotalCount>
    <xsl:for-each select="CandidateList/ListItem">
      <BillingAccountActivity>
        <ActivityId>
          <xsl:value-of select="ItemData/ActivityId"/>
        </ActivityId>
        <PolicyTermId>
          <xsl:value-of select="ItemData/PolicyTermId"/>
        </PolicyTermId>
        <ActivityType>
          <xsl:value-of select="ItemData/ActivityType"/>
        </ActivityType>
        <ActivityAmount>
          <xsl:value-of select="ItemData/ActivityAmount"/>
        </ActivityAmount>
        <ActivityUserFullName>
          <xsl:value-of select="ItemData/ActivityUserFullName"/>
        </ActivityUserFullName>
      </BillingAccountActivity>
    </xsl:for-each>
  </xsl:template>

  <!--Claim Involvements Templates-->
  <xsl:template name="ClaimInvolvementsandSummary">
    <ClaimId>
      <xsl:value-of select="ClaimID"/>
    </ClaimId>
    <ClaimNumber>
      <xsl:value-of select="ClaimsNumber"/>
    </ClaimNumber>
    <LOB>
      <xsl:value-of select="LOB"/>
    </LOB>
    <ClaimType>
      <xsl:value-of select="LOBCategory"/>
    </ClaimType>
    <LossDate>
      <xsl:value-of select="LossDate"/>
    </LossDate>
    <ClaimSensitivity>
      <xsl:value-of select="ClaimSensitivity"/>
    </ClaimSensitivity>
    <ClaimStatus>
      <xsl:value-of select="ClaimStatus"/>
    </ClaimStatus>
    <xsl:call-template name="claimsdetailRoles" />
    <!--<RoleCodes>
      <xsl:call-template name="ClaimRoleCodes" />
    </RoleCodes>-->
    <NavigationLink>
      <xsl:value-of select="NavigationLink"/>
    </NavigationLink>
  </xsl:template>

  <xsl:template name="ClaimRoleCodes" >
    <xsl:for-each select="Roles/RoleCode">
      <RoleCode>
        <xsl:value-of select="current()"/>
      </RoleCode>
    </xsl:for-each>
  </xsl:template>

  <!--Claim Details Templates-->
  <xsl:template name="ClaimDetails">
    <xsl:call-template name="ClaimSummary" />
    <CoveredItems>
      <xsl:call-template name="CoveredItems" />
    </CoveredItems>
    <Participants>
      <xsl:call-template name="ClaimParticipants" />
    </Participants>
    <ClaimActivities>
      <xsl:call-template name="ClaimActivities" />
    </ClaimActivities>
  </xsl:template>

  <xsl:template name="ClaimSummary">
    <ClaimId>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Claim/ClaimID"/>
    </ClaimId>
    <ClaimNumber>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Claim/ClaimNumber"/>
    </ClaimNumber>
    <ClaimStatus>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Claim/ClaimStatus"/>
    </ClaimStatus>
    <LineOfBusiness>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Claim/LineOfBusiness"/>
    </LineOfBusiness>
    <PolicyNumber>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Policy/PolicyNumber"/>
    </PolicyNumber>
    <LossDate>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Loss/LossDate"/>
    </LossDate>
    <PolicyID>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Policy/PolicyID"/>
    </PolicyID>
    <WritingCompany>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Claim/HandlingCompany"/>
    </WritingCompany>
    <PolicyNumber>
      <xsl:value-of select="ClaimSummaryAndEventHistory/Policy/PolicyNumber"/>
    </PolicyNumber>
  </xsl:template>

  <xsl:template name="CoveredItems">
    <CoveredPersons>
      <xsl:for-each select="ClaimSummaryAndEventHistory/CoveredPersons/PersonItem">
        <CoveredPerson>
          <ClaimID>
            <xsl:value-of select="Item/ClaimID"/>
          </ClaimID>
          <ItemID>
            <xsl:value-of select="Item/ItemID"/>
          </ItemID>
          <ItemType>
            <xsl:value-of select="Item/ItemType"/>
          </ItemType>
          <LossDescription>
            <xsl:value-of select="Item/DamageDescription"/>
          </LossDescription>
          <Passengers>
            <xsl:value-of select="ItemPerson/ParticipantID"/>
          </Passengers>
        </CoveredPerson>
      </xsl:for-each>
    </CoveredPersons>
    <CoveredVehicles>
      <xsl:for-each select="ClaimSummaryAndEventHistory/CoveredVehicles/VehicleItem">
        <CoveredVehicle>
          <ClaimID>
            <xsl:value-of select="Item/ClaimID"/>
          </ClaimID>
          <ItemID>
            <xsl:value-of select="Item/ItemID"/>
          </ItemID>
          <ItemType>
            <xsl:value-of select="Item/ItemType"/>
          </ItemType>
          <LossDescription>
            <xsl:value-of select="Item/DamageDescription"/>
          </LossDescription>
          <Passengers>
            <xsl:value-of select="ItemVehicleDetail/Passengers"/>
          </Passengers>
          <VehicleIdentificationNumber>
            <xsl:value-of select="ItemVehicleDetail/VehicleIdentificationNumber"/>
          </VehicleIdentificationNumber>
          <VehicleDescription>
            <xsl:value-of select="ItemVehicleDetail/VehicleDescription"/>
          </VehicleDescription>
        </CoveredVehicle>
      </xsl:for-each>
    </CoveredVehicles>
    <CoveredProperties>
      <xsl:for-each select="ClaimSummaryAndEventHistory/CoveredProperties/PropertyItem">
        <CoveredProperty>
          <ClaimID>
            <xsl:value-of select="Item/ClaimID"/>
          </ClaimID>
          <ItemID>
            <xsl:value-of select="Item/ItemID"/>
          </ItemID>
          <ItemType>
            <xsl:value-of select="Item/ItemType"/>
          </ItemType>
          <LossDescription>
            <xsl:value-of select="Item/DamageDescription"/>
          </LossDescription>
          <FullAddress>
            <xsl:value-of select="ItemSite/SiteDetailDescription"/>
          </FullAddress>
        </CoveredProperty>
      </xsl:for-each>
    </CoveredProperties>
    <WorkersCompensationLoss>
      <LossID>
        <xsl:value-of select="ClaimSummaryAndEventHistory/WorkersCompensationLoss/LossID"/>
      </LossID>
      <LossType>
        <xsl:value-of select="ClaimSummaryAndEventHistory/WorkersCompensationLoss/WorkersCompensationLossType"/>
      </LossType>
      <LossDescription>
        <xsl:value-of select="ClaimSummaryAndEventHistory/WorkersCompensationLoss/DamageDescription"/>
      </LossDescription>
      <MachinePartName>
        <xsl:value-of select="ClaimSummaryAndEventHistory/WorkersCompensationLoss/MachinePartName"/>
      </MachinePartName>
    </WorkersCompensationLoss>
  </xsl:template>

  <xsl:template name="ClaimParticipants">
    <xsl:for-each select="ClaimSummaryAndEventHistory/Participants/Participant">
      <Participant>
        <PartyId>
          <xsl:value-of select="ParticipantID"/>
        </PartyId>
        <PartyFullName>
          <xsl:value-of select="ParticipantFullName"/>
        </PartyFullName>
        <PartyRoleCode>
          <xsl:for-each select="ParticipantRoles/ParticipantRole">
            <xsl:call-template name="ClaimParticipantRoles" />
          </xsl:for-each>
        </PartyRoleCode>
      </Participant>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="ClaimActivities">
    <xsl:for-each select="ClaimSummaryAndEventHistory/ClaimEventsHistory/ClaimEventHistory">
      <ClaimActivity>
        <ClaimID>
          <xsl:value-of select="ClaimHistory/ClaimID"/>
        </ClaimID>
        <ActivityType>
          <xsl:value-of select="ClaimHistory/Description"/>
        </ActivityType>
        <Details>
          <xsl:value-of select="ClaimHistory/Description"/>
        </Details>
        <Performer>
          <xsl:value-of select="ClaimHistory/PerformerID"/>
        </Performer>
        <Date>
          <xsl:value-of select="ClaimHistory/CreateDate"/>
        </Date>
      </ClaimActivity>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="ClaimParticipantRoles">
    <RoleCode>
      <xsl:value-of select="ParticipantRole"/>
    </RoleCode>
  </xsl:template>
</xsl:stylesheet>