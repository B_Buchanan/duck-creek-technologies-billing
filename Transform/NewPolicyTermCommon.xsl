﻿<?xml version="1.0"?>
<!-- Used by Renew, Rewrite, and Reissue -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes"/>


	<xsl:template match="policy">
		<xsl:param name="previousPolicyNumber"/>
		<xsl:param name="transactionTypeCode"/>
		<PartySetup>
			<LookupAgent name="AgentParty">
				<AgencyId>
					<xsl:value-of select="../agencyDetails/agency/@agencyID"/>
				</AgencyId>
			</LookupAgent>
			<LookupRoleByReference name="InsuredParty">
				<PartyReferenceRecord>
					<RoleInPolicy>I</RoleInPolicy>
					<PolicyEffectiveDate>
						<xsl:value-of select="../policyAdmin/transactions/transaction[Type = 'New' or Type = 'Renew' or Type = 'Rewrite' or Type = 'Reissue'][position()=last()-1]/EffectiveDate"/>
					</PolicyEffectiveDate>
					<PolicyExpirationDate>
						<xsl:value-of select="../policyAdmin/transactions/transaction[Type = 'New' or Type = 'Renew' or Type = 'Rewrite' or Type = 'Reissue' or Type = 'Endorse'][position()=last()-1]/ExpirationDate"/>
					</PolicyExpirationDate>
					<PolicyReference>
						<xsl:value-of select="$previousPolicyNumber"/>
					</PolicyReference>
					<PolicyIssueSystemCode>
						<xsl:value-of select="IssueSystem"/>
					</PolicyIssueSystemCode>
					<PolicyMasterCompanyCode>
						<xsl:value-of select="MasterCompany"/>
					</PolicyMasterCompanyCode>
					<PolicyLineOfBusinessCode>
						<xsl:value-of select="LineOfBusinessCode"/>
					</PolicyLineOfBusinessCode>
					<PolicyProductCode>
						<xsl:value-of select="ProductCode"/>
					</PolicyProductCode>
				</PartyReferenceRecord>
			</LookupRoleByReference>
		</PartySetup>
		<!-- The LookupConfig can be used as the equivelant of BillingKeys or the 
			LookupConfig in OneShotBillingSetup.xsl in the event that a completely new 
			config is desired for a new policy term.  This is not used out of the box. -->
		<PolicyTermSetup>
			<LookupConfig>
			<!--
				<BillClass>
				   <xsl:value-of select="/root/_BillingInputDoc/BillingKeys/BillClass"/>
				</BillClass>
				-->
				<AccountType/>
				<LineOfBusiness/>
				<ProcessingBranch/>
				<RiskState/>
				<Frequency/>
				<DepositPercent/>
				<DepositTransaction/>
				<Installments/>
				<InstallmentPlanDescription/>
			</LookupConfig>
		</PolicyTermSetup>
		<PolicyTermRecord>
			<PolicyTermEffectiveDate>
				<xsl:value-of select="EffectiveDate"/>
			</PolicyTermEffectiveDate>
			<PolicyTermExpirationDate>
				<xsl:value-of select="ExpirationDate"/>
			</PolicyTermExpirationDate>
			<PolicyReference>
				<xsl:value-of select="PolicyNumber"/>
			</PolicyReference>
			<PolicyIssueSystemCode>
				<xsl:value-of select="IssueSystem"/>
			</PolicyIssueSystemCode>
			<PolicyMasterCompanyCode>
				<xsl:value-of select="MasterCompany"/>
			</PolicyMasterCompanyCode>
			<PolicyLineOfBusinessCode>
				<xsl:value-of select="LineOfBusinessCode"/>
			</PolicyLineOfBusinessCode>
			<PolicyProductCode>
				<xsl:value-of select="ProductCode"/>
			</PolicyProductCode>
			<PolicyStateCode>
				<xsl:value-of select="PrimaryRatingState"/>
			</PolicyStateCode>
			<ProcessingOrgUnitCode>
				<xsl:value-of select="PolicyProcessingOrgUnitCode"/>
			</ProcessingOrgUnitCode>
			<TransactionTypeCode>
				<xsl:value-of select="$transactionTypeCode"/>
			</TransactionTypeCode>
		</PolicyTermRecord>
		<OldPolicyTermRecord>
			<PolicyTermEffectiveDate>
				<xsl:value-of select="../policyAdmin/transactions/transaction[Type = 'New' or Type = 'Renew' or Type = 'Rewrite' or Type = 'Reissue'][position()=last()-1]/ExpirationDate"/>
			</PolicyTermEffectiveDate>
			<PolicyTermExpirationDate>
				<xsl:value-of select="../policyAdmin/transactions/transaction[Type = 'New' or Type = 'Renew' or Type = 'Rewrite' or Type = 'Reissue' or Type = 'Endorse'][position()=last()-1]/ExpirationDate"/>
			</PolicyTermExpirationDate>
			<PolicyReference>
				<xsl:value-of select="$previousPolicyNumber"/>
			</PolicyReference>
			<PolicyIssueSystemCode>
				<xsl:value-of select="IssueSystem"/>
			</PolicyIssueSystemCode>
			<PolicyMasterCompanyCode>
				<xsl:value-of select="MasterCompany"/>
			</PolicyMasterCompanyCode>
			<PolicyLineOfBusinessCode>
				<xsl:value-of select="LineOfBusinessCode"/>
			</PolicyLineOfBusinessCode>
			<PolicyProductCode>
				<xsl:value-of select="ProductCode"/>
			</PolicyProductCode>
		</OldPolicyTermRecord>
	</xsl:template>

</xsl:stylesheet>