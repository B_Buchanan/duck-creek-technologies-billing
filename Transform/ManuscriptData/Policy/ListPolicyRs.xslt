﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Util="urn:Util" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs msxsl xsl Util">
  <xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>

  <xsl:template match="/">

    <xsl:variable name="listRsstatus">
      <xsl:value-of select="OnlineData.listRs/@status" />
    </xsl:variable>

    
      <xsl:if test="$listRsstatus = 'success'">
        <xsl:if test="OnlineData.listRs/policyList/@listCount > 0">
          
            <xsl:for-each select="OnlineData.listRs/policyList/policy">
             <policy>
                <PolicyID>
                  <xsl:value-of select="@policyID" />
                </PolicyID>
                <PartyID>
                  <xsl:value-of select="@clientID" />
                </PartyID>
                <PolicyStatus>
                  <xsl:value-of select="@status" />
                </PolicyStatus>
                <LOB>
                  <xsl:value-of select="@LOB" />
                </LOB>
                <PolicyNumber>
                  <xsl:value-of select="@policyNumber" />
                </PolicyNumber>
                <Description>
                  <xsl:value-of select="@description" />
                </Description>
                <Description2>
                  <xsl:value-of select="@description2" />
                </Description2>
                <EffectiveDate>
                  <xsl:value-of select="@effectiveDate" />
                </EffectiveDate>
                <ExpirationDate>
                  <xsl:value-of select="@expirationDate" />
                </ExpirationDate>
                <TransactionDate>
                  <xsl:value-of select="@transactionDate" />
                </TransactionDate>
                <CreationDate>
                  <xsl:value-of select="@creationDate" />
                </CreationDate>
                <Producer>
                  <xsl:value-of select="@producer" />
                </Producer>
                <NamedInsured>
                  <xsl:value-of select="@clientName" />
                </NamedInsured>
                
                <State>
                  <xsl:value-of select="@state" />
                </State>
                <CountryOfIssue>
                  <xsl:value-of select="@country" />
                </CountryOfIssue>
                <ZipCode>
                  <xsl:value-of select="@zip" />
                </ZipCode>
             </policy>
            </xsl:for-each>
           
          </xsl:if>
      </xsl:if> 
  </xsl:template>
</xsl:stylesheet>