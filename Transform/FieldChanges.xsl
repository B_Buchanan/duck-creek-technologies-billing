﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:for-each select="/_compareResults/node[(@changed='1') and (@showChanges='1')]">
			<xsl:value-of select="@fieldID" />
			<xsl:choose>
				<xsl:when test='@fieldType = "boolean"'>
					<xsl:choose>
						<xsl:when test="value[2]=1"> was enabled. </xsl:when>
						<xsl:otherwise> was disable. </xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="count(value[1])=0 and count(value[2])=0"> record was changed. </xsl:when>
						<xsl:when test="count(value[1]/node()[1])=0 and count(value[2]/node()[1])=1"> record was changed to <xsl:value-of select="value[2]"/>. </xsl:when>
						<xsl:when test="count(value[1])=0"> record was removed. </xsl:when>
						<xsl:when test="count(value[2])=0"> record was added. </xsl:when>
						<xsl:otherwise> was changed from <xsl:value-of select="value[1]" /> to <xsl:value-of select="value[2]" />. </xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>