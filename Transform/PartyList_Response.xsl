﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

	<xsl:template match="_PartyData">
		<partyList return="{CandidateList/SearchControl/Paging/ReturnCount}" total="{CandidateList/SearchControl/Paging/TotalCount}">
			<xsl:apply-templates select="CandidateList/ListItem/ItemData/PartyRecord"/>
		</partyList>
	</xsl:template>

	<xsl:template match="PartyRecord">
		<party id="{PartyRecordPartyId}" name="{Party/PartyCorrespondenceName}" address="{Locations/Location/LocationAddressLine1}" city="{Locations/Location/LocationCity}" state="{Locations/Location/LocationStateCode}" zip="{Locations/Location/LocationPostalCode}" phone="{PartyPhones/PartyPhone/PhoneNumber}" entity="{AuthorizedInterest/Name}" partyType="{Party/PartyTypeCode}"/>
	</xsl:template>

</xsl:stylesheet>