﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:dc="DuckCreek.XSL">
	<xsl:template match="policy">
		<xsl:call-template name="dc:Policy.Body"/>
	</xsl:template>
	<xsl:template name="dc:Policy.Body">
		<DC_Policy Id="{@id}">
			<EffectiveDate><xsl:value-of select="EffectiveDate"/></EffectiveDate>
			<ExpirationDate><xsl:value-of select="ExpirationDate"/></ExpirationDate>
			<LineOfBusiness><xsl:value-of select="LineOfBusiness"/></LineOfBusiness>
			<Term><xsl:value-of select="Term"/></Term>
			<PrimaryRatingState><xsl:value-of select="PrimaryRatingState"/></PrimaryRatingState>
			<Product><xsl:value-of select="Product"/></Product>
			<HonorRates><xsl:value-of select="HonorRates"/></HonorRates>
			<AuditPeriod><xsl:value-of select="AuditPeriod"/></AuditPeriod>
			<SICCode><xsl:value-of select="SICCode"/></SICCode>
			<SICCodeDesc><xsl:value-of select="SICCodeDesc"/></SICCodeDesc>
			<NAICSCode><xsl:value-of select="NAICSCode"/></NAICSCode>
			<NAICSCodeDesc><xsl:value-of select="NAICSCodeDesc"/></NAICSCodeDesc>
			<QuoteNumber><xsl:value-of select="QuoteNumber"/></QuoteNumber>
			<TermFactor><xsl:value-of select="TermFactor"/></TermFactor>
			<CancellationDate><xsl:value-of select="CancellationDate"/></CancellationDate>
			<Description><xsl:value-of select="Description"/></Description>
			<PolicyNumber><xsl:value-of select="PolicyNumber"/></PolicyNumber>
			<Status><xsl:value-of select="Status"/></Status>
			<TransactionDate><xsl:value-of select="TransactionDate"/></TransactionDate>
			<TransactionDateTime><xsl:value-of select="TransactionDateTime"/></TransactionDateTime>
			<PreviousPolicyNumber><xsl:value-of select="PreviousPolicyNumber"/></PreviousPolicyNumber>
			<InceptionDate><xsl:value-of select="InceptionDate"/></InceptionDate>
			<PolicyTermID><xsl:value-of select="PolicyTermID"/></PolicyTermID>
			<AccountID><xsl:value-of select="AccountID"/></AccountID>
			<TaxesSurcharges><xsl:value-of select="TaxesSurcharges"/></TaxesSurcharges>
			<Auditable><xsl:value-of select="Indicators[Type='IsAuditable']/bValue"/></Auditable>
			<xsl:call-template name="dc:Policy.Extensions"/>
			<xsl:call-template name="dc:Policy.Relationships"/>
		</DC_Policy>
	</xsl:template>
	<xsl:template name="dc:Policy.Extensions"/>
	<xsl:template name="dc:Policy.Relationships"/>
</xsl:stylesheet>
