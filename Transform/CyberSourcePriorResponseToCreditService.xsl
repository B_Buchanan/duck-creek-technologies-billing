﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns:c="urn:schemas-cybersource-com:transaction-data-1.41"
	xmlns:sec="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
	<xsl:output omit-xml-declaration="yes"/>

	<xsl:param name="username"/>
	<xsl:param name="password"/>

	<xsl:template match="PaymentProcessorResponse">
		<s:Envelope>
			<s:Header>
				<sec:Security mustUnderstand="1">
					<sec:UsernameToken Id="uuid-90128b0b-6212-4e16-9382-e55489fa6444-1">
						<sec:Username><xsl:value-of select="$username"/></sec:Username>
						<sec:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wssusername-token-profile-1.0#PasswordText"><xsl:value-of select="$password"/></sec:Password>
					</sec:UsernameToken>
				</sec:Security>
			</s:Header>
			<s:Body>
				<xsl:apply-templates select="s:Envelope/s:Body/c:replyMessage"/>
			</s:Body>
		</s:Envelope>
	</xsl:template>
	
	<xsl:template match="c:replyMessage">
		<c:requestMessage>
			<c:merchantID><xsl:value-of select="$username"/></c:merchantID>
			<c:merchantReferenceCode><xsl:value-of select="c:merchantReferenceCode"/></c:merchantReferenceCode>
			<c:purchaseTotals>
				<c:currency><xsl:value-of select="c:purchaseTotals/c:currency"/></c:currency>
				<c:grandTotalAmount><xsl:value-of select="c:ccAuthReply/c:amount"/></c:grandTotalAmount>
			</c:purchaseTotals>
			<c:orderRequestToken><xsl:value-of select="c:requestToken"/></c:orderRequestToken>
			<c:ccCreditService run="true">
				<c:captureRequestID><xsl:value-of select="c:requestID"/></c:captureRequestID>
			</c:ccCreditService>
		</c:requestMessage> 
	</xsl:template>

</xsl:stylesheet>
