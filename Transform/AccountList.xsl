﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<AccountList>
			<xsl:apply-templates select="*/CandidateList/ListItem/ItemData"/>
		</AccountList>
	</xsl:template>

	<xsl:template match="ItemData">
		<Account>
			<AccountId>
				<xsl:value-of select="AccountId"/>
			</AccountId>
			<AccountNumber>
				<xsl:value-of select="AccountReference"/>
			</AccountNumber>
			<AccountTypeCode>
				<xsl:value-of select="AccountTypeCode"/>
			</AccountTypeCode>
			<xsl:apply-templates select="//Parties/PartyRecord[PartyRecordPartyId=current()/AssociatedParties/Party/@PartyId]"/>
		</Account>
	</xsl:template>

	<xsl:template match="PartyRecord">
		<xsl:apply-templates select="Party"/>
		<xsl:apply-templates select="Locations/Location[1]"/>
		<Status>Active</Status>
	</xsl:template>

	<xsl:template match="Party">
		<PayorName>
			<xsl:value-of select="PartyFirstName"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="PartyName"/>
		</PayorName>
	</xsl:template>

	<xsl:template match="Location">
		<Address>
			<xsl:value-of select="LocationCity"/>, <xsl:value-of select="LocationStateCode"/>,  <xsl:value-of select="LocationPostalCode"/></Address>
	</xsl:template>
</xsl:stylesheet>
<!-- Stylesheet edited using Stylus Studio - (c) 2004-2006. Progress Software Corporation. All rights reserved. -->