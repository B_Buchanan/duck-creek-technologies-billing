﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes"/>
	
	<xsl:template name="BuildAmountItem.BOR">
		<xsl:param name="tranEffectiveDate"/>
		<xsl:param name="entryindex"/>
		<xsl:param name="recordType" />
		<xsl:apply-templates select="/*/_TransactionReport/report/entry[@type=$recordType][position()=$entryindex]/financials/policy/transaction/transEntry">
			<xsl:sort select="@CurrencyAmt" data-type="number" order="descending"/>
			<xsl:with-param name="termEffectiveDate">
				<xsl:value-of select="$tranEffectiveDate"/>
			</xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>
	
	<xsl:template match="transEntry">
		<xsl:variable name="transaction" select="ancestor::entry/transaction"/>
		<xsl:variable name="policyTermEffectiveDate">
			<xsl:call-template name="LookupPolicyTermEffectiveDate">
				<xsl:with-param name="EffectiveDate">
					<xsl:value-of select="$transaction/EffectiveDate"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="transGroup" select="$transaction/../../../report/entry[@type = 'onset'][1]/@transGroup" />
		<AmountItem>
			<xsl:if test="$transaction/@id = $transGroup or $transaction/OriginalID = $transGroup">
				<CurrentTermTrans>1</CurrentTermTrans>
			</xsl:if>
			<id>
				<xsl:value-of select="$transaction/@id"/>
			</id>
			<TransactionTypeCode>
				<xsl:call-template name="LookupTransactionTypeCode">
					<xsl:with-param name="code">
						<xsl:value-of select="$transaction/Type"/>
					</xsl:with-param>
					<xsl:with-param name="termEffectiveDate">
						<xsl:value-of select="$policyTermEffectiveDate"/>
					</xsl:with-param>
					<xsl:with-param name="effectiveDate">
						<xsl:value-of select="$transaction/EffectiveDate"/>
					</xsl:with-param>
					<xsl:with-param name="deprecatedBy">
						<xsl:value-of select="$transaction/DeprecatedBy"/>
					</xsl:with-param>
					<xsl:with-param name="onsetBy">
						<xsl:value-of select="$transaction/OnsetBy"/>
					</xsl:with-param>
				</xsl:call-template>
			</TransactionTypeCode>
			<ReasonTypeCode />
			
			<ItemEffectiveDate><xsl:value-of select="@EntryEffDt"/></ItemEffectiveDate>
			<ItemExpirationDate><xsl:value-of select="@EntryExpDt"/></ItemExpirationDate>
			<CoverageReference>
				<!-- Needs to be disabled for right now because of negative (discount) premiums.
				<xsl:value-of select="@CoverageGUID"/>-->
			</CoverageReference>
			<ReceivableTypeCode><xsl:value-of select="@EntryTypeCd"/></ReceivableTypeCode>
			<ReceivableSubTypeCode />
			<ItemAmount><xsl:value-of select="@CurrencyAmt"/></ItemAmount>
			
			<AggregationReference />
			<CommissionGroupReference />
			<ItemCommissionAmount />
			<ItemCommissionPercent />
			<ItemCommissionPlanId/>
			<ItemCommissionType/>
			<UnitReference/>
			<CallingProcess>UPTM</CallingProcess>
			<FinancialReportingGroupReference><xsl:value-of select="statEntry/@ASLOBCd"/></FinancialReportingGroupReference>
		</AmountItem>
	</xsl:template>
	
</xsl:stylesheet>
