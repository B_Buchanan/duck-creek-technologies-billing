﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns="urn:schemas-cybersource-com:transaction-data-1.41"
	xmlns:sec="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
	<xsl:output omit-xml-declaration="yes"/>

	<xsl:param name="username"/>
	<xsl:param name="password"/>

	<xsl:template match="PaymentRecord">
		<s:Envelope>
			<s:Header>
				<sec:Security mustUnderstand="1">
					<sec:UsernameToken Id="uuid-90128b0b-6212-4e16-9382-e55489fa6444-1">
						<sec:Username><xsl:value-of select="$username"/></sec:Username>
						<sec:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wssusername-token-profile-1.0#PasswordText"><xsl:value-of select="$password"/></sec:Password>
					</sec:UsernameToken>
				</sec:Security>
			</s:Header>
			<s:Body>
				<xsl:call-template name="requestMessage"/>
			</s:Body>
		</s:Envelope>
	</xsl:template>
	
	<xsl:template name="requestMessage">
		<requestMessage>
			<xsl:call-template name="merchantData"/>
			<xsl:apply-templates select="PaymentMethodDetail/*"/>
			<xsl:call-template name="requestServices"/>
		</requestMessage> 
	</xsl:template>

	<xsl:template name="merchantData">
		<merchantID><xsl:value-of select="$username"/></merchantID>
		<merchantReferenceCode><xsl:value-of select="TransactionId"/></merchantReferenceCode>
	</xsl:template>

	<xsl:template name="billTo">
		<billTo>
			<firstName><xsl:value-of select="BillToFirstName"/></firstName>
			<lastName><xsl:value-of select="BillToLastName"/></lastName>
			<street1><xsl:value-of select="BillToAddressLine1"/></street1>
			<city><xsl:value-of select="BillToCity"/></city>
			<state><xsl:value-of select="BillToState"/></state>
			<postalCode><xsl:value-of select="BillToPostalCode"/></postalCode>
			<country><xsl:value-of select="BillToCountry"/></country>
			<email><xsl:value-of select="BillToEmail"/></email>
		</billTo>
	</xsl:template>

	<xsl:template name="purchaseTotals">
		<purchaseTotals>
			<currency>USD</currency>
			<!--
			<currency><xsl:value-of select="CurrencyCulture"/></currency>
			-->
			<grandTotalAmount><xsl:value-of select="../../Amount"/></grandTotalAmount>
		</purchaseTotals>
	</xsl:template>
	
	<xsl:template match="CreditCard">
		<xsl:call-template name="billTo"/>
		<xsl:call-template name="purchaseTotals"/>
		<card>
			<accountNumber><xsl:value-of select="CardNumber"/></accountNumber>
			<expirationMonth><xsl:value-of select="ExpiryMonth"/></expirationMonth>
			<expirationYear><xsl:value-of select="ExpiryYear"/></expirationYear>
		</card>
	</xsl:template>

	<xsl:template match="Check">
		<xsl:call-template name="billTo"/>
		<xsl:call-template name="purchaseTotals"/>
		<check>
			<fullName><xsl:value-of select="LastName"/></fullName>
			<accountNumber><xsl:value-of select="AccountNumber"/></accountNumber>
			<bankTransitNumber><xsl:value-of select="TransitNumber"/></bankTransitNumber>
			<checkNumber><xsl:value-of select="CheckNumber"/></checkNumber>
		</check>
	</xsl:template>

	<xsl:template name="requestServices">
		<ccAuthService run="true"/>
		<ccCaptureService run="true"/>
	</xsl:template>

</xsl:stylesheet>
