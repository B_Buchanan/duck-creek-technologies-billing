﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="PolicyTermExtendedData.xsl"/>
	<xsl:output omit-xml-declaration="yes"/>

	<xsl:template match="/*">
		<Billing.postPolicyRq>
			<PolicyData>
				<xsl:apply-templates select="session/data/policy"/>
				<CurrencyCulture>
					<xsl:value-of select="session/properties/@cultureCode"/>
				</CurrencyCulture>
				<BillItemExtendedData/>
			</PolicyData>
		</Billing.postPolicyRq>
	</xsl:template>

	<xsl:template match="policy">
		<AccountId>
			<xsl:value-of select="AccountId"/>
		</AccountId>
		<PolicyTermId>
			<xsl:value-of select="PolicyTermId"/>
		</PolicyTermId>
		<PolicyLineOfBusinessCode>
			<xsl:value-of select="LineOfBusinessCode"/>
		</PolicyLineOfBusinessCode>
		<PolicyProductCode>
			<xsl:value-of select="ProductCode"/>
		</PolicyProductCode>
		<PolicyIssueSystemCode>
			<xsl:value-of select="IssueSystem"/>
		</PolicyIssueSystemCode>
		<PolicyMasterCompanyCode>
			<xsl:value-of select="MasterCompany"/>
		</PolicyMasterCompanyCode>
		<PolicyReference>
			<xsl:value-of select="PolicyNumber"/>
		</PolicyReference>
		<PolicyTermEffectiveDate>
			<xsl:value-of select="EffectiveDate"/>
		</PolicyTermEffectiveDate>
		<PolicyTermExpirationDate>
			<xsl:value-of select="ExpirationDate"/>
		</PolicyTermExpirationDate>
		<TransactionTypeCode>
			<xsl:call-template name="LookupTransactionTypeCode">
				<xsl:with-param name="code">
					<xsl:value-of select="../policyAdmin/transactions/transaction[last()]/Type"/>
				</xsl:with-param>
			</xsl:call-template>
		</TransactionTypeCode>
		<ReasonTypeCode>
			<xsl:call-template name="LookupReasonTypeCode">
				<xsl:with-param name="code">
					<xsl:value-of select="LastReasonType"/>
				</xsl:with-param>
			</xsl:call-template>
		</ReasonTypeCode>
		<Auditable>
			<xsl:value-of select="Indicators[Type='IsAuditable']/bValue"/>
		</Auditable>
		<CommissionPlanId>
			<xsl:value-of select="CommissionPlanId"/>
		</CommissionPlanId>
		<xsl:call-template name="BuildItemList">
			<xsl:with-param name="transactionId">
				<xsl:value-of select="../policyAdmin/transactions/transaction[last()]/@id"/>
			</xsl:with-param>
			<xsl:with-param name="commissionAmount">
				<xsl:choose>
					<xsl:when test="TransactionCommissionType[node()]">
						<xsl:choose>
						<xsl:when test="TransactionCommissionType = 'FLAT'">
							<xsl:value-of select="TransactionCommissionValue"/>
						</xsl:when>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="CommissionAmount"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="commissionPercent">
				<xsl:choose>
					<xsl:when test="TransactionCommissionType[node()]">
						<xsl:choose>
						<xsl:when test="TransactionCommissionType = 'PCT'">
							<xsl:value-of select="TransactionCommissionValue div 100"/>
						</xsl:when>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="CommissionPercent" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="commissionType">
				<xsl:value-of select="TransactionCommissionType"/>
			</xsl:with-param>
			<xsl:with-param name="commissionPlanId">
				<xsl:value-of select="CommissionPlanId"/>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="PolicyTermExtendedData"/>
	</xsl:template>

	<xsl:template name="BuildItemList">
		<xsl:param name="transactionId"/>
		<xsl:param name="commissionAmount"/>
		<xsl:param name="commissionPercent"/>
		<xsl:param name="commissionPlanId"/>
		<xsl:param name="commissionType"/>

		<AmountItems>
			<xsl:call-template name="BuildItems">
				<xsl:with-param name="transactionId" select="$transactionId"/>
				<xsl:with-param name="commissionAmount" select="$commissionAmount"/>
				<xsl:with-param name="commissionPercent" select="$commissionPercent"/>
				<xsl:with-param name="commissionPlanId" select="$commissionPlanId"/>
				<xsl:with-param name="commissionType" select="$commissionType"/>
			</xsl:call-template>
			<!--
				This will need to change if we upgrade to newer version, current Billing Base Product is on 3_0_0_0
				Current Version:	data/policy/TaxesSurcharges/@change
				5_0_0_0 : 			data/policy/TaxesSurchargesChange
			-->
			<xsl:apply-templates select="TaxesSurcharges[@change != '0']">
				<xsl:with-param name="commissionPlanId" select="$commissionPlanId"/>
				<xsl:with-param name="commissionType" select="$commissionType"/>
			</xsl:apply-templates>
			<!--			
			<xsl:apply-templates select="TaxesSurcharges[TaxesSurcharges/@change != '0']"/>
-->
		</AmountItems>
	</xsl:template>

	<xsl:template name="BuildItems">
		<xsl:param name="transactionId"/>
		<xsl:param name="commissionAmount"/>
		<xsl:param name="commissionPercent"/>
		<xsl:param name="commissionPlanId"/>
		<xsl:param name="commissionType"/>


		<!-- list this current record, deprecated, and onset -->
		<!--
		/_processingData/transactions/transaction[@id='t7AC27587E4964E5A8179F1024E63EBEA' 
		or DeprecatedBy='t7AC27587E4964E5A8179F1024E63EBEA'
		or OnsetBy='t7AC27587E4964E5A8179F1024E63EBEA']
		-->
		<xsl:for-each select="/*/_processingData/transactions/transaction[@id=$transactionId or ((DeprecatedBy=$transactionId) and (Type != 'Renew')) or ((OnsetBy=$transactionId) and (Type != 'Renew'))]">
			<xsl:choose>
				<xsl:when test="/*/session/data/policy/MPRPolicy = '1' and /*/session/data/policyAdmin/transactions/transaction[last()]/Type = 'FinalAudit'">
					<xsl:call-template name="BuildMPRFinalAuditAmountItem">
						<xsl:with-param name="commissionAmount" select="$commissionAmount"/>
						<xsl:with-param name="commissionPercent" select="$commissionPercent"/>
						<xsl:with-param name="commissionPlanId" select="$commissionPlanId"/>
						<xsl:with-param name="commissionType" select="$commissionType"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="BuildAmountItem">
						<xsl:with-param name="commissionAmount" select="$commissionAmount"/>
						<xsl:with-param name="commissionPercent" select="$commissionPercent"/>
						<xsl:with-param name="commissionPlanId" select="$commissionPlanId"/>
						<xsl:with-param name="commissionType" select="$commissionType"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="BuildAmountItem">
		<xsl:param name="commissionAmount"/>
		<xsl:param name="commissionPercent"/>
		<xsl:param name="commissionPlanId"/>
		<xsl:param name="commissionType"/>

		<xsl:variable name="useDeposit">
			<xsl:value-of select="/*/session/data/policy/MPRPolicy"/>
		</xsl:variable>
		<xsl:variable name="charge">
			<xsl:choose>
				<xsl:when test="$useDeposit = 1">
					<xsl:value-of select="Deposit"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="Charge"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<AmountItem>
			<id>
				<xsl:value-of select="@id"/>
			</id>
			<TransactionTypeCode>
				<xsl:call-template name="LookupTransactionTypeCode">
					<xsl:with-param name="code">
						<xsl:value-of select="Type"/>
					</xsl:with-param>
				</xsl:call-template>
			</TransactionTypeCode>
			<ReasonTypeCode>
				<xsl:call-template name="LookupReasonTypeCode">
					<xsl:with-param name="code">
						<xsl:value-of select="reasons/reason[last()]/Code"/>
					</xsl:with-param>
				</xsl:call-template>
			</ReasonTypeCode>
			<ItemEffectiveDate>
				<xsl:value-of select="EffectiveDate"/>
			</ItemEffectiveDate>
			<ItemExpirationDate>
				<xsl:value-of select="ExpirationDate"/>
			</ItemExpirationDate>
			<CoverageReference/>
			<ReceivableTypeCode>PREM</ReceivableTypeCode>
			<ReceivableSubTypeCode>
				<xsl:choose>
					<xsl:when test="$useDeposit = 1">DPST</xsl:when>
					<xsl:otherwise/>
				</xsl:choose>
			</ReceivableSubTypeCode>
			<ItemAmount>
				<!-- if this is a deprecated transaction, that indicates we need to reverse the charge, either by
					applying a credit of the same amount, or by charging back the amount that was refunded by this transaction -->
				<xsl:choose>
					<xsl:when test="DeprecatedBy != ''">
						<xsl:value-of select="$charge * -1"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$charge"/>
					</xsl:otherwise>
				</xsl:choose>
			</ItemAmount>
			<AggregationReference/>
			<CommissionGroupReference/>
			<ItemCommissionAmount>
				<xsl:value-of select="$commissionAmount"/>
			</ItemCommissionAmount>
			<ItemCommissionPercent>
				<xsl:value-of select="$commissionPercent"/>
			</ItemCommissionPercent>
			<ItemCommissionPlanId>
				<xsl:value-of select="$commissionPlanId"/>
			</ItemCommissionPlanId>
			<ItemCommissionType>
				<xsl:value-of select="$commissionType"/>
			</ItemCommissionType>

			<ReceivableSubTypeCode/>
			<UnitReference/>
			<CallingProcess>UPTM</CallingProcess>
		</AmountItem>
	</xsl:template>

	<xsl:template name="BuildMPRFinalAuditAmountItem">
		<xsl:param name="commissionAmount"/>
		<xsl:param name="commissionPercent"/>
		<xsl:param name="commissionPlanId"/>
		<xsl:param name="commissionType"/>
		<!-- list the current node of the $transactionId -->
		<AmountItem>
			<id>
				<xsl:value-of select="@id"/>
			</id>
			<TransactionTypeCode>
				<xsl:call-template name="LookupTransactionTypeCode">
					<xsl:with-param name="code">
						<xsl:value-of select="Type"/>
					</xsl:with-param>
				</xsl:call-template>
			</TransactionTypeCode>
			<ReasonTypeCode>
				<xsl:call-template name="LookupReasonTypeCode">
					<xsl:with-param name="code">
						<xsl:value-of select="reasons/reason[last()]/Code"/>
					</xsl:with-param>
				</xsl:call-template>
			</ReasonTypeCode>
			<ItemEffectiveDate>
				<xsl:value-of select="EffectiveDate"/>
			</ItemEffectiveDate>
			<ItemExpirationDate>
				<xsl:value-of select="ExpirationDate"/>
			</ItemExpirationDate>
			<CoverageReference/>
			<ReceivableTypeCode>PREM</ReceivableTypeCode>
			<ItemAmount>
				<xsl:value-of select="/*/session/data/policy/Premium"/>
			</ItemAmount>
			<AggregationReference/>
			<CommissionGroupReference/>
			<ItemCommissionAmount>
				<xsl:value-of select="$commissionAmount"/>
			</ItemCommissionAmount>
			<ItemCommissionPercent>
				<xsl:value-of select="$commissionPercent"/>
			</ItemCommissionPercent>
			<ItemCommissionPlanId>
				<xsl:value-of select="$commissionPlanId"/>
			</ItemCommissionPlanId>
			<ItemCommissionType>
				<xsl:value-of select="$commissionType"/>
			</ItemCommissionType>

			<ReceivableSubTypeCode/>
			<UnitReference/>
			<CallingProcess>UPTM</CallingProcess>
		</AmountItem>
	</xsl:template>

	<xsl:template match="TaxesSurcharges">
		<xsl:param name="commissionPlanId"/>
		<xsl:param name="commissionType"/>
		<xsl:variable name="lastTransaction" select="/*/session/data/policyAdmin/transactions/transaction[last()]"/>
		<AmountItem>
			<CoverageReference/>
			<ReceivableTypeCode>FEE</ReceivableTypeCode>
			<!--
			<ItemAmount><xsl:value-of select="TaxesSurcharges/@change" /></ItemAmount>
			<ItemAmount><xsl:value-of select="@change" /></ItemAmount>
			-->
			<TransactionTypeCode>
				<xsl:call-template name="LookupTransactionTypeCode">
					<xsl:with-param name="code">
						<xsl:value-of select="$lastTransaction/Type"/>
					</xsl:with-param>
				</xsl:call-template>
			</TransactionTypeCode>
			<ReasonTypeCode>
				<xsl:call-template name="LookupReasonTypeCode">
					<xsl:with-param name="code">
						<xsl:value-of select="$lastTransaction/reasons/reason[last()]/Code"/>
					</xsl:with-param>
				</xsl:call-template>
			</ReasonTypeCode>
			<ItemEffectiveDate>
				<xsl:value-of select="$lastTransaction/EffectiveDate"/>
			</ItemEffectiveDate>
			<ItemExpirationDate>
				<xsl:value-of select="$lastTransaction/ExpirationDate"/>
			</ItemExpirationDate>
			<ItemAmount>
				<xsl:value-of select="@change"/>
			</ItemAmount>
			<AggregationReference/>
			<CommissionGroupReference/>
			<ItemCommissionAmount>0</ItemCommissionAmount>
			<ItemCommissionPercent>0</ItemCommissionPercent>
			<ItemCommissionPlanId>
				<xsl:value-of select="$commissionPlanId"/>
			</ItemCommissionPlanId>
			<ItemCommissionType>
				<xsl:value-of select="$commissionType"/>
			</ItemCommissionType>
			<ReceivableSubTypeCode>
				<xsl:call-template name="LookupReceivableSubTypeCode">
					<xsl:with-param name="code">
						<xsl:value-of select="Type"/>
					</xsl:with-param>
				</xsl:call-template>
			</ReceivableSubTypeCode>
			<UnitReference/>
		</AmountItem>
	</xsl:template>

	<xsl:template name="LookupTransactionTypeCode">
		<xsl:param name="code"/>
		<xsl:choose>
			<xsl:when test="$code = 'New'">NBUS</xsl:when>
			<xsl:when test="$code = 'Endorse'">ENDT</xsl:when>
			<xsl:when test="$code = 'EndorseWorkComp'">ENDT</xsl:when>
			<xsl:when test="$code = 'Cancel'">
				<xsl:choose>
					<xsl:when test="/*/session/data/policyAdmin/transactions/transaction[last()]/EffectiveDate = /*/session/data/policyAdmin/transactions/transaction[1]/EffectiveDate">FCAN</xsl:when>
					<xsl:otherwise>PCAN</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$code = 'Renew'">RNEW</xsl:when>
			<xsl:when test="$code = 'Reinstate'">REIN</xsl:when>
			<xsl:when test="$code = 'Reissue'">REIS</xsl:when>
			<xsl:when test="$code = 'Rewrite'">RWRI</xsl:when>
			<xsl:when test="$code = 'Information'">INFO</xsl:when>
			<xsl:when test="$code = 'BookPolicy'">INFO</xsl:when>
			<xsl:when test="$code = 'IssuePolicy'">INFO</xsl:when>
			<xsl:when test="$code = 'RescindNonRenew'">INFO</xsl:when>
			<xsl:when test="$code = 'NonRenew'">INFO</xsl:when>
			<xsl:when test="$code = 'RenewalChange'">INFO</xsl:when>
			<xsl:when test="$code = 'FinalAudit'">FAUD</xsl:when>
			<xsl:when test="$code = 'RevisedFinalAudit'">RAUD</xsl:when>
			<xsl:when test="$code = 'InterimAudit'">IAUD</xsl:when>
			<xsl:when test="$code = 'VoidFinalAudit'">VAUD</xsl:when>
			<xsl:when test="$code = 'Rescind'">RESC</xsl:when>
			<xsl:when test="$code = 'RescindCancelPending'">RESC</xsl:when>
			<xsl:when test="$code = 'PaidInFull' or $code = 'PIF'">PIF</xsl:when>
			<xsl:otherwise>OTHR</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="LookupReasonTypeCode">
		<xsl:param name="code"/>
		<xsl:choose>
			<xsl:when test="$code = 'nonPay'">NPAY</xsl:when>
			<xsl:when test="$code = ''"/>
			<xsl:otherwise>UKWN</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="LookupReceivableSubTypeCode">
		<xsl:param name="code"/>
		<xsl:choose>
			<xsl:when test="$code = 'PolicyFee'">POL</xsl:when>
			<xsl:when test="$code = 'ReinstatementFee'">REIN</xsl:when>
			<xsl:when test="$code = 'FireInsuranceFee'">FIRE</xsl:when>
			<xsl:when test="$code = 'InstallmentPlanFee'">INST</xsl:when>
			<xsl:when test="$code = 'CancellationFee'">CANC</xsl:when>
			<xsl:otherwise/>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>