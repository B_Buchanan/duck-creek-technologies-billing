﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	
	<xsl:template match="PartyRecord">
		<PartyObj.updatePartyPhoneRq>
			<RequestReturnData>Y</RequestReturnData>
			<TransactionDate>
				<xsl:value-of select="@transactionDate"/>
			</TransactionDate>
			<PartyRecord>
				<PartyRecordPartyId>
					<xsl:value-of select="@partyId"/>
				</PartyRecordPartyId>
				<PartyPhones>
					<xsl:apply-templates select="PartyPhone"/>
				</PartyPhones>
			</PartyRecord>
		</PartyObj.updatePartyPhoneRq>
	</xsl:template>
	
	<xsl:template match="PartyPhone">
		<PartyPhone>
			<xsl:copy-of select="*"/>
		</PartyPhone>
	</xsl:template>

</xsl:stylesheet>
