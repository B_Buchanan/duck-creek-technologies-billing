﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

	<xsl:template match="*">
		<PartyObj.addPartyRs status="success" databaseId="{PartyRecord/Party/PartyId}"/>
	</xsl:template>

</xsl:stylesheet>

