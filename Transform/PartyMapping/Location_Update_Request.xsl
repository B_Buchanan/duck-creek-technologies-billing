﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	
	<!-- Assuming this is not the default location -->
	<xsl:template match="PartyRecord">
		<PartyObj.correctPartyLocationRq>
			<RequestReturnData>Y</RequestReturnData>
			<TransactionDate>
				<xsl:value-of select="@transactionDate"/>
			</TransactionDate>
			<CalledInHierarchy>N</CalledInHierarchy>
			<PartyRecord>
				<PartyRecordPartyId>
					<xsl:value-of select="@partyId"/>
				</PartyRecordPartyId>
				<Locations>
					<xsl:apply-templates select="Location"/>
				</Locations>
			</PartyRecord>
		</PartyObj.correctPartyLocationRq>
	</xsl:template>
	
	<xsl:template match="Location">
		<Location>
			<xsl:copy-of select="*"/>
		</Location>
	</xsl:template>

</xsl:stylesheet>
