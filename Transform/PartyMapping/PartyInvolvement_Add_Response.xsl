﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

	<xsl:template match="*">
		<PartyObj.addPartyInvolvementRs status="success" databaseId="{PartyRecord/PartyInvolvements/PartyInvolvement/PartyInvolvementId}"/>
	</xsl:template>

</xsl:stylesheet>
