﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	
	<xsl:template match="PartyRecord">
		<PartyObj.addPartyPhoneRq>
			<RequestReturnData>Y</RequestReturnData>
			<TransactionDate>
				<xsl:value-of select="@transactionDate"/>
			</TransactionDate>
			<CalledInHierarchy>N</CalledInHierarchy>
			<PartyRecord>
				<PartyRecordPartyId>
					<xsl:value-of select="@partyId"/>
				</PartyRecordPartyId>
				<PartyPhones>
					<xsl:apply-templates select="PartyPhone"/>
				</PartyPhones>
			</PartyRecord>
		</PartyObj.addPartyPhoneRq>
	</xsl:template>

	<xsl:template match="PartyPhone">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="PartyPhoneId"/>
	
</xsl:stylesheet>
