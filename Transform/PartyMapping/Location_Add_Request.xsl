﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	
	<!-- Assuming this is not the default location -->
	<xsl:template match="PartyRecord">
		<PartyObj.addPartyLocationRq>
			<RequestReturnData>Y</RequestReturnData>
			<TransactionDate>
				<xsl:value-of select="@transactionDate"/>
			</TransactionDate>
			<CalledInHierarchy>N</CalledInHierarchy>
			<PartyRecord>
				<PartyRecordPartyId>
					<xsl:value-of select="@partyId"/>
				</PartyRecordPartyId>
				<Locations>
					<xsl:apply-templates select="Location"/>
				</Locations>
			</PartyRecord>
		</PartyObj.addPartyLocationRq>
	</xsl:template>
	
	<xsl:template match="Location">
		<Location>
			<DefaultLocationIndicator>N</DefaultLocationIndicator>
			<xsl:copy-of select="*"/>
			<LocationInvolvements>
				<LocationInvolvement>
					<ObjectTypeCode>PTY</ObjectTypeCode>
					<RoleCode/>
					<LocationInvolvementTypeCode>P</LocationInvolvementTypeCode>
					<EffectiveDate/>
					<ExpirationDate />
				</LocationInvolvement>
			</LocationInvolvements>
		</Location>
	</xsl:template>

</xsl:stylesheet>
