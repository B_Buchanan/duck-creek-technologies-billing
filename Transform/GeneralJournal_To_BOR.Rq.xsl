﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes"/>

	<xsl:template match="*">
		<xsl:call-template name="FinancialLog.addRq"/>
	</xsl:template>

	<xsl:template name="AuditLog.addRq">
		<xsl:variable name="varTotal" select="sum(Journal/@TransactionGrossAmount)" />
		<RequestProxy.sendRq endPoint="Balancing">
			<AuditLog.addRq>
				<auditLog sourceReference="{Journal/@JournalId}" sourceUID="{Journal/@JournalId}" sourceTypeCode="BGJ" transactionLabel="{Journal/@TransactionTypeCode}" typeCode="GJRNL" componentName="GeneralJournal" sourceSystemCode="DCBIL" transactionTotalAmount="{$varTotal}" auditAmount="{$varTotal}" />
			</AuditLog.addRq>
		</RequestProxy.sendRq>
	</xsl:template>

	<xsl:template name="FinancialLog.addRq">
		<!-- This creates the Audit Record for the General Journal -->
		<xsl:call-template name="AuditLog.addRq"/>
		<!-- This creates the Financial/Audit for the BOR -->
		<FinancialLog.addRq>
			<financials SourceRef="{Journal/@JournalId}" SourceUID="{Journal/@JournalId}" SourceUIDCd="BGJ" AuditAmt="{sum(Journal/@TransactionGrossAmount)}">
				<xsl:call-template name="BillAccount"/>
			</financials>
		</FinancialLog.addRq>
	</xsl:template>

	<xsl:template name="BillAccount">
		<billAccount AccountRef="{Journal/@AccountId}" AccountGUID="{Journal/@AccountId}" IssueSystemCd="DCBIL" BillItemGUID="{Journal/@AccountId}">
			<xsl:call-template name="Transaction"/>
		</billAccount>
	</xsl:template>

	<xsl:template name="Transaction">
		<transaction TransEffDt="{Journal/@ActivityEffectiveDate}" TransactionLabel="{Journal/@TransactionTypeCode}" TransactionUID="{Journal/@JournalId}" TransTotalAmt="{sum(Journal/@TransactionGrossAmount)}" CurrencyCd="USD">
			<xsl:apply-templates select="Journal"/>
		</transaction>		
	</xsl:template>

	<xsl:template match="Journal">
		<transEntry TypeCd="DRECT" EntryTypeCd="{@ActivityTypeCode}" CurrencyAmt="{@TransactionGrossAmount}" StateAbbr="{@AccountStateCode}" EntryEffDt="{@ActivityEffectiveDate}" CurrencyCd="USD"/>
	</xsl:template>
</xsl:stylesheet>
