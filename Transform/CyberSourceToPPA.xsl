﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" 
	xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:c="urn:schemas-cybersource-com:transaction-data-1.41"
	>
	<xsl:output omit-xml-declaration="yes"/>

	<xsl:template match="s:Envelope">
		<xsl:apply-templates select="s:Body/c:replyMessage"/>
	</xsl:template>

	<xsl:template match="c:replyMessage">
		<replyMessage>
			<duckCreekId><xsl:value-of select="c:merchantReferenceCode"/></duckCreekId>
			<transactionId><xsl:value-of select="c:ccAuthReply/c:reconciliationID"/></transactionId>
			<requestReference><xsl:value-of select="c:requestID"/></requestReference>
			<processor>C</processor>
			<status><xsl:value-of select="c:ccAuthReply/c:processorResponse"/></status>
			<returnCode><xsl:value-of select="c:reasonCode"/></returnCode>
			<statusDescription><xsl:value-of select="c:decision"/></statusDescription>
		</replyMessage>
	</xsl:template>

</xsl:stylesheet>
