﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes"/>
		
	<xsl:template match="/">
		<xsl:if test="count(/BillItemRecords/BillItemRecord) &gt; 0">
			<xsl:call-template name="PolicyData" />
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="PolicyData">
		<PolicyData>
			<AccountId><xsl:value-of select="/BillItemRecords/BillItemRecord/AccountId" /></AccountId>
			<PolicyTermId><xsl:value-of select="/BillItemRecords/BillItemRecord/PolicyTermId" /></PolicyTermId>
			<ItemEffectiveDate><xsl:value-of select="/BillItemRecords/BillItemRecord/ItemEffectiveDate" /></ItemEffectiveDate>
			<ItemExpirationDate><xsl:value-of select="/BillItemRecords/BillItemRecord/ItemExpirationDate"/></ItemExpirationDate>
			<PolicyLineOfBusinessCode><xsl:value-of select="/BillItemRecords/BillItemRecord/PolicyLineOfBusinessCode" /></PolicyLineOfBusinessCode>
			<PolicyReference><xsl:value-of select="/BillItemRecords/BillItemRecord/PolicyReference" /></PolicyReference>
			<TransactionTypeCode><xsl:value-of select="/BillItemRecords/BillItemRecord/TransactionTypeCode" /></TransactionTypeCode>
			<AmountItems>
				<AmountItem>
					<CoverageReference />
					<ReceivableTypeCode>PREM</ReceivableTypeCode>
					<ItemAmount><xsl:value-of select="/BillItemRecords/BillItemRecord/ItemAmount" /></ItemAmount>
					<AggregationReference />
					<CommissionGroupReference><xsl:value-of select="/BillItemRecords/BillItemRecord/CommissionGroupReference" /></CommissionGroupReference>
					<ItemCommissionAmount><xsl:value-of select="/BillItemRecords/BillItemRecord/ItemCommissionAmount" /></ItemCommissionAmount>
					<ItemCommissionPercent><xsl:value-of select="/BillItemRecords/BillItemRecord/ItemCommissionPercent" /></ItemCommissionPercent>
					<ReceivableSubTypeCode />
					<UnitReference />
					<CallingProcess>TRFT</CallingProcess>
				</AmountItem>
			</AmountItems>
			<PolicyProductCode>BillingBaseProduct</PolicyProductCode>
			<CurrencyCulture>en-US</CurrencyCulture>
			<PolicyIssueSystemCode>DCT</PolicyIssueSystemCode>
			<PolicyMasterCompanyCode>DCT</PolicyMasterCompanyCode>
			<BillItemExtendedData />
			<CalculateEffectiveARP>true</CalculateEffectiveARP>
		</PolicyData>
	</xsl:template>

</xsl:stylesheet>