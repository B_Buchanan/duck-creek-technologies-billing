﻿<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
	<xsl:template match="/">
	<DC_ClearanceDetails>
    <xsl:if test="session/data/policy/PackagePolicyIndicator[contains(text(),'1')]" >
			<xsl:for-each select="session/data/policy/line[Indicators[Type='Selected'] and Indicators[bValue='1']]">
				<DC_ClearanceMatch>
					<MatchCodeType>LOB</MatchCodeType>
					<MatchLabel>Line</MatchLabel>
					<MatchValue><xsl:value-of select="Type"/></MatchValue>
					<SourceID><xsl:value-of select="@id"/></SourceID>
				</DC_ClearanceMatch>
			</xsl:for-each>
     </xsl:if>
	</DC_ClearanceDetails>
	</xsl:template>
</xsl:stylesheet>