﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

	<xsl:template match="Party.listRq">
		<_PartyData>
			<SearchRequestRecord>
				<SoundexIndicator>0</SoundexIndicator>
				<SearchCriteria>
					<PartyRecord>
						<Locations>
							<Location>
								<LocationInvolvements>
									<LocationInvolvement>
										<ObjectTypeCode>L</ObjectTypeCode>
										<RoleCode>OT</RoleCode>
									</LocationInvolvement>
								</LocationInvolvements>
								<LocationAddressLine1 />
								<LocationCity />
								<LocationStateCode><xsl:value-of select="@state"/></LocationStateCode>
								<LocationPostalCode />
							</Location>
						</Locations>
						<Party>
							<PartyFirstName />
							<PartyMiddleName />
							<PartyTypeCode />
							<PartyName><xsl:value-of select="@name"/></PartyName>
							<PartyBirthDate />
						</Party>
						<PartyEmails>
							<PartyEmail/>
						</PartyEmails>
						<PartyLicenses>
							<PartyLicense>
								<LicenseExtendedData/>
								<LicenseNumber />
							</PartyLicense>
						</PartyLicenses>
						<PartyPhones>
							<PartyPhone>
								<PhoneNumber><xsl:value-of select="@phone"/></PhoneNumber>
							</PartyPhone>
						</PartyPhones>
						<PartyReferences>
							<PartyReference>
								<Reference><xsl:value-of select="@reference"/></Reference>
							</PartyReference>
						</PartyReferences>
					</PartyRecord>
					<PartyNameSearchType />
				</SearchCriteria>
				<SearchControl>
					<Paging>
						<ListStart><xsl:value-of select="@start"/></ListStart>
						<ListLength><xsl:value-of select="@count"/></ListLength>
					</Paging>
					<!--
					<Filters>
						<Filter Type="PartyType" Value="{@type}" />
					</Filters>
					-->
				</SearchControl>
			</SearchRequestRecord>
			<CandidateList/>
		</_PartyData>
	</xsl:template>

</xsl:stylesheet>