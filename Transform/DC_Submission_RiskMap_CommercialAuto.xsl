﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:template match="Session.getAllDocumentsRs">
		<xsl:apply-templates select="session" />
	</xsl:template>
	<xsl:template match="line">
		<xsl:copy>
			<!--   -->
			<!-- Copies the existing line element -->
			<xsl:apply-templates select="@*|node()" mode="copyChildren" />
			<!--   -->
			<!-- Formats and inserts risk elements as children of line -->
			<xsl:apply-templates select="/Session.getAllDocumentsRs/_submissionDocument/Upload/risk" />
		</xsl:copy>
	</xsl:template>
	<xsl:template match="risk">
		<xsl:copy>
			<xsl:attribute name="id">
				<xsl:value-of select="position()" />
			</xsl:attribute>
			<LocationID>
				<xsl:value-of select="@LocationRef" />
			</LocationID>
			<riskVehicle>
				<exposure>
					<Type>Make</Type>
					<sValue>
						<xsl:value-of select="Manufacturer" />
					</sValue>
				</exposure>
				<exposure>
					<Type>Model</Type>
					<sValue>
						<xsl:value-of select="Model" />
					</sValue>
				</exposure>
				<exposure>
					<Type>Year</Type>
					<iValue>
						<xsl:value-of select="ModelYear" />
					</iValue>
				</exposure>
				<limit>
					<Type>ValueEstimate</Type>
					<iValue>
						<xsl:value-of select="CostNewAmt/Amt" />
					</iValue>
				</limit>
			</riskVehicle>
			<!--   -->
			<!-- Matches on templates found in the Carrier_Submission_RiskMap_CommercialAuto.xsl file -->
			<xsl:apply-templates select="CommlCoverage" />
		</xsl:copy>
	</xsl:template>
	<xsl:template match="@*|node()" mode="copyChildren">
		<xsl:copy-of select="." />
	</xsl:template>
	<xsl:template match="node()">
		<xsl:apply-templates />
	</xsl:template>
	<xsl:include href="Carrier_Submission_RiskMap_CommercialAuto.xsl" />
</xsl:stylesheet>