﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:msxsl="urn:schemas-microsoft-com:xslt"	xmlns:local="#local-functions" >
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

	<xsl:template match="/">
		<DIV class="TechSpec">
			<xsl:apply-templates select="libraryInfo/body"/>
			<xsl:apply-templates select="libraryInfo/params"/>
			<xsl:apply-templates select="libraryInfo/techSpec"/>
			<xsl:apply-templates select="libraryInfo/xslt"/>
		</DIV>
	</xsl:template>

	<xsl:template match="body">
		<H1>
			<xsl:value-of select="h1"/>
		</H1>
	</xsl:template>

	<xsl:template match="params">
		<DIV>
			<P>Parameters:</P>
			<UL>
				<xsl:for-each select="param">
					<xsl:if test="(@type != 'scriptRef') and (@type != 'hidden')">
						<LI>
							<xsl:value-of select="@name"/>
							<xsl:text disable-output-escaping="yes"> - </xsl:text>
							<xsl:value-of select="text()"/>
						</LI>
					</xsl:if>					
				</xsl:for-each>
			</UL>
		</DIV>
	</xsl:template>

	<xsl:template match="techSpec">
		<DIV>
			<P></P>
			<xsl:for-each select ="./node() | ./*">
				<xsl:copy-of select="."/>
			</xsl:for-each>
		</DIV>
	</xsl:template>

	<xsl:template match="xslt">
		<DIV class="xslt">
			<xsl:call-template name="groups"/>
			<xsl:call-template name="fields"/>
			<xsl:call-template name="tables"/>
			<xsl:call-template name="modify"/>
			<xsl:call-template name="todo"/>
			<P>Unknown Items:</P>
			<UL>
				<xsl:for-each select ="xsl:stylesheet/xsl:template[@match='params']/*">
					<!-- List anything this stylesheet does not already know how to render. -->
					<xsl:choose>
						<xsl:when test="name()='ManuScript.addGroupRq'"></xsl:when>
						<xsl:when test="name()='ManuScript.addFieldRq'"></xsl:when>
						<xsl:when test="name()='ManuScript.setFieldDetailRq'"></xsl:when>
						<xsl:when test="name()='Project.addToDoItemRq'"></xsl:when>
						<xsl:when test="name()='ManuScript.modifyItemRq'"></xsl:when>
						<!-- Everything else is unknown at this point. -->
						<xsl:otherwise>
							<LI>
								<xsl:value-of select="name()"/>
							</LI>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</UL>
		</DIV>
	</xsl:template>

	<xsl:template name="groups">
		<DIV class="groups">
			<P>Groups to be added:</P>
			<UL>
				<xsl:apply-templates select="xsl:stylesheet/xsl:template[@match='params']/ManuScript.addGroupRq"/>
			</UL>
		</DIV>
	</xsl:template>

	<xsl:template match="ManuScript.addGroupRq">
		<LI>
			<xsl:value-of select="@groupID"/>
		</LI>
	</xsl:template>

	<xsl:template name="fields">
		<DIV class="fields">
			<P>Fields to be added:</P>
			<UL>
				<xsl:apply-templates select="xsl:stylesheet/xsl:template[@match='params']/ManuScript.addFieldRq[@fieldType!='table']"/>
			</UL>
		</DIV>
	</xsl:template>

	<xsl:template match="ManuScript.addFieldRq[@fieldType!='table']">
		<LI>
			<xsl:value-of select="@fieldID"/>
		</LI>
	</xsl:template>

	<xsl:template name="tables">
		<DIV class="tables">
			<P>Tables to be added:</P>
			<UL>
				<xsl:apply-templates select="xsl:stylesheet/xsl:template[@match='params']/ManuScript.addFieldRq[@fieldType='table']"/>
			</UL>
		</DIV>
	</xsl:template>

	<xsl:template match="ManuScript.addFieldRq[@fieldType='table']">
		<LI>
			<xsl:value-of select="@fieldID"/>
		</LI>
	</xsl:template>

	<xsl:template name="modify">
		<DIV class="modify">
			<P>Items to be modified:</P>
			<UL>
				<xsl:apply-templates select="xsl:stylesheet/xsl:template[@match='params']/ManuScript.modifyItemRq"/>
			</UL>
		</DIV>
	</xsl:template>

	<xsl:template match="ManuScript.modifyItemRq">
		<LI>
			<xsl:value-of select="@fieldID"/>
		</LI>
	</xsl:template>

	<xsl:template name="todo">
		<DIV class="todo">
			<P>To do list:</P>
			<UL>
				<xsl:apply-templates select="xsl:stylesheet/xsl:template[@match='params']/Project.addToDoItemRq"/>
			</UL>
		</DIV>
	</xsl:template>

	<xsl:template match="Project.addToDoItemRq">
		<LI>
			<xsl:value-of select="ToDoItem/Category"/>
			<xsl:text disable-output-escaping="yes"> - </xsl:text>
			<xsl:value-of select="ToDoItem/Description"/>
		</LI>
	</xsl:template>


	<!--	*************************************************************************************************************
		This section contains javascript functions that are used internally to perform this XSLT
		************************************************************************************************************* -->

	<msxsl:script implements-prefix="local"  language='CSharp'>
		<![CDATA[
  
    public string replaceName(string sValue)
    {
        sValue = sValue.Replace("DuckCreekTech", "ISO");
        sValue = sValue.Replace("_"," ");
        sValue = CaptionFromName(sValue);
        sValue = sValue.Replace("Cov ","");
        sValue = sValue.Replace(" Output","");
        sValue = sValue.Replace(" Output ","");
        sValue = sValue.Replace(" Input","");
        sValue = sValue.Replace(" Input ","");
        sValue = sValue.Replace("   "," ");
        sValue = sValue.Replace("  "," ");
        sValue = sValue.Trim();
        return sValue;
    }
    
		public string CaptionFromName(string sName)
		{
			string result = null;
			int I;
			result = string.Empty;
			for (I = 0; I < sName.Length; I++)
			{
				// if not the first or last character and UpperCase or Numeric, insert space
				if ((I > 0) && (I < (sName.Length - 1)) && (char.IsUpper(sName[I]) || char.IsNumber(sName[I])))
				{
					// if either the previous or next char is lowerCase
					if (char.IsLower(sName[I - 1]) || char.IsLower(sName[I + 1]))
					{
						result = result + ' ';
					}
				}
				else
				{
					// if last character is UpperCase or Numeric, insert space
					if ((I > 0) && (I == (sName.Length - 1)) && (char.IsUpper(sName[I]) || char.IsNumber(sName[I])))
					{
						// if previous is LowerCase, insert space
						if (char.IsLower(sName[I - 1]))
						{
							result = result + ' ';
						}
					}
				}
				//if (sName[I] != '.')
				//{
					result = result + sName[I];
				//}
				//else
				//{
					// if period separator, check if previos was lower
				//	if ((I < (sName.Length - 1)) && (char.IsLower(sName[I + 1])))
				//	{
				//		result = result + ' ';
				//	}
				//}
			}
			return result;
		}
    
  
   ]]>
	</msxsl:script>

</xsl:stylesheet>
