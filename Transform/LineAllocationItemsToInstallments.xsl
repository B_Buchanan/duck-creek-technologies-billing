﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1">
	<xsl:output omit-xml-declaration="yes" />

	<xsl:template match="AllInvoiceLineAllocations">
		<xsl:element name="Installments">
			<xsl:for-each select="InvoiceLineAllocations">
				<xsl:apply-templates select="." mode="Singular" />
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template match="InvoiceLineAllocations">
		<xsl:element name="Installments">
			<xsl:apply-templates select="." mode="Singular" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="InvoiceLineAllocations" mode="Singular">
		<xsl:element name="Installment">
			<xsl:variable name="ItemBalance" select="sum(InvoiceLineAllocation/@Balance)" />
			<xsl:variable name="ItemAmount" select="sum(InvoiceLineAllocation/@Amt)" />

			<xsl:attribute name="DueDate">
				<xsl:value-of select="InvoiceLineAllocation[1]/@DueDate" />
			</xsl:attribute>
			<xsl:attribute name="InstallmentDate">
				<xsl:value-of select="InvoiceLineAllocation[1]/@InstDate" />
			</xsl:attribute>
			<xsl:attribute name="InstallmentNumber" />
			<xsl:attribute name="ItemBalance">
				<xsl:value-of select="$ItemBalance" />
			</xsl:attribute>
			<xsl:attribute name="ItemClosedToCashAmount">
				<xsl:value-of select="sum(InvoiceLineAllocation/@Cash)" />
			</xsl:attribute>
			<xsl:attribute name="ItemClosedToCreditAmount">
				<xsl:value-of select="sum(InvoiceLineAllocation/@Credit)" />
			</xsl:attribute>
			<xsl:attribute name="ItemRedistributedAmount">
				<xsl:text><![CDATA[0]]></xsl:text>
			</xsl:attribute>
			<xsl:attribute name="ItemScheduleAmount">
				<xsl:choose>
					<xsl:when test="$ItemAmount != 0">
						<xsl:value-of select="$ItemAmount" />
					</xsl:when>
					<xsl:when test="$ItemBalance != 0">
						<xsl:value-of select="$ItemBalance" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:text><![CDATA[0]]></xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ItemWrittenOffAmount">
				<xsl:value-of select="sum(InvoiceLineAllocation/@WO)" />
			</xsl:attribute>
			<xsl:element name="InstallmentItems">
				<xsl:for-each select="InvoiceLineAllocation">
					<xsl:element name="InstallmentItem">
						<xsl:attribute name="AggregationReference">
							<xsl:value-of select="@AggRef" />
						</xsl:attribute>
						<xsl:attribute name="AllocationPriority">
							<xsl:value-of select="@AllocationPriority" />
						</xsl:attribute>
						<xsl:attribute name="CoverageReference">
							<xsl:value-of select="@CovRef" />
						</xsl:attribute>
						<xsl:attribute name="CurrencyCulture" />
						<xsl:attribute name="DueDate">
							<xsl:value-of select="@DueDate" />
						</xsl:attribute>
						<xsl:attribute name="FirstInvoiceId" />
						<xsl:attribute name="InstallmentDate">
							<xsl:value-of select="@InstDate" />
						</xsl:attribute>
						<xsl:attribute name="InstallmentTypeCode">
							<xsl:value-of select="@Type" />
						</xsl:attribute>
						<xsl:attribute name="ItemBalance">
							<xsl:value-of select="@Balance" />
						</xsl:attribute>
						<xsl:attribute name="ItemClosedIndicator">
							<xsl:choose>
								<xsl:when test="@Balance = 0">
									<xsl:text><![CDATA[Y]]></xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text><![CDATA[N]]></xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="ItemClosedToCashAmount">
							<xsl:value-of select="@Cash" />
						</xsl:attribute>
						<xsl:attribute name="ItemClosedToCreditAmout">
							<xsl:value-of select="@Credit" />
						</xsl:attribute>
						<xsl:attribute name="ItemId">
							<xsl:value-of select="@ItemId" />
						</xsl:attribute>
						<xsl:attribute name="ItemRedistributedAmount">
							<xsl:text><![CDATA[0]]></xsl:text>
						</xsl:attribute>
						<xsl:attribute name="ItemScheduleAmount">
							<xsl:choose>
								<xsl:when test="@Amt != 0">
									<xsl:value-of select="@Amt" />
								</xsl:when>
								<xsl:when test="@Balance != 0">
									<xsl:value-of select="@Balance" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:text><![CDATA[0]]></xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="ItemScheduleId" />
						<xsl:attribute name="ItemWrittenOfAmount">
							<xsl:value-of select="@WO" />
						</xsl:attribute>
						<xsl:attribute name="PolicyTermId">
							<xsl:value-of select="@PolTermId" />
						</xsl:attribute>
						<xsl:attribute name="ReceivableSubTypeCode">
							<xsl:value-of select="@RecSubType" />
						</xsl:attribute>
						<xsl:attribute name="ReceivableTypeCode">
							<xsl:value-of select="@RecType" />
						</xsl:attribute>
						<xsl:attribute name="TransactionTypeCode">
							<xsl:value-of select="@TranType" />
						</xsl:attribute>
						<xsl:attribute name="UnitReference">
							<xsl:value-of select="@UnitRef" />
						</xsl:attribute>
					</xsl:element>
				</xsl:for-each>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
