﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	
	<xsl:template match="session">
		<xsl:call-template name="WrapSessionData" />
	</xsl:template>

	<xsl:template name="WrapSessionData">
		<DownloadDocument>
			<xsl:copy-of select="." />
		</DownloadDocument>
	</xsl:template>
	
</xsl:stylesheet>