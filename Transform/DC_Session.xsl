﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:dc="DuckCreek.XSL">
	<xsl:template match="session">
		<xsl:call-template name="dc:Session.Body"/>
	</xsl:template>
	<xsl:template name="dc:Session.Body">
		<DC_Session ExampleQuoteId="{@id}">
			<UserName><xsl:value-of select="properties/userName"/></UserName>
			<CreateDateTime/>
			<Purpose>Onset</Purpose>
			<xsl:call-template name="dc:Session.Extensions"/>
			<xsl:call-template name="dc:Session.Relationships"/>
		</DC_Session>
	</xsl:template>
	<xsl:template name="dc:Session.Extensions"/>
	<xsl:template name="dc:Session.Relationships"/>
</xsl:stylesheet>

