﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"  xmlns:LockBoxRecord="urn:LockBoxRecord">
	<xsl:output method="xml" indent="yes"/>

	<xsl:template match="@* | node()">
		<BillingCashManagement.processAddBatchPaymentRq>
			<PaymentBatchEntryRecord>
				<PaymentBatchEntryId/>
				<AccountId>
				</AccountId>
				<!--<xsl:value-of select="LockBoxRecord:getAccountNumber()"/>-->
				<AccountReference>
					<xsl:value-of select="LockBoxRecord:getSubString(2,14)"/>
				</AccountReference>
				<!--<xsl:value-of select="LockBoxRecord:getPolicyNumber()"/>-->
				<PolicyReference>
					<xsl:value-of select="LockBoxRecord:getSubString(16,14)"/>
				</PolicyReference>
				<PaymentBatchId>
					<xsl:value-of select="LockBoxRecord:getBatchId()"/>
				</PaymentBatchId>
				<PaymentReference>
					<xsl:value-of select="LockBoxRecord:getTrimmedValue(55,32)"/>
				</PaymentReference>
				<InvoiceReference>
					<xsl:value-of select="LockBoxRecord:getTrimmedValue(87,32)"/>
				</InvoiceReference>
				<PaymentBatchEntryReference>
					<xsl:value-of select="LockBoxRecord:getTrimmedValue(119,55)"/>		  
				</PaymentBatchEntryReference>
				<PaymentAction>
					<xsl:value-of select="LockBoxRecord:getPaymentAction()"/>		  
				</PaymentAction>
				<!--<xsl:value-of select="LockBoxRecord:getAmount()"/>-->
				<Amount>
					<xsl:value-of select="LockBoxRecord:getSubStringDecimal(36,10)"/>
				</Amount>
				<EntryUserId>Admin</EntryUserId>
				<CurrencyCulture>
					<xsl:value-of select="LockBoxRecord:getCurrencyCulture(179,5)"/>
				</CurrencyCulture>
				<PaymentMethodCode>
					<xsl:value-of select="LockBoxRecord:getTrimmedValue(51,4)"/>	
				</PaymentMethodCode>
				<AllocationClassCode>P</AllocationClassCode>
				<PostMarkDate/>
				<EntryDate>
					<xsl:value-of select="LockBoxRecord:getProcessedDate()"/>
				</EntryDate>
				<PaymentBatchEntryLockingTS>
				</PaymentBatchEntryLockingTS>
				<ProcessedTimestamp>
				</ProcessedTimestamp>
				<xsl:call-template name="processPaymentMethodDetails"/>				
				<UnidentifiedRecord>
					<UnidentifiedAccountIdentifier>
					</UnidentifiedAccountIdentifier>
					<UnidentifiedPolicyIdentifier>
					</UnidentifiedPolicyIdentifier>
					<UnidentifiedOtherIdentifier>
					</UnidentifiedOtherIdentifier>
					<UnidentifiedLastName>
					</UnidentifiedLastName>
					<UnidentifiedFirstName>
					</UnidentifiedFirstName>
					<UnidentifiedMiddleName>
					</UnidentifiedMiddleName>
				</UnidentifiedRecord>
			</PaymentBatchEntryRecord>
		</BillingCashManagement.processAddBatchPaymentRq>
	</xsl:template>
	<xsl:template name="processPaymentMethodDetails">
		<xsl:variable name="paymentMethod" select="LockBoxRecord:getTrimmedValue(51,4)"/>
		<xsl:variable name="ProviderTemplate" select="LockBoxRecord:getTrimmedValue(184,8)"/>		
		<xsl:choose>
			<xsl:when test="$paymentMethod = 'CK'">
				<xsl:call-template name="Check"/>
			</xsl:when>
			<xsl:when test="$paymentMethod = 'CS'">
				<xsl:call-template name="Cash"/>
			</xsl:when>
			<!--xsl:when test="$paymentMethod = '1XCC'and $ProviderTemplate = '6'"-->
			<xsl:when test="$paymentMethod = '1XCC'">
				<xsl:call-template name="CardSample"/>
			</xsl:when>			
			<!--xsl:when test="$paymentMethod = '1XDC' and $ProviderTemplate = '6'"-->
			<xsl:when test="$paymentMethod = '1XDC'">
				<xsl:call-template name="CardSample"/>
			</xsl:when>			
			<xsl:when test="$paymentMethod = '1EFT'">
				<xsl:call-template name="eCheck"/>
			</xsl:when>
			<xsl:otherwise>
				<PaymentMethodDetail/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="Check">		
		<PaymentMethodDetail>
			<ReportingGroupCode>1</ReportingGroupCode>
			<CurrencyCulture>
				<xsl:value-of select="LockBoxRecord:getCurrencyCulture(179,5)"/>
			</CurrencyCulture>			
			<Check>
				<CheckNumber>
					<xsl:value-of select="LockBoxRecord:getTrimmedValue(175,10)"/>
				</CheckNumber>
				<NameOnCheck>
					<xsl:value-of select="LockBoxRecord:getTrimmedValue(175,10)"/>
				</NameOnCheck>
			</Check>
		</PaymentMethodDetail>
		<ReversalReasonCode>
			<xsl:value-of select="LockBoxRecord:getTrimmedValue(185,4)"/>
		</ReversalReasonCode>
	</xsl:template>
	<xsl:template name="Cash">		
		<PaymentMethodDetail>
			<CurrencyCulture>
				<xsl:value-of select="LockBoxRecord:getCurrencyCulture(179,5)"/>
			</CurrencyCulture>
			<ReportingGroupCode>1</ReportingGroupCode>
			<Cash>
			</Cash>
		</PaymentMethodDetail>
		<ReversalReasonCode>
			<xsl:value-of select="LockBoxRecord:getTrimmedValue(175,4)"/>
		</ReversalReasonCode>
	</xsl:template>
	<xsl:template name="CardSample">
		<PaymentMethodDetail>
			<CardPaymentDetails>
				<ReportingGroupCode>
					<xsl:value-of select="LockBoxRecord:getReportingGroupCode(175,4)"/>
				</ReportingGroupCode>
				<DrawDate>
					<xsl:value-of select="LockBoxRecord:getDateValue(192,8)"/>
				</DrawDate>	
				<CurrencyCulture>
					<xsl:value-of select="LockBoxRecord:getCurrencyCulture(179,5)"/>
				</CurrencyCulture>
				<Token>
					<PaymentProfileID>
						<xsl:value-of select="LockBoxRecord:getTrimmedValue(200,36)"/>
					</PaymentProfileID>
					<ProfileID>
						<xsl:value-of select="LockBoxRecord:getTrimmedValue(236,36)"/>
					</ProfileID>
				</Token>
				<CardTransactionDetails>
					<AuthorizationCode>
						<xsl:value-of select="LockBoxRecord:getTrimmedValue(272,10)"/>
					</AuthorizationCode>
					<CardPaymentStatus>C</CardPaymentStatus>
				</CardTransactionDetails>
				<CardNumber>
					<xsl:value-of select="LockBoxRecord:getTrimmedValue(282,20)"/>
				</CardNumber>
				<CardType>
					<xsl:value-of select="LockBoxRecord:getTrimmedValue(302,4)"/>
				</CardType>
				<ExpirationDate>
					<xsl:value-of select="LockBoxRecord:getTrimmedValue(306,8)"/>
				</ExpirationDate>
				<PaymentMethod>
					<xsl:value-of select="LockBoxRecord:getTrimmedValue(314,6)"/>
				</PaymentMethod>
			</CardPaymentDetails>
		</PaymentMethodDetail>
		<ReversalReasonCode>
			<xsl:value-of select="LockBoxRecord:getTrimmedValue(320,4)"/>
		</ReversalReasonCode>
	</xsl:template>
	<xsl:template name="eCheck">
		<PaymentMethodDetail>
			<EFTPaymentDetails>	
				<ReportingGroupCode>
					<xsl:value-of select="LockBoxRecord:getReportingGroupCode(175,4)"/>
				</ReportingGroupCode>
				<DrawDate>
					<xsl:value-of select="LockBoxRecord:getDateValue(192,8)"/>
				</DrawDate>
				<CurrencyCulture>
					<xsl:value-of select="LockBoxRecord:getCurrencyCulture(179,5)"/>
				</CurrencyCulture>
				<AuthorizationType>
					<xsl:value-of select="LockBoxRecord:getTrimmedValue(200,3)"/>
				</AuthorizationType>	
				<AccountNumber>
					<xsl:value-of select="LockBoxRecord:getTrimmedValue(203,20)"/>
				</AccountNumber>	
				<AccountType>
					<xsl:value-of select="LockBoxRecord:getTrimmedValue(223,1)"/>
				</AccountType>	
				<BankRoutingNumber>
					<xsl:value-of select="LockBoxRecord:getTrimmedValue(224,10)"/>
				</BankRoutingNumber>	
				<AccountName>
					<xsl:value-of select="LockBoxRecord:getTrimmedValue(234,50)"/>
				</AccountName>	
				<BankName>
					<xsl:value-of select="LockBoxRecord:getTrimmedValue(284,50)"/>
				</BankName>	
				<FirstName />	
				<LastName />	
			</EFTPaymentDetails>
		</PaymentMethodDetail>
		<ReversalReasonCode>
			<xsl:value-of select="LockBoxRecord:getTrimmedValue(334,4)"/>
		</ReversalReasonCode>
	</xsl:template>
</xsl:stylesheet>
