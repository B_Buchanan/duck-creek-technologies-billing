﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes"/>

	<xsl:template match="root">
		<xsl:apply-templates select="session/data/policy"/>
	</xsl:template>
	
	<xsl:template match="policy">
		<xsl:call-template name="BuildItemList"/>
	</xsl:template>
	
	<xsl:template name="BuildItemList">
		<xsl:variable name="lastTranId">
			<xsl:value-of select="/root/session/data/policyAdmin/transactions/transaction[last()]/@id"/>
		</xsl:variable>
		<BillItems>
			<xsl:for-each select="/root/_processingData/transactions/transaction[@id=$lastTranId]">
				<xsl:call-template name="BillItem"/>
			</xsl:for-each>
			<xsl:call-template name="TaxesSurcharges">
				<xsl:with-param name="lastTranId" select="$lastTranId" />
			</xsl:call-template>
		</BillItems>
	</xsl:template>

	<xsl:template name="BillItem">
		<BillItem>
			<TransactionTypeCode>
				<xsl:call-template name="LookupTransactionTypeCode">
					<xsl:with-param name="code">
						<xsl:value-of select="Type"/>
					</xsl:with-param>
				</xsl:call-template>
			</TransactionTypeCode>
			<TransactionDate><xsl:value-of select="CreatedDate" /></TransactionDate>
			<xsl:choose>
				<xsl:when test="TypeCaption='New Business' and Status='Pending' and Type='New'">
					<ItemEffectiveDate>
						<xsl:value-of select="/root/session/data/policy/EffectiveDate" />
					</ItemEffectiveDate>
				</xsl:when>
				<xsl:otherwise>
					<ItemEffectiveDate>
						<xsl:value-of select="EffectiveDate" />
					</ItemEffectiveDate>
				</xsl:otherwise>
			</xsl:choose>
			
            <ItemExpirationDate><xsl:value-of select="ExpirationDate" /></ItemExpirationDate>
            <BillInFullIndicator>N</BillInFullIndicator>
			<ReceivableTypeCode>PREM</ReceivableTypeCode>
			<ReceivableSubTypeCode/>
            <ItemAmount><xsl:value-of select="/root/session/data/policy/PurePremium/@change" /></ItemAmount>
            <ItemCommissionAmount><xsl:value-of select="/root/session/data/policy/CommissionAmount" /></ItemCommissionAmount>
		</BillItem>
	</xsl:template>
	
	<xsl:template name="TaxesSurcharges">
		<xsl:param name="lastTranId">
			<xsl:value-of select="/root/session/data/policyAdmin/transactions/transaction[last()]/@id"/>
		</xsl:param>
		
		<xsl:if test="TaxesSurcharges[@change != '0']">
			<xsl:variable name="lastTransaction" select="/root/_processingData/transactions/transaction[@id=$lastTranId]"/>
			<BillItem>
				<TransactionTypeCode>
					<xsl:call-template name="LookupTransactionTypeCode">
						<xsl:with-param name="code">
							<xsl:value-of select="$lastTransaction/Type"/>
						</xsl:with-param>
					</xsl:call-template>
				</TransactionTypeCode>
				<TransactionDate>
					<xsl:value-of select="$lastTransaction/CreatedDate"/>
				</TransactionDate>
				
				<xsl:choose>
					<xsl:when test="$lastTransaction/TypeCaption='New Business' and $lastTransaction/Status='Pending' and $lastTransaction/Type='New'">
						<ItemEffectiveDate>
							<xsl:value-of select="/root/session/data/policy/EffectiveDate"/>
						</ItemEffectiveDate>
					</xsl:when>
					<xsl:otherwise>
						<ItemEffectiveDate>
							<xsl:value-of select="$lastTransaction/EffectiveDate"/>
						</ItemEffectiveDate>
						
					</xsl:otherwise>
					</xsl:choose>
				<ItemExpirationDate>
					<xsl:value-of select="$lastTransaction/ExpirationDate"/>
				</ItemExpirationDate>
				<BillInFullIndicator>N</BillInFullIndicator>
				<ReceivableTypeCode>TAX</ReceivableTypeCode>
				<ReceivableSubTypeCode>
					<xsl:call-template name="LookupReceivableSubTypeCode">
						<xsl:with-param name="code">
							<xsl:value-of select="Type"/>
						</xsl:with-param>
					</xsl:call-template>
				</ReceivableSubTypeCode>
				<ItemAmount><xsl:value-of select="TaxesSurcharges/@change" /></ItemAmount>
				<ItemCommissionAmount>0</ItemCommissionAmount>
			</BillItem>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="LookupTransactionTypeCode">
		<xsl:param name="code"/>
		<xsl:choose>
			<xsl:when test="$code = 'New'">NBUS</xsl:when>
			<xsl:when test="$code = 'Endorse'">ENDT</xsl:when>
			<xsl:when test="$code = 'Cancel'">PCAN</xsl:when>
			<xsl:when test="$code = 'FlatCancel'">FCAN</xsl:when>
			<xsl:when test="$code = 'Renew'">RNEW</xsl:when>
			<xsl:when test="$code = 'Reissue'">REIS</xsl:when>
			<xsl:when test="$code = 'Rewrite'">RWRI</xsl:when>
			<xsl:when test="$code = 'PaidInFull' or $code = 'PIF'">PIF</xsl:when>
			<xsl:otherwise>UKWN</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="LookupReceivableSubTypeCode">
		<xsl:param name="code"/>
		<xsl:choose>
			<xsl:when test="$code = 'PolicyFee'">POL</xsl:when>
			<xsl:when test="$code = 'ReinstatementFee'">REIN</xsl:when>
			<xsl:when test="$code = 'FireInsuranceFee'">FIRE</xsl:when>
			<xsl:when test="$code = 'InstallmentPlanFee'">INST</xsl:when>
			<xsl:when test="$code = 'CancellationFee'">CANC</xsl:when>
			<xsl:otherwise/>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
