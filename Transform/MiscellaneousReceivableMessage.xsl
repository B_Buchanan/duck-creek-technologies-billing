﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt">

	<xsl:output omit-xml-declaration="yes"/>

	<xsl:template match="/Billing.addMiscellaneousBillingItemRq">
		<PolicyData>
			<AccountId>
				<xsl:value-of select="AccountId"/>
			</AccountId>
			<PolicyTermId>
				<xsl:value-of select="PolicyTerm/PolicyTermId"/>
			</PolicyTermId>
			<PolicyLineOfBusinessCode>
				<xsl:value-of select="PolicyTerm/PolicyLineOfBusinessCode"/>
			</PolicyLineOfBusinessCode>
			<PolicyProductCode>
				<xsl:value-of select="PolicyTerm/PolicyProductCode"/>
			</PolicyProductCode>
			<PolicyIssueSystemCode>
				<xsl:value-of select="PolicyTerm/PolicyIssueSystemCode"/>
			</PolicyIssueSystemCode>
			<PolicyMasterCompanyCode>
				<xsl:value-of select="PolicyTerm/PolicyMasterCompanyCode"/>
			</PolicyMasterCompanyCode>
			<PolicyReference>
				<xsl:value-of select="PolicyTerm/PolicyReference"/>
			</PolicyReference>
			<ReasonTypeCode />
			<Auditable>0</Auditable>
			<TransactionEffectiveDate>
				<xsl:value-of select="ReceivableEffectiveDate"/>
			</TransactionEffectiveDate>
			<PolicyTermEffectiveDate>
				<xsl:value-of select="PolicyTerm/PolicyTermEffectiveDate"/>
			</PolicyTermEffectiveDate>
			<PolicyTermExpirationDate>
				<xsl:value-of select="PolicyTerm/PolicyTermExpirationDate"/>
			</PolicyTermExpirationDate>
			<TransactionTypeCode>
				<xsl:value-of select="TransactionTypeCode"/>
			</TransactionTypeCode>
			<AmountItems>
				<AmountItem>
					<CurrentTermTrans>1</CurrentTermTrans>
					<TransactionTypeCode>
						<xsl:value-of select="TransactionTypeCode"/>
					</TransactionTypeCode>
					<ReasonTypeCode />
					<ItemEffectiveDate>
						<xsl:value-of select="ReceivableEffectiveDate"/>
					</ItemEffectiveDate>
					<ItemExpirationDate>
						<xsl:value-of select="ReceivableExpirationDate"/>
					</ItemExpirationDate>
					<ReceivableLineOfBusinessCode>
						<xsl:value-of select="ReceivableLineOfBusinessCode"/>
					</ReceivableLineOfBusinessCode>
					<CoverageReference>
						<xsl:choose>
							<xsl:when test ="CoverageReference != ''">
								<xsl:value-of select="CoverageReference"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="PolicyTerm/PolicyLineOfBusinessCode"/>
							</xsl:otherwise>
						</xsl:choose>
					</CoverageReference>
					<ReceivableTypeCode>
						<xsl:value-of select="ReceivableTypeCode"/>
					</ReceivableTypeCode>
					<ReceivableSubTypeCode>
						<xsl:value-of select="ReceivableSubTypeCode"/>
					</ReceivableSubTypeCode>
					<ItemAmount>
						<xsl:value-of select="Amount"/>
					</ItemAmount>
					<AggregationReference>
						<xsl:value-of select="AggregationReference"/>
					</AggregationReference>
					<CommissionGroupReference />
					<ItemCommissionAmount />
					<ItemCommissionPercent />
					<ItemCommissionPlanId>
						<xsl:value-of select="CommissionPlanId"/>
					</ItemCommissionPlanId>
					<ItemCommissionType>
						<xsl:value-of select="RateTypeCode"/>
					</ItemCommissionType>
					<UnitReference>
						<xsl:value-of select="UnitReference"/>
					</UnitReference>
					<CallingProcess>UPTM</CallingProcess>
					<BillInFull>
						<xsl:value-of select="BillInFull" />
					</BillInFull>
					<MiscTypeCode>
						<xsl:value-of select="MiscTypeCode"/>
					</MiscTypeCode>
				</AmountItem>
			</AmountItems>
			<CurrencyCulture>
				<xsl:value-of select="CurrencyCulture"/>
			</CurrencyCulture>
			<BillItemExtendedData/>
		</PolicyData>
	</xsl:template>
</xsl:stylesheet>
