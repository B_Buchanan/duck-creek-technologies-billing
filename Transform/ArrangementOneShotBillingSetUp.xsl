﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt">

	<xsl:output omit-xml-declaration="yes"/>

	<xsl:template match="/*">
		<Billing.setupRq>
			<OneShotBillingSetUp>
				<PartySetup>
					<Assign name="InsuredParty">
						<xsl:attribute name="partyId">
							<xsl:value-of select="PayorId"></xsl:value-of>
						</xsl:attribute>
					</Assign>
					<Assign name="PayorParty">
						<xsl:attribute name="partyId">
							<xsl:value-of select="PayorId"></xsl:value-of>
						</xsl:attribute>
					</Assign>
					<Assign name="PayeeParty">
						<xsl:attribute name="partyId">
							<xsl:value-of select="PayeeId"></xsl:value-of>
						</xsl:attribute>
					</Assign>
					<LookupAgent name="AgentParty">
						<AgencyId>
							<xsl:value-of select="AgencyId"></xsl:value-of>
						</AgencyId>
					</LookupAgent>
				</PartySetup>
				<AccountSetup>
					<Use>
						<xsl:attribute name="AccountId">
							<xsl:value-of select="AccountId"></xsl:value-of>
						</xsl:attribute>
					</Use>
				</AccountSetup>
				<PolicyTermSetup>
					<LookupConfig>
						<PolicyPlanCode>
							<xsl:value-of select="PlanCode"></xsl:value-of>
						</PolicyPlanCode>
						<BillClass/>
						<PolicyLOB/>
						<PolicyLOBCode>
							<xsl:value-of select="LineOfBusinessCode"></xsl:value-of>
						</PolicyLOBCode>
						<PolicyTermEffectiveDate>
							<xsl:value-of select="EffectiveDate"></xsl:value-of>
						</PolicyTermEffectiveDate>
						<UseMPR>0</UseMPR>
					</LookupConfig>
					<PolicyTermRecord>
						<PolicyTermEffectiveDate>
							<xsl:value-of select="EffectiveDate"></xsl:value-of>
						</PolicyTermEffectiveDate>
						<PolicyTermExpirationDate>
							<xsl:value-of select="ExpirationDate"></xsl:value-of>
						</PolicyTermExpirationDate>
						<PolicyReference>
							<xsl:value-of select="ArrangementReference/Prefix"></xsl:value-of>
							<xsl:value-of select="ArrangementReference/Sequential"></xsl:value-of>
						</PolicyReference>
						<SplitBillIndicator>N</SplitBillIndicator>
						<PolicyTermStatusCode>ACV</PolicyTermStatusCode>
						<PolicyIssueSystemCode>
							<xsl:choose>
								<xsl:when test="normalize-space(IssueSystemCode) != ''">
									<xsl:value-of select="IssueSystemCode"></xsl:value-of>		
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>DCT</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</PolicyIssueSystemCode>
						<PolicyMasterCompanyCode>
							<xsl:choose>
								<xsl:when test="normalize-space(MasterCompanyCode) != ''">
									<xsl:value-of select="MasterCompanyCode"></xsl:value-of>		
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>DCT</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</PolicyMasterCompanyCode>
						<PolicyLineOfBusinessCode>
							<xsl:value-of select="LineOfBusinessCode"></xsl:value-of>
						</PolicyLineOfBusinessCode>
						<PolicyProductCode>
							<xsl:value-of select="ProductCode"></xsl:value-of>
						</PolicyProductCode>
						<PolicyStateCode>
							<xsl:value-of select="StateCode"></xsl:value-of>
						</PolicyStateCode>
						<ProcessingOrgUnitCode>
							<xsl:choose>
								<xsl:when test="normalize-space(ProcessingOrgUnitCode) != ''">
									<xsl:value-of select="ProcessingOrgUnitCode"></xsl:value-of>		
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>1</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</ProcessingOrgUnitCode>
						<TransactionTypeCode>
							<xsl:value-of select="TransactionTypeCode"></xsl:value-of>
						</TransactionTypeCode>
						<PolicyTermTypeCode>ARRG</PolicyTermTypeCode>
					</PolicyTermRecord>
				</PolicyTermSetup>
			</OneShotBillingSetUp>
		</Billing.setupRq>
	</xsl:template>
</xsl:stylesheet>