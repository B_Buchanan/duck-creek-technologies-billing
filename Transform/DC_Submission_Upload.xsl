﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:template match="ACORD">
		<Upload>
			<Agent>
				<Name>
					<xsl:value-of select="SignonRq/SignonPswd/CustId/CustLoginId" />
				</Name>
			</Agent>
			<RqUID>
				<xsl:value-of select="InsuranceSvcRq/RqUID"/>			
			</RqUID>
			<Producer>
				<Agency>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/Producer/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>
				</Agency>
				<AgencyNumber>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/Producer/ProducerInfo/NIPRId"/>
				</AgencyNumber>
				<LastName>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/Producer/GeneralPartyInfo/NameInfo[@id='ProducerName']/PersonName/Surname"/>
				</LastName>
				<FirstName>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/Producer/GeneralPartyInfo/NameInfo[@id='ProducerName']/PersonName/GivenName"/>
				</FirstName>
				<Prefix>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/Producer/GeneralPartyInfo/NameInfo[@id='ProducerName']/PersonName/TitlePrefix"/>
				</Prefix>
				<PhoneNumber>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/Producer/GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Phone']/PhoneNumber"/>
				</PhoneNumber>
				<FaxNumber>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/Producer/GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Fax']/PhoneNumber"/>
				</FaxNumber>
				<CellNumber>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/Producer/GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Cell']/PhoneNumber"/>
				</CellNumber>
				<Email>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/Producer/GeneralPartyInfo/Communications/EmailInfo/EmailAddr"/>
				</Email>
			</Producer>
			<Organization>
				<LegalEntity>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/LegalEntityCd"/>
				</LegalEntity>
				<FEIN>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity[TaxIdTypeCd='FEIN']/TaxId"/>
				</FEIN>
				<SSN>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity[TaxIdTypeCd='SSN']/TaxId"/>
				</SSN>
				<Name>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>
				</Name>
				<LocationAddressLine1>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr1"/>
				</LocationAddressLine1>
				<LocationAddressLine2>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr2"/>
				</LocationAddressLine2>
				<LocationAddressLine3>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr3"/>
				</LocationAddressLine3>
				<LocationAddressLine4>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr4"/>
				</LocationAddressLine4>
				<LocationCity>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Addr/City"/>
				</LocationCity>
				<LocationStateCode>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProvCd"/>
				</LocationStateCode>
				<LocationPostalCode>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Addr/PostalCode"/>
				</LocationPostalCode>
				<LocationCounty>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Addr/County"/>
				</LocationCounty>
				<PhoneNumber>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Phone']/PhoneNumber"/>
				</PhoneNumber>
				<FaxNumber>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Fax']/PhoneNumber"/>
				</FaxNumber>
				<CellNumber>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Cell']/PhoneNumber"/>
				</CellNumber>
				<Email>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Communications/EmailInfo/EmailAddr"/>
				</Email>
				<WebSite>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Communications/WebsiteInfo/WebsiteURL"/>
				</WebSite>
			</Organization>
			<PartyEmail>
				<EmailAddress>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Communications/EmailInfo/EmailAddr"/>
				</EmailAddress>
			</PartyEmail>
			<PolicyData>
				<LOB>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/CommlPolicy/LOBCd"/>
				</LOB>
				<EffectiveDate>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/CommlPolicy/ContractTerm/EffectiveDt"/>
				</EffectiveDate>
				<ExpirationDate>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/CommlPolicy/ContractTerm/ExpirationDt"/>
				</ExpirationDate>
				<BillingMethod>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/CommlPolicy/BillingMethodCd"/>
				</BillingMethod>
				<PaymentMethod>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/CommlPolicy/PaymentOption/PaymentPlanCd"/>
				</PaymentMethod>
				<SIC>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/InsuredOrPrincipalInfo/BusinessInfo/SICCd"/>
				</SIC>
				<NAICS>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/InsuredOrPrincipalInfo/BusinessInfo/NAICSCd"/>
				</NAICS>
			</PolicyData>
			<xsl:for-each select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/Location">
				<Location>
					<LocationId>
						<xsl:value-of select="@id"/>
					</LocationId>
					<LocationNumber>
						<xsl:value-of select="ItemIdInfo/AgencyId"/>
					</LocationNumber>
					<LocationAddressLine1>
						<xsl:value-of select="Addr/Addr1"/>
					</LocationAddressLine1>
					<LocationAddressLine2>
						<xsl:value-of select="Addr/Addr2"/>
					</LocationAddressLine2>
					<LocationAddressLine3>
						<xsl:value-of select="Addr/Addr3"/>
					</LocationAddressLine3>
					<LocationAddressLine4>
						<xsl:value-of select="Addr/Addr4"/>
					</LocationAddressLine4>
					<LocationCity>
						<xsl:value-of select="Addr/City"/>
					</LocationCity>
					<LocationStateCode>
						<xsl:value-of select="Addr/StateProvCd"/>
					</LocationStateCode>
					<LocationPostalCode>
						<xsl:value-of select="Addr/PostalCode"/>
					</LocationPostalCode>
					<LocationCounty>
						<xsl:value-of select="Addr/County"/>
					</LocationCounty>
					<LocationCountryCode>
						<xsl:value-of select="Addr/CountryCd"/>
					</LocationCountryCode>
					<LocationCountry>
						<xsl:value-of select="Addr/Country"/>
					</LocationCountry>
				</Location>
			</xsl:for-each>
			<xsl:for-each select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/CommlAutoLineBusiness/CommlRateState/CommlVeh">
				<risk>
					<xsl:apply-templates select="@*|node()"/>
				</risk>
			</xsl:for-each>
		</Upload>
	</xsl:template>
	<!--  Matches on ASW formatted payloads -->
	<xsl:template match="Upload">
		<xsl:copy-of select="." />
	</xsl:template>
	<xsl:template match="node()|@*">
			<xsl:apply-templates select="@*|node()" />
	</xsl:template>
	<xsl:include href="Carrier_Submission_Upload.xsl"/>
</xsl:stylesheet>