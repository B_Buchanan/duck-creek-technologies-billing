﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<!--  
	This stylesheet is included in the DC_Submission_RiskMap_CommercialAuto.xsl and can be used to make customizations.
	Add templates that match on CommlCoverage to map risk coverages (e.g. <xsl:template match="CommlCoverage[CoverageCd='COLL']" />
	-->
</xsl:stylesheet>