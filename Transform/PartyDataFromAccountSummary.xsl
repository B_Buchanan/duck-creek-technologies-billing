﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes"/>
		
	<xsl:template match="/">
		<xsl:call-template name="AccountPartyData" />
	</xsl:template>
	
	<xsl:template name="AccountPartyData">
		<AccountPartyData>
			<xsl:for-each select="/BillingObj.getAccountSummaryRs/Account/AssociatedParties/Party">
				<xsl:call-template name="PartyRow"/>
			</xsl:for-each>
		</AccountPartyData>
	</xsl:template>
	
	<xsl:template name="PartyRow">
		<xsl:param name="PartyId" select="@PartyId"/>
		<PartyRecord>
			<PartyRecordPartyType><xsl:value-of select="@Role" /></PartyRecordPartyType>
			<xsl:copy-of select="/BillingObj.getAccountSummaryRs/Account/Parties/PartyRecord[PartyRecordPartyId = $PartyId]/*" />
		</PartyRecord>
		
	</xsl:template>

</xsl:stylesheet>