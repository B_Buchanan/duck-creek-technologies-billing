﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:dc="DuckCreek.XSL">
	<xsl:template match="limit">
		<xsl:if test="(sValue and sValue != '') or (iValue and iValue != '') or (fValue and fValue != '')">
			<xsl:call-template name="dc:Limit.Body"/>
		</xsl:if>		
	</xsl:template>
	<xsl:template name="dc:Limit.Body">
		<DC_Limit>
			<Type><xsl:value-of select="Type"/></Type>
			<Scope><xsl:value-of select="Scope"/></Scope>
			<xsl:call-template name="dc:Helper.GetTypeData"/>
			<xsl:call-template name="dc:Limit.Extensions"/>
			<xsl:call-template name="dc:Limit.Relationships"/>
		</DC_Limit>
	</xsl:template>
	<xsl:template name="dc:Limit.Body.Parameters">
		<xsl:param name="Type"/>
		<xsl:param name="Value"/>
		<xsl:param name="DataType"/>
		<xsl:if test="$Value != ''">
			<DC_Limit>
				<Type><xsl:value-of select="$Type"/></Type>
				<Value><xsl:value-of select="$Value"/></Value>
				<DataType><xsl:value-of select="$DataType"/></DataType>
				<xsl:call-template name="dc:Limit.Extensions"/>
				<xsl:call-template name="dc:Limit.Relationships"/>
			</DC_Limit>
		</xsl:if>
	</xsl:template>
	<xsl:template name="dc:Limit.Extensions"/>
	<xsl:template name="dc:Limit.Relationships"/>
</xsl:stylesheet>
