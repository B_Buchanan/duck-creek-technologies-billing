﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes"/>

	<xsl:template match="/*">
		<ReconciliationSession>
			<AccountId>
				<xsl:value-of select="session/data/AccountId"></xsl:value-of>
			</AccountId>
			<UserName>
				<xsl:value-of select="session/data/CurrentUserName"></xsl:value-of>
			</UserName>
			<TransactionDate>
				<xsl:value-of select="session/data/SystemDate"></xsl:value-of>
			</TransactionDate>
			<AgencyRemittedItems>
				<xsl:apply-templates select="session/data/OpenList/OpenItem[Selected ='1']"/>
			</AgencyRemittedItems>
			<AgencyAddedItems>
				<xsl:apply-templates select="session/data/AgencyAddedItems/AgencyAddedItem"/>
			</AgencyAddedItems>
			<AccountLevelWriteOffs>
				<xsl:apply-templates select="session/data/AccountLevelWriteOffs/AccountLevelWriteOff"/>
			</AccountLevelWriteOffs>
			<PromisesToPay>
				<xsl:call-template name="CreateImpliedPromiseToPay"></xsl:call-template>
				<xsl:apply-templates select="session/data/DebitOrCreditBalances/DebitOrCreditBalance[ItemTypeCode='PP']"/>
			</PromisesToPay>
			<CreditBalances>
				<xsl:apply-templates select="session/data/DebitOrCreditBalances/DebitOrCreditBalance[ItemTypeCode='CB']"/>
			</CreditBalances>
			<SuspenseRecords>
				<xsl:apply-templates select="session/data/SuspenseRecords/SuspenseRecord[Selected= '1'] "/>
			</SuspenseRecords>
			<ItemNotes>
				<xsl:apply-templates select="session/data/OpenList/*/ItemNote"/>
			</ItemNotes>
		</ReconciliationSession>
	</xsl:template>


	<!-- suppress default with this template -->

	<xsl:template match="text()"/>

	<xsl:template name="CreateImpliedPromiseToPay">
		<xsl:if test="session/data/ImpliedPromiseToPay != 0">
			<PromiseToPay>
				<PromiseToPayReasonCode>
					<xsl:text>MREC</xsl:text>
				</PromiseToPayReasonCode>
				<PromiseToPayAmount>
					<xsl:value-of select="session/data/ImpliedPromiseToPay"></xsl:value-of>
				</PromiseToPayAmount>
				<CurrencyCulture>
					<xsl:value-of select="session/data/DefaultCurrency"></xsl:value-of>
				</CurrencyCulture>
			</PromiseToPay>
		</xsl:if>
	</xsl:template>

	<xsl:template match="ItemNote[Status = 'New']">
		<ItemNote>
			<Status>New</Status>
			<AgencyStatementNoteTypeCode>
				<xsl:value-of select="AgencyStatementNoteTypeCode"></xsl:value-of>
			</AgencyStatementNoteTypeCode>
			<AgencyStatementNoteText>
				<xsl:value-of select="AgencyStatementNoteText"></xsl:value-of>
			</AgencyStatementNoteText>
			<UserId>
				<xsl:value-of select="UserId"></xsl:value-of>
			</UserId>
			<ObjectTypeCode>
				<xsl:value-of select="../OriginatingTypeCode"></xsl:value-of>
			</ObjectTypeCode>
			<ObjectId>
				<xsl:value-of select="../OriginatingId"></xsl:value-of>
			</ObjectId>
		</ItemNote>
	</xsl:template>
	<xsl:template match="ItemNote[Status = 'Update']">
		<ItemNote>
			<Status>Update</Status>
			<AgencyStatementNoteText>
				<xsl:value-of select="AgencyStatementNoteText"></xsl:value-of>
			</AgencyStatementNoteText>
			<UserId>
				<xsl:value-of select="UserId"></xsl:value-of>
			</UserId>
			<AgencyStatementNoteId>
				<xsl:value-of select="AgencyStatementNoteId"></xsl:value-of>
			</AgencyStatementNoteId>
			<AgencyStatementNoteLockingTS>
				<xsl:value-of select="AgencyStatementNoteLockingTS"></xsl:value-of>
			</AgencyStatementNoteLockingTS>
		</ItemNote>
	</xsl:template>
	<xsl:template match="ItemNote[Status = 'Delete']">
		<ItemNote>
			<Status>Delete</Status>
			<AgencyStatementNoteId>
				<xsl:value-of select="AgencyStatementNoteId"></xsl:value-of>
			</AgencyStatementNoteId>
			<AgencyStatementNoteLockingTS>
				<xsl:value-of select="AgencyStatementNoteLockingTS"></xsl:value-of>
			</AgencyStatementNoteLockingTS>
		</ItemNote>
	</xsl:template>


	<xsl:template match="OpenItem[IsAgencySubmittedItem = 0]">
		<AgencyRemittedItem>
			<PolicyTermId>
				<xsl:value-of select="PolicyTermId"></xsl:value-of>
			</PolicyTermId>
			<TransactionTypeCode>
				<xsl:value-of select="TransactionTypeCode"></xsl:value-of>
			</TransactionTypeCode>
			<RemittedAmount>
				<xsl:value-of select="RemittedNetAmount"></xsl:value-of>
			</RemittedAmount>
			<RemittedGrossAmount>
				<xsl:value-of select="RemittedGrossAmount"></xsl:value-of>
			</RemittedGrossAmount>
			<RemittedCommissionPercent>
				<xsl:value-of select="RemittedCommissionPercent"></xsl:value-of>
			</RemittedCommissionPercent>
			<RemittedCommissionAmount>
				<xsl:value-of select="RemittedCommissionAmount"></xsl:value-of>
			</RemittedCommissionAmount>
			<RemittedNetAmount>
				<xsl:value-of select="RemittedNetAmount"></xsl:value-of>
			</RemittedNetAmount>
			<OriginatingId>
				<xsl:value-of select="OriginatingId"></xsl:value-of>
			</OriginatingId>
			<OriginatingTypeCode>
				<xsl:value-of select="OriginatingTypeCode"></xsl:value-of>
			</OriginatingTypeCode>
			<ItemComment>
			</ItemComment>
		</AgencyRemittedItem>
	</xsl:template>

	<xsl:template match="OpenItem[IsAgencySubmittedItem = 1]">
		<AgencyRemittedItem>
			<PolicyTermId>
				<xsl:value-of select="PolicyTermId"></xsl:value-of>
			</PolicyTermId>
			<TransactionTypeCode>
				<xsl:value-of select="TransactionTypeCode"></xsl:value-of>
			</TransactionTypeCode>
			<RemittedAmount>
				<xsl:value-of select="RemittedNetAmount"></xsl:value-of>
			</RemittedAmount>
			<RemittedGrossAmount>
				<xsl:value-of select="RemittedGrossAmount"></xsl:value-of>
			</RemittedGrossAmount>
			<RemittedCommissionPercent>
				<xsl:value-of select="RemittedCommissionPercent"></xsl:value-of>
			</RemittedCommissionPercent>
			<RemittedCommissionAmount>
				<xsl:value-of select="RemittedCommissionAmount"></xsl:value-of>
			</RemittedCommissionAmount>
			<RemittedNetAmount>
				<xsl:value-of select="RemittedNetAmount"></xsl:value-of>
			</RemittedNetAmount>
			<OriginatingId>
				<xsl:value-of select="AgencyOpenItemsId"></xsl:value-of>
			</OriginatingId>
			<OriginatingTypeCode>
				<xsl:text>ARI</xsl:text>
			</OriginatingTypeCode>
		</AgencyRemittedItem>
	</xsl:template>


	<xsl:template match="AgencyAddedItem">
		<AgencyAddedDetail>
			<PolicyReference>
				<xsl:value-of select="PolicyReference"></xsl:value-of>
			</PolicyReference>
			<InsuredName>
				<xsl:value-of select="InsuredName"></xsl:value-of>
			</InsuredName>
			<EffectiveDate>
				<xsl:value-of select="EffectiveDate"></xsl:value-of>
			</EffectiveDate>
			<LineOfBusiness>
				<xsl:value-of select="LineOfBusinessCode"></xsl:value-of>
			</LineOfBusiness>
			<ReceivableTypeCode>
				<xsl:value-of select="ReceivableTypeCode"></xsl:value-of>
			</ReceivableTypeCode>
			<ReceivableSubTypeCode>
				<xsl:value-of select="ReceivableSubTypeCode"></xsl:value-of>
			</ReceivableSubTypeCode>
			<TransactionTypeCode>
				<xsl:value-of select="TransactionTypeCode"></xsl:value-of>
			</TransactionTypeCode>
			<CurrencyCulture>
				<xsl:value-of select="CurrencyCulture"></xsl:value-of>
			</CurrencyCulture>
			<AmountDue></AmountDue>
			<AddedItemNetAmount>
				<xsl:value-of select="RemittedNetAmount"></xsl:value-of>
			</AddedItemNetAmount>
			<AddedItemGrossAmount>
				<xsl:value-of select="RemittedGrossAmount"></xsl:value-of>
			</AddedItemGrossAmount>
			<AddedItemCommissionPercent>
				<xsl:value-of select="RemittedCommissionPercent"></xsl:value-of>
			</AddedItemCommissionPercent>
			<AddedItemCommissionAmount>
				<xsl:value-of select="RemittedCommissionAmount"></xsl:value-of>
			</AddedItemCommissionAmount>
			<AgencyComment></AgencyComment>
			<ItemStatusCode>SB</ItemStatusCode>
			<OriginatingTypeCode>AG</OriginatingTypeCode>
			<ItemReasonCode>AGNT</ItemReasonCode>
			<AgencyRemittedItem>
				<TransactionTypeCode>
					<xsl:value-of select="TransactionTypeCode"></xsl:value-of>
				</TransactionTypeCode>
				<RemittedAmount>
					<xsl:value-of select="RemittedNetAmount"></xsl:value-of>
				</RemittedAmount>
				<RemittedGrossAmount>
					<xsl:value-of select="RemittedGrossAmount"></xsl:value-of>
				</RemittedGrossAmount>
				<RemittedCommissionPercent>
					<xsl:value-of select="RemittedCommissionPercent"></xsl:value-of>
				</RemittedCommissionPercent>
				<RemittedCommissionAmount>
					<xsl:value-of select="RemittedCommissionAmount"></xsl:value-of>
				</RemittedCommissionAmount>
				<RemittedNetAmount>
					<xsl:value-of select="RemittedNetAmount"></xsl:value-of>
				</RemittedNetAmount>
				<OriginatingId></OriginatingId>
				<OriginatingTypeCode>AA</OriginatingTypeCode>
			</AgencyRemittedItem>
		</AgencyAddedDetail>
	</xsl:template>
	<xsl:template match="SuspenseRecord">
		<SuspenseRecord>
			<SuspenseId>
				<xsl:value-of select="SuspenseId"></xsl:value-of>
			</SuspenseId>
			<TotalAllocatedAmount>
				<xsl:value-of select="Amount"></xsl:value-of>
			</TotalAllocatedAmount>
			<SuspenseLockingTS>
				<xsl:value-of select="SuspenseLockingTS"></xsl:value-of>
			</SuspenseLockingTS>
		</SuspenseRecord>
	</xsl:template>
	<xsl:template match="AccountLevelWriteOff">
		<AccountLevelWriteOff>
			<WriteOffReasonCode>
				<xsl:value-of select="WriteOffReasonCode"></xsl:value-of>
			</WriteOffReasonCode>
			<WriteOffGrossAmount>
				<xsl:value-of select="RemittedGrossAmount"></xsl:value-of>
			</WriteOffGrossAmount>
			<WriteOffCommissionAmount>
				<xsl:value-of select="RemittedCommissionAmount"></xsl:value-of>
			</WriteOffCommissionAmount>
			<WriteOffNetAmount>
				<xsl:value-of select="RemittedNetAmount"></xsl:value-of>
			</WriteOffNetAmount>
			<CurrencyCulture>
				<xsl:value-of select="CurrencyCulture"></xsl:value-of>
			</CurrencyCulture>
			<WriteOffWaiveRequest>
				<WriteOffTypeCode>WO</WriteOffTypeCode>
				<ReasonCode>
					<xsl:value-of select="WriteOffReasonCode"></xsl:value-of>
				</ReasonCode>
				<Receivables>
					<Receivable>
						<ReceivableSourceId></ReceivableSourceId>
						<ReceivableSourceTypeCode>AP</ReceivableSourceTypeCode>
						<Amount>
							<xsl:value-of select="RemittedNetAmount"></xsl:value-of>
						</Amount>
					</Receivable>
				</Receivables>
			</WriteOffWaiveRequest>
		</AccountLevelWriteOff>
	</xsl:template>
	<xsl:template match="DebitOrCreditBalance[ItemTypeCode='PP']">
		<PromiseToPay>
			<PromiseToPayReasonCode>
				<xsl:value-of select="ItemReasonCode"></xsl:value-of>
			</PromiseToPayReasonCode>
			<PromiseToPayAmount>
				<xsl:value-of select="RemittedGrossAmount"></xsl:value-of>
			</PromiseToPayAmount>
			<CurrencyCulture>
				<xsl:value-of select="CurrencyCulture"></xsl:value-of>
			</CurrencyCulture>
		</PromiseToPay>
	</xsl:template>
	<xsl:template match="DebitOrCreditBalance[ItemTypeCode='CB']">
		<CreditBalance>
			<ProcessingOrgUnit>1</ProcessingOrgUnit>
			<CreditBalanceReasonCode>
				<xsl:value-of select="ItemReasonCode"></xsl:value-of>
			</CreditBalanceReasonCode>
			<CreditBalanceGrossAmount>
				<xsl:value-of select="RemittedGrossAmount * -1"></xsl:value-of>
			</CreditBalanceGrossAmount>
			<CreditBalanceCommissionAmount>
				<xsl:value-of select="RemittedCommissionAmount * -1"></xsl:value-of>
			</CreditBalanceCommissionAmount>
			<CreditBalanceNetAmount>
				<xsl:value-of select="RemittedNetAmount * -1"></xsl:value-of>
			</CreditBalanceNetAmount>
			<CurrencyCulture>
				<xsl:value-of select="CurrencyCulture"></xsl:value-of>
			</CurrencyCulture>
			<BillingPaymentObj.processManualAgencyPaymentRq>
				<PaymentInformationRecord>
					<PaymentType>P</PaymentType>
					<ApplyToOption>suspense</ApplyToOption>
				</PaymentInformationRecord>
				<PaymentAccountRecord>
					<AccountId>
						<xsl:value-of select="//session/data/AccountId"></xsl:value-of>
					</AccountId>
				</PaymentAccountRecord>
				<PaymentRecord>
					<PaymentReference>
					</PaymentReference>
					<EntryDate>
						<xsl:value-of select="//session/data/SystemDate"></xsl:value-of>
					</EntryDate>
					<Amount>
						<xsl:value-of select="RemittedNetAmount"></xsl:value-of>
					</Amount>
					<CurrencyCulture>
						<xsl:value-of select="CurrencyCulture"></xsl:value-of>
					</CurrencyCulture>
					<PaymentMethodCode>AGCB</PaymentMethodCode>
					<InitialAllocationAccountId>
						<xsl:value-of select="//session/data/AccountId"></xsl:value-of>
					</InitialAllocationAccountId>
					<ReportingGroupCode>1</ReportingGroupCode>
					<EntryUserId>
						<xsl:value-of select="//session/data/CurrentUserName"></xsl:value-of>
					</EntryUserId>
					<Description>
					</Description>
				</PaymentRecord>
				<SuspenseRecords>
					<SuspenseRecord>
						<Amount>
							<xsl:value-of select="RemittedNetAmount"></xsl:value-of>
						</Amount>
					</SuspenseRecord>
				</SuspenseRecords>
			</BillingPaymentObj.processManualAgencyPaymentRq>
		</CreditBalance>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="file:///c:/Users/p.manning/Documents/#Customers/Mountain States/Untitled1.xml" htmlbaseurl="" outputurl="" processortype="internal" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->